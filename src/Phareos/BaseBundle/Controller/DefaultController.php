<?php

namespace Phareos\BaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;


class DefaultController extends Controller
{
    
    public function indexAction()
    {
        
		$request = $this->get('request');
		$em = $this->getDoctrine()->getEntityManager();
		$repository_respsect = $em->getRepository('PhareosNomadeNetServiceBundle:respsect');
		
		$user = $this->container->get('security.context')->getToken()->getUser();
		$username = $user->getusername();
		$userid = $user->getid();
		
		$nomRespSecteur = $repository_respsect->findOneBy(array('idfos' => $userid));
		$nomRespSecteur2 = $nomRespSecteur->getNom();
		$prenomRespSecteur2 = $nomRespSecteur->getPrenom();
		$societeRespSecteur = $nomRespSecteur->getSociete();
		$applicationRespSecteur = $nomRespSecteur->getApplication();
		
		$session = $this->get('session');
		$session->set('nomUSER', $nomRespSecteur2);
		$session->set('prenomUSER', $prenomRespSecteur2);
		$session->set('societeUSER', $societeRespSecteur);
		$session->set('applicationUSER', $applicationRespSecteur);
		$session->set('nomDEPT', 'Tous');
		
		
		//Redirection suivant application
		
		if ($applicationRespSecteur == 'Toolbox'){
		
			return $this->redirect( $this->generateUrl('PhareosLogisToolBoxBundle_homepage') );
		}
		else if ($applicationRespSecteur == 'ToolboxClient'){
		
			return $this->redirect( $this->generateUrl('PhareosLogisToolBoxBundle_homepage') );
		}
		else if ($applicationRespSecteur == 'ToolboxInterv'){
		
			return $this->redirect( $this->generateUrl('PhareosLogisToolBoxBundle_homepage') );
		}
		else if ($applicationRespSecteur == 'PhareosAdmin'){
		
			return $this->redirect( $this->generateUrl('PhareosDeskNetServiceBundle_homepage') );
		}
		else if ($applicationRespSecteur == 'PhareosPointage'){
		
			return $this->redirect( $this->generateUrl('PhareosPointageServiceBundle_homepage') );
		}
		
		else if ($applicationRespSecteur == 'NomadeClient'){
		
			return $this->redirect( $this->generateUrl('PhareosNomadeNetServiceBundle_homemagclient', array('id' => '1629')) );
		}
		//else if ($username == 'FCharcotToolbox'){
		
		//	return $this->redirect( $this->generateUrl('PhareosLogisToolBoxBundle_homepage') );
		//}
		//else if ($username == 'JCharcotToolbox'){
		
		//	return $this->redirect( $this->generateUrl('PhareosLogisToolBoxBundle_homepage') );
		//}
		//else if ($username == 'VClient'){
		
		//	return $this->redirect( $this->generateUrl('PhareosLogisToolBoxBundle_homepage') );
		//}
		else {
			return $this->redirect( $this->generateUrl('PhareosNomadeNetServiceBundle_homepage') );
		}
    }
}
