<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\NomadeNetServiceBundle\Entity\pocontrqte
 *
 * @ORM\Table(name="nom_pocontrqte")
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\pocontrqteRepository")
 */
class pocontrqte
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\ManyToOne(targetEntity="tachesprog", inversedBy="pocontrqtetachesprog", cascade={"remove"})
	 * @ORM\JoinColumn(name="tachesprog_id", referencedColumnName="id")
	 */
	protected $tachesprog;
	
	/**
     * @var integer $lieux_id
     *
     * @ORM\Column(name="lieux_id", type="integer")
     */
    private $lieux_id;
	
	
	/**
     * @var integer $qte
     *
     * @ORM\Column(name="qte", type="integer")
     */
    private $qte;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set qte
     *
     * @param integer $qte
     */
    public function setQte($qte)
    {
        $this->qte = $qte;
    }

    /**
     * Get qte
     *
     * @return integer 
     */
    public function getQte()
    {
        return $this->qte;
    }

    /**
     * Set tachesprog
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\tachesprog $tachesprog
     */
    public function setTachesprog(\Phareos\NomadeNetServiceBundle\Entity\tachesprog $tachesprog)
    {
        $this->tachesprog = $tachesprog;
    }

    /**
     * Get tachesprog
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\tachesprog 
     */
    public function getTachesprog()
    {
        return $this->tachesprog;
    }

    /**
     * Set lieux_id
     *
     * @param integer $lieuxId
     */
    public function setLieuxId($lieuxId)
    {
        $this->lieux_id = $lieuxId;
    }

    /**
     * Get lieux_id
     *
     * @return integer 
     */
    public function getLieuxId()
    {
        return $this->lieux_id;
    }
}