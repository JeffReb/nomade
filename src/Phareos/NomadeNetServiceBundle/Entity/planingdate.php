<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\NomadeNetServiceBundle\Entity\planingdate
 *
 * @ORM\Table(name="nom_planingdate")
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\planingdateRepository")
 */
class planingdate
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $datedeb
     *
     * @ORM\Column(name="datedeb", type="datetime")
     */
    private $datedeb;
	
	/**
	* @ORM\ManyToOne(targetEntity="lieux", inversedBy="plandlieux")
	* @ORM\JoinColumn(name="lieux_id", referencedColumnName="id")
	*/
	protected $lieux;
	
	/**
	* @ORM\ManyToOne(targetEntity="agent", inversedBy="plandagent")
	* @ORM\JoinColumn(name="agent_id", referencedColumnName="id")
	*/
	protected $agent;
	
	/**
	 * @ORM\OneToMany(targetEntity="planjs", mappedBy="planingdate", cascade={"remove", "persist"})
	 */
	protected $plandplanjs;
	
	/**
     * @var integer $lieuxid2
     *
     * @ORM\Column(name="lieuxid2", type="integer", nullable=true)
     */
    private $lieuxid2;
	
	/**
     * @var integer $semaine
     *
     * @ORM\Column(name="semaine", type="integer", nullable=true)
     */
    private $semaine;

	
	/**
     * @var datetime $lundid
     *
     * @ORM\Column(name="lundid", type="datetime", nullable=true)
     */
    private $lundid;

	
	/**
     * @var datetime $mardid
     *
     * @ORM\Column(name="mardid", type="datetime", nullable=true)
     */
    private $mardid;

	
	/**
     * @var datetime $mercredid
     *
     * @ORM\Column(name="mercredid", type="datetime", nullable=true)
     */
    private $mercredid;

	
	/**
     * @var datetime $jeudid
     *
     * @ORM\Column(name="jeudid", type="datetime", nullable=true)
     */
    private $jeudid;

	
	/**
     * @var datetime $vendredid
     *
     * @ORM\Column(name="vendredid", type="datetime", nullable=true)
     */
    private $vendredid;

	
	/**
     * @var datetime $samedid
     *
     * @ORM\Column(name="samedid", type="datetime", nullable=true)
     */
    private $samedid;

    /**
     * @var integer $nbrsemaine
     *
     * @ORM\Column(name="nbrsemaine", type="integer", nullable=true)
     */
    private $nbrsemaine;
	
	/**
     * @var integer $numsemaine
     *
     * @ORM\Column(name="numsemaine", type="integer", nullable=true)
     */
    private $numsemaine;



    /**
     * @var string $vitrage
     *
     * @ORM\Column(name="vitrage", type="string", length=255, nullable=true)
     */
    private $vitrage;
	
	/**
     * @var string $agent2
     *
     * @ORM\Column(name="agent2", type="string", length=255, nullable=true)
     */
    private $agent2;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datedeb
     *
     * @param datetime $datedeb
     */
    public function setDatedeb($datedeb)
    {
        $this->datedeb = $datedeb;
    }

    /**
     * Get datedeb
     *
     * @return datetime 
     */
    public function getDatedeb()
    {
        return $this->datedeb;
    }


    /**
     * Set nbrsemaine
     *
     * @param integer $nbrsemaine
     */
    public function setNbrsemaine($nbrsemaine)
    {
        $this->nbrsemaine = $nbrsemaine;
    }

    /**
     * Get nbrsemaine
     *
     * @return integer 
     */
    public function getNbrsemaine()
    {
        return $this->nbrsemaine;
    }


	/**
     * Set agent2
     *
     * @param string $agent2
     */
    public function setAgent2($agent2)
    {
        $this->agent2 = $agent2;
    }

    /**
     * Get agent2
     *
     * @return string 
     */
    public function getAgent2()
    {
        return $this->agent2;
    }
	
    /**
     * Set vitrage
     *
     * @param string $vitrage
     */
    public function setVitrage($vitrage)
    {
        $this->vitrage = $vitrage;
    }

    /**
     * Get vitrage
     *
     * @return string 
     */
    public function getVitrage()
    {
        return $this->vitrage;
    }

    /**
     * Set lieux
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\lieux $lieux
     */
    public function setLieux(\Phareos\NomadeNetServiceBundle\Entity\lieux $lieux)
    {
        $this->lieux = $lieux;
    }

    /**
     * Get lieux
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\lieux 
     */
    public function getLieux()
    {
        return $this->lieux;
    }

    /**
     * Set agent
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\agent $agent
     */
    public function setAgent(\Phareos\NomadeNetServiceBundle\Entity\agent $agent)
    {
        $this->agent = $agent;
    }

    /**
     * Get agent
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\agent 
     */
    public function getAgent()
    {
        return $this->agent;
    }
    
	/**
     * Set lieuxid2
     *
     * @param integer $lieuxid2
     */
    public function setLieuxid2($lieuxid2)
    {
        $this->lieuxid2 = $lieuxid2;
    }

    /**
     * Get lieuxid2
     *
     * @return integer 
     */
    public function getLieuxid2()
    {
        return $this->lieuxid2;
    }
	
	/**
     * Set semaine
     *
     * @param integer $semaine
     */
    public function setSemaine($semaine)
    {
        $this->semaine = $semaine;
    }

    /**
     * Get semaine
     *
     * @return integer 
     */
    public function getSemaine()
    {
        return $this->semaine;
    }

    /**
     * Set lundid
     *
     * @param datetime $lundid
     */
    public function setLundid($lundid)
    {
        $this->lundid = $lundid;
    }

    /**
     * Get lundid
     *
     * @return datetime 
     */
    public function getLundid()
    {
        return $this->lundid;
    }

    /**
     * Set mardid
     *
     * @param datetime $mardid
     */
    public function setMardid($mardid)
    {
        $this->mardid = $mardid;
    }

    /**
     * Get mardid
     *
     * @return datetime 
     */
    public function getMardid()
    {
        return $this->mardid;
    }

    /**
     * Set mercredid
     *
     * @param datetime $mercredid
     */
    public function setMercredid($mercredid)
    {
        $this->mercredid = $mercredid;
    }

    /**
     * Get mercredid
     *
     * @return datetime 
     */
    public function getMercredid()
    {
        return $this->mercredid;
    }

    /**
     * Set jeudid
     *
     * @param datetime $jeudid
     */
    public function setJeudid($jeudid)
    {
        $this->jeudid = $jeudid;
    }

    /**
     * Get jeudid
     *
     * @return datetime 
     */
    public function getJeudid()
    {
        return $this->jeudid;
    }

    /**
     * Set vendredid
     *
     * @param datetime $vendredid
     */
    public function setVendredid($vendredid)
    {
        $this->vendredid = $vendredid;
    }

    /**
     * Get vendredid
     *
     * @return datetime 
     */
    public function getVendredid()
    {
        return $this->vendredid;
    }

    /**
     * Set samedid
     *
     * @param datetime $samedid
     */
    public function setSamedid($samedid)
    {
        $this->samedid = $samedid;
    }

    /**
     * Get samedid
     *
     * @return datetime 
     */
    public function getSamedid()
    {
        return $this->samedid;
    }

    /**
     * Set numsemaine
     *
     * @param integer $numsemaine
     */
    public function setNumsemaine($numsemaine)
    {
        $this->numsemaine = $numsemaine;
    }

    /**
     * Get numsemaine
     *
     * @return integer 
     */
    public function getNumsemaine()
    {
        return $this->numsemaine;
    }
    public function __construct()
    {
        $this->plandplanjs = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add plandplanjs
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\planjs $plandplanjs
     */
    public function addplanjs(\Phareos\NomadeNetServiceBundle\Entity\planjs $plandplanjs)
    {
        $this->plandplanjs[] = $plandplanjs;
    }

    /**
     * Get plandplanjs
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPlandplanjs()
    {
        return $this->plandplanjs;
    }
}