<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\NomadeNetServiceBundle\Entity\fonction
 *
 * @ORM\Table(name="nom_fonction")
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\fonctionRepository")
 */
class fonction
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\OneToMany(targetEntity="tachesprog", mappedBy="fonction", cascade={"remove", "persist"})
	 */
	protected $fonctachprog;
	
	/**
	 * @ORM\OneToMany(targetEntity="tachesprogmag", mappedBy="fonction", cascade={"remove", "persist"})
	 */
	protected $fonctachprogmag;

    /**
     * @var string $nom
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;
	
	/**
     * @var string $nomafficher
     *
     * @ORM\Column(name="nomafficher", type="string", length=255, nullable=true)
     */
    private $nomafficher;
	
	
	/**
     * @var integer $grdcompte_id
     *
     * @ORM\Column(name="grdcompte_id", type="integer")
     */
    private $grdcompte_id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set grdcompte_id
     *
     * @param integer $grdcompteId
     */
    public function setGrdcompteId($grdcompteId)
    {
        $this->grdcompte_id = $grdcompteId;
    }

    /**
     * Get grdcompte_id
     *
     * @return integer 
     */
    public function getGrdcompteId()
    {
        return $this->grdcompte_id;
    }
    public function __construct()
    {
        $this->fonctachprog = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add fonctachprog
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\tachesprog $fonctachprog
     */
    public function addtachesprog(\Phareos\NomadeNetServiceBundle\Entity\tachesprog $fonctachprog)
    {
        $this->fonctachprog[] = $fonctachprog;
    }

    /**
     * Get fonctachprog
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getFonctachprog()
    {
        return $this->fonctachprog;
    }

    /**
     * Add fonctachprogmag
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\tachesprogmag $fonctachprogmag
     */
    public function addtachesprogmag(\Phareos\NomadeNetServiceBundle\Entity\tachesprogmag $fonctachprogmag)
    {
        $this->fonctachprogmag[] = $fonctachprogmag;
    }

    /**
     * Get fonctachprogmag
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getFonctachprogmag()
    {
        return $this->fonctachprogmag;
    }

    /**
     * Set nomafficher
     *
     * @param string $nomafficher
     */
    public function setNomafficher($nomafficher)
    {
        $this->nomafficher = $nomafficher;
    }

    /**
     * Get nomafficher
     *
     * @return string 
     */
    public function getNomafficher()
    {
        return $this->nomafficher;
    }
}