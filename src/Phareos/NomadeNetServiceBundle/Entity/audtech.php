<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


use Symfony\Component\Validator\Constraints as Assert;

/**
 * Phareos\NomadeNetServiceBundle\Entity\audtech
 *
 * @ORM\Table(name="nom_audtech")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\audtechRepository")
 */
class audtech
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\OneToMany(targetEntity="audtprog", mappedBy="audtech", cascade={"remove", "persist"})
	 */
	protected $audtechtachepros;
	
	/**
	* @ORM\ManyToOne(targetEntity="lieux", inversedBy="audtechlieux")
	* @ORM\JoinColumn(name="lieux_id", referencedColumnName="id")
	*/
	protected $lieux;
	
	/**
	 * @ORM\ManyToOne(targetEntity="prodmag", inversedBy="prodmaudtech", cascade={"remove"})
	 * @ORM\JoinColumn(name="prodmag_id", referencedColumnName="id")
	 */
	protected $prodmag;

    /**
     * @var boolean $trait_prio
     *
     * @ORM\Column(name="trait_prio", type="boolean", nullable=true)
     */
    private $trait_prio;
	
	/**
     * @var boolean $matenpanne
     *
     * @ORM\Column(name="matenpanne", type="boolean", nullable=true)
     */
    private $matenpanne;
	
	/**
     * @var boolean $bonvit
     *
     * @ORM\Column(name="bonvit", type="boolean", nullable=true)
     */
    private $bonvit;
	
	/**
     * @var boolean $bonvitval
     *
     * @ORM\Column(name="bonvitval", type="boolean", nullable=true)
     */
    private $bonvitval;
	
	/**
     * @var text $trait_prio_com
     *
     * @ORM\Column(name="trait_prio_com", type="text", nullable=true)
     */
    private $trait_prio_com;

    /**
     * @var boolean $poin_mag
     *
     * @ORM\Column(name="poin_mag", type="boolean", nullable=true)
     */
    private $poin_mag;

    /**
     * @var boolean $reapro
     *
     * @ORM\Column(name="reapro", type="boolean", nullable=true)
     */
    private $reapro;

    /**
     * @var boolean $form_po_agen
     *
     * @ORM\Column(name="form_po_agen", type="boolean", nullable=true)
     */
    private $form_po_agen;
	
	/**
     * @var boolean $carnetbord
     *
     * @ORM\Column(name="carnetbord", type="boolean", nullable=true)
     */
    private $carnetbord;
	
	/**
     * @var boolean $dispodir
     *
     * @ORM\Column(name="dispodir", type="boolean", nullable=true)
     */
    private $dispodir;
	
	/**
     * @var string $pointdirres
     *
     * @ORM\Column(name="pointdirres", type="string", length=255, nullable=true)
     */
    private $pointdirres;
	
	/**
     * @var string $pointagent
     *
     * @ORM\Column(name="pointagent", type="string", length=255, nullable=true)
     */
    private $pointagent;


    /**
     * @var date $date_maj
     *
     * @ORM\Column(name="date_maj", type="date")
     */
    private $date_maj;

    /**
     * @var time $heure_maj
     *
     * @ORM\Column(name="heure_maj", type="time", nullable=true)
     */
    private $heure_maj;
	
	/**
     * @var string $responsecteur
     *
     * @ORM\Column(name="responsecteur", type="string", length=255, nullable=true)
     */
    private $responsecteur;

    /**
     * @var string $nomsignaltea
     *
     * @ORM\Column(name="nomsignaltea", type="string", length=255, nullable=true)
     */
    private $nomsignaltea;

    /**
     * @var string $imgsignaltea
     *
     * @ORM\Column(name="imgsignaltea", type="string", length=255, nullable=true)
     */
    private $imgsignaltea;

    /**
     * @var string $nomsignclient
     *
     * @ORM\Column(name="nomsignclient", type="string", length=255, nullable=true)
     */
    private $nomsignclient;

    /**
     * @var string $imgsignclient
     *
     * @ORM\Column(name="imgsignclient", type="string", length=255, nullable=true)
     */
    private $imgsignclient;
	
	/**
     * @var string $filebv1
     * @Assert\File( maxSize = "4M", mimeTypesMessage = "SVP Uploader un fichier img")
     * @ORM\Column(name="filebv1", type="string", length=255, nullable=true)
     */
    private $filebv1;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set trait_prio
     *
     * @param boolean $traitPrio
     */
    public function setTraitPrio($traitPrio)
    {
        $this->trait_prio = $traitPrio;
    }

    /**
     * Get trait_prio
     *
     * @return boolean 
     */
    public function getTraitPrio()
    {
        return $this->trait_prio;
    }
	
	/**
     * Set matenpanne
     *
     * @param boolean $matenpanne
     */
    public function setMatenpanne($matenpanne)
    {
        $this->matenpanne = $matenpanne;
    }

    /**
     * Get matenpanne
     *
     * @return boolean 
     */
    public function getMatenpanne()
    {
        return $this->matenpanne;
    }
	
	/**
     * Set bonvit
     *
     * @param boolean $bonvit
     */
    public function setBonvit($bonvit)
    {
        $this->bonvit = $bonvit;
    }

    /**
     * Get bonvit
     *
     * @return boolean 
     */
    public function getBonvit()
    {
        return $this->bonvit;
    }
	
	/**
     * Set bonvitval
     *
     * @param boolean $bonvitval
     */
    public function setBonvitval($bonvitval)
    {
        $this->bonvitval = $bonvitval;
    }

    /**
     * Get bonvitval
     *
     * @return boolean 
     */
    public function getBonvitval()
    {
        return $this->bonvitval;
    }

    /**
     * Set poin_mag
     *
     * @param boolean $poinMag
     */
    public function setPoinMag($poinMag)
    {
        $this->poin_mag = $poinMag;
    }

    /**
     * Get poin_mag
     *
     * @return boolean 
     */
    public function getPoinMag()
    {
        return $this->poin_mag;
    }

    /**
     * Set reapro
     *
     * @param boolean $reapro
     */
    public function setReapro($reapro)
    {
        $this->reapro = $reapro;
    }

    /**
     * Get reapro
     *
     * @return boolean 
     */
    public function getReapro()
    {
        return $this->reapro;
    }

    /**
     * Set form_po_agen
     *
     * @param boolean $formPoAgen
     */
    public function setFormPoAgen($formPoAgen)
    {
        $this->form_po_agen = $formPoAgen;
    }

    /**
     * Get form_po_agen
     *
     * @return boolean 
     */
    public function getFormPoAgen()
    {
        return $this->form_po_agen;
    }
	
	/**
     * Set carnetbord
     *
     * @param boolean $carnetbord
     */
    public function setCarnetbord($carnetbord)
    {
        $this->carnetbord = $carnetbord;
    }

    /**
     * Get carnetbord
     *
     * @return boolean 
     */
    public function getCarnetbord()
    {
        return $this->carnetbord;
    }
	
	/**
     * Set dispodir
     *
     * @param boolean $dispodir
     */
    public function setDispodir($dispodir)
    {
        $this->dispodir = $dispodir;
    }

    /**
     * Get dispodir
     *
     * @return boolean 
     */
    public function getDispodir()
    {
        return $this->dispodir;
    }
	
	/**
     * Set pointdirres
     *
     * @param string $pointdirres
     */
    public function setPointdirres($pointdirres)
    {
        $this->pointdirres = $pointdirres;
    }

    /**
     * Get pointdirres
     *
     * @return string 
     */
    public function getPointdirres()
    {
        return $this->pointdirres;
    }

    /**
     * Set date_maj
     *
     * @param date $dateMaj
     */
    public function setDateMaj($dateMaj)
    {
        $this->date_maj = $dateMaj;
    }

    /**
     * Get date_maj
     *
     * @return date 
     */
    public function getDateMaj()
    {
        return $this->date_maj;
    }

    /**
     * Set heure_maj
     *
     * @param time $heureMaj
     */
    public function setHeureMaj($heureMaj)
    {
        $this->heure_maj = $heureMaj;
    }

    /**
     * Get heure_maj
     *
     * @return time 
     */
    public function getHeureMaj()
    {
        return $this->heure_maj;
    }
	
	/**
     * Set responsecteur
     *
     * @param string $responsecteur
     */
    public function setResponsecteur($responsecteur)
    {
        $this->responsecteur = $responsecteur;
    }

    /**
     * Get responsecteur
     *
     * @return string 
     */
    public function getResponsecteur()
    {
        return $this->responsecteur;
    }
	
	/**
     * Set pointagent
     *
     * @param string $pointagent
     */
    public function setPointagent($pointagent)
    {
        $this->pointagent = $pointagent;
    }

    /**
     * Get pointagent
     *
     * @return string 
     */
    public function getPointagent()
    {
        return $this->pointagent;
    }

    /**
     * Set nomsignaltea
     *
     * @param string $nomsignaltea
     */
    public function setNomsignaltea($nomsignaltea)
    {
        $this->nomsignaltea = $nomsignaltea;
    }

    /**
     * Get nomsignaltea
     *
     * @return string 
     */
    public function getNomsignaltea()
    {
        return $this->nomsignaltea;
    }

    /**
     * Set imgsignaltea
     *
     * @param string $imgsignaltea
     */
    public function setImgsignaltea($imgsignaltea)
    {
        $this->imgsignaltea = $imgsignaltea;
    }

    /**
     * Get imgsignaltea
     *
     * @return string 
     */
    public function getImgsignaltea()
    {
        return $this->imgsignaltea;
    }

    /**
     * Set nomsignclient
     *
     * @param string $nomsignclient
     */
    public function setNomsignclient($nomsignclient)
    {
        $this->nomsignclient = $nomsignclient;
    }

    /**
     * Get nomsignclient
     *
     * @return string 
     */
    public function getNomsignclient()
    {
        return $this->nomsignclient;
    }

    /**
     * Set imgsignclient
     *
     * @param string $imgsignclient
     */
    public function setImgsignclient($imgsignclient)
    {
        $this->imgsignclient = $imgsignclient;
    }

    /**
     * Get imgsignclient
     *
     * @return string 
     */
    public function getImgsignclient()
    {
        return $this->imgsignclient;
    }

    /**
     * Set lieux
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\lieux $lieux
     */
    public function setLieux(\Phareos\NomadeNetServiceBundle\Entity\lieux $lieux)
    {
        $this->lieux = $lieux;
    }

    /**
     * Get lieux
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\lieux 
     */
    public function getLieux()
    {
        return $this->lieux;
    }

    /**
     * Set trait_prio_com
     *
     * @param text $traitPrioCom
     */
    public function setTraitPrioCom($traitPrioCom)
    {
        $this->trait_prio_com = $traitPrioCom;
    }

    /**
     * Get trait_prio_com
     *
     * @return text 
     */
    public function getTraitPrioCom()
    {
        return $this->trait_prio_com;
    }
    public function __construct()
    {
        $this->audtechaudtprog = new \Doctrine\Common\Collections\ArrayCollection();
		$this->audtechtachepros = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    
	
	/**
     * Add audtechtachepros
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\audtprog $audtechtachepros
     */
    public function addaudtprog(\Phareos\NomadeNetServiceBundle\Entity\audtprog $audtechtachepros)
    {
        $this->audtechtachepros[] = $audtechtachepros;
    }

    /**
     * Get audtechtachepros
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getAudtechtachepros()
    {
        return $this->audtechtachepros;
    }

    /**
     * Set prodmag
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\prodmag $prodmag
     */
    public function setProdmag(\Phareos\NomadeNetServiceBundle\Entity\prodmag $prodmag)
    {
        $this->prodmag = $prodmag;
    }

    /**
     * Get prodmag
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\prodmag 
     */
    public function getProdmag()
    {
        return $this->prodmag;
    }
	
	/**
     * Set filebv1
     *
     * @param string $filebv1
     */
    public function setFilebv1($filebv1)
    {
        $this->filebv1 = $filebv1;
    }

    /**
     * Get filebv1
     *
     * @return string 
     */
    public function getFilebv1()
    {
        return $this->filebv1;
    }
	
	public function getFullPdfPath() {
        return null === $this->filebv1 ? null : $this->getUploadRootDir(). $this->filebv1;
    }
	
	protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return $this->getTmpUploadRootDir().$this->getId()."/";
    }
	
	protected function getTmpUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/upload/nomade/audit/bonvitrage/';
    }
	
	/**
     * @ORM\PrePersist()
     * 
     */
    public function uploadPdf() {
        // the file property can be empty if the field is not required
        if (null === $this->filebv1) {
            return;
        }
        if(!$this->id){
            $this->filebv1->move($this->getTmpUploadRootDir(), $this->filebv1->getClientOriginalName());
        }else{
            $this->filebv1->move($this->getUploadRootDir(), $this->filebv1->getClientOriginalName());
        }
        $this->setFilebv1($this->filebv1->getClientOriginalName());
    }
	
	/**
     * @ORM\PostPersist()
     */
    public function movePdf()
    {
        if (null === $this->filebv1) {
            return;
        }
        if(!is_dir($this->getUploadRootDir())){
            mkdir($this->getUploadRootDir());
        }
        copy($this->getTmpUploadRootDir().$this->filebv1, $this->getFullPdfPath());
        unlink($this->getTmpUploadRootDir().$this->filebv1);
    }
}