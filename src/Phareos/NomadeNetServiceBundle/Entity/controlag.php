<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\NomadeNetServiceBundle\Entity\controlag
 *
 * @ORM\Table(name="nom_controlag")
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\controlagRepository")
 */
class controlag
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	
	/**
	* @ORM\ManyToOne(targetEntity="lieux", inversedBy="controlaglieux", cascade={"remove"})
	* @ORM\JoinColumn(name="lieux_id", referencedColumnName="id")
	*/
	protected $lieux;

    /**
     * @var string $moi
     *
     * @ORM\Column(name="moi", type="string", nullable=true)
     */
    private $moi;

    /**
     * @var integer $annee
     *
     * @ORM\Column(name="annee", type="integer", nullable=true)
     */
    private $annee;

    /**
     * @var boolean $signe
     *
     * @ORM\Column(name="signe", type="boolean", nullable=true)
     */
    private $signe;

    /**
     * @var boolean $rdvous
     *
     * @ORM\Column(name="rdvous", type="boolean", nullable=true)
     */
    private $rdvous;

    /**
     * @var string $respsect
     *
     * @ORM\Column(name="respsect", type="string", length=255, nullable=true)
     */
    private $respsect;

    /**
     * @var boolean $active
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;
	
	/**
     * @var integer $dernumag
     *
     * @ORM\Column(name="dernumag", type="integer", nullable=true)
     */
    private $dernumag;
	
	/**
     * @var boolean $dispodir
     *
     * @ORM\Column(name="dispodir", type="boolean", nullable=true)
     */
    private $dispodir;
	
	/**
     * @var string $motifabs
     *
     * @ORM\Column(name="motifabs", type="string", nullable=true)
     */
    private $motifabs;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
	
	/**
     * Set lieux
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\lieux $lieux
     */
    public function setLieux(\Phareos\NomadeNetServiceBundle\Entity\lieux $lieux)
    {
        $this->lieux = $lieux;
    }
	
    /**
     * Get lieux
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\lieux 
     */
    public function getLieux()
    {
        return $this->lieux;
    }

    /**
     * Set moi
     *
     * @param string $moi
     */
    public function setMoi($moi)
    {
        $this->moi = $moi;
    }

    /**
     * Get moi
     *
     * @return string 
     */
    public function getMoi()
    {
        return $this->moi;
    }

    /**
     * Set annee
     *
     * @param integer $annee
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;
    }

    /**
     * Get annee
     *
     * @return integer 
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Set signe
     *
     * @param boolean $signe
     */
    public function setSigne($signe)
    {
        $this->signe = $signe;
    }

    /**
     * Get signe
     *
     * @return boolean 
     */
    public function getSigne()
    {
        return $this->signe;
    }

    /**
     * Set rdvous
     *
     * @param boolean $rdvous
     */
    public function setRdvous($rdvous)
    {
        $this->rdvous = $rdvous;
    }

    /**
     * Get rdvous
     *
     * @return boolean 
     */
    public function getRdvous()
    {
        return $this->rdvous;
    }

    /**
     * Set respsect
     *
     * @param string $respsect
     */
    public function setRespsect($respsect)
    {
        $this->respsect = $respsect;
    }

    /**
     * Get respsect
     *
     * @return string 
     */
    public function getRespsect()
    {
        return $this->respsect;
    }

    /**
     * Set active
     *
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }
	
	/**
     * Set dernumag
     *
     * @param integer $dernumag
     */
    public function setDernumag($dernumag)
    {
        $this->dernumag = $dernumag;
    }

    /**
     * Get dernumag
     *
     * @return integer 
     */
    public function getDernumag()
    {
        return $this->dernumag;
    }
	
	/**
     * Set dispodir
     *
     * @param boolean $dispodir
     */
    public function setDispodir($dispodir)
    {
        $this->dispodir = $dispodir;
    }

    /**
     * Get dispodir
     *
     * @return boolean 
     */
    public function getDispodir()
    {
        return $this->dispodir;
    }
	
	/**
     * Set motifabs
     *
     * @param string $motifabs
     */
    public function setMotifabs($motifabs)
    {
        $this->motifabs = $motifabs;
    }

    /**
     * Get motifabs
     *
     * @return string 
     */
    public function getMotifabs()
    {
        return $this->motifabs;
    }
}