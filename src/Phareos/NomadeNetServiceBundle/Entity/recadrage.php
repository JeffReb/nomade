<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\NomadeNetServiceBundle\Entity\recadrage
 *
 * @ORM\Table(name="nom_recadrage")
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\recadrageRepository")
 */
class recadrage
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\ManyToOne(targetEntity="agent", inversedBy="agentrecadre", cascade={"remove"})
	 * @ORM\JoinColumn(name="agent_id", referencedColumnName="id")
	 */
	protected $agent;
	
	/**
	 * @ORM\ManyToOne(targetEntity="lieux", inversedBy="lieuxrecadre", cascade={"remove"})
	 * @ORM\JoinColumn(name="lieux_id", referencedColumnName="id")
	 */
	protected $lieux;

    /**
     * @var boolean $object
     *
     * @ORM\Column(name="object", type="boolean", nullable=true)
     */
    private $object;

    /**
     * @var boolean $objatt
     *
     * @ORM\Column(name="objatt", type="boolean", nullable=true)
     */
    private $objatt;

    /**
     * @var text $objectcom
     *
     * @ORM\Column(name="objectcom", type="text", nullable=true)
     */
    private $objectcom;

    /**
     * @var date $daterecad
     *
     * @ORM\Column(name="daterecad", type="date", nullable=true)
     */
    private $daterecad;

    /**
     * @var date $dateobj
     *
     * @ORM\Column(name="dateobj", type="date", nullable=true)
     */
    private $dateobj;
	
	/**
     * @var string $nomsignaltea
     *
     * @ORM\Column(name="nomsignaltea", type="string", nullable=true)
     */
    private $nomsignaltea;
	
	/**
     * @var string $imgsignaltea
     *
     * @ORM\Column(name="imgsignaltea", type="string", nullable=true)
     */
    private $imgsignaltea;
	
	/**
     * @var string $nomsignagent
     *
     * @ORM\Column(name="nomsignagent", type="string", nullable=true)
     */
    private $nomsignagent;
	
	
	/**
     * @var string $imgsignagent
     *
     * @ORM\Column(name="imgsignagent", type="string", nullable=true)
     */
    private $imgsignagent;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set object
     *
     * @param boolean $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }

    /**
     * Get object
     *
     * @return boolean 
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * Set objatt
     *
     * @param boolean $objatt
     */
    public function setObjatt($objatt)
    {
        $this->objatt = $objatt;
    }

    /**
     * Get objatt
     *
     * @return boolean 
     */
    public function getObjatt()
    {
        return $this->objatt;
    }

    /**
     * Set objectcom
     *
     * @param text $objectcom
     */
    public function setObjectcom($objectcom)
    {
        $this->objectcom = $objectcom;
    }

    /**
     * Get objectcom
     *
     * @return text 
     */
    public function getObjectcom()
    {
        return $this->objectcom;
    }

    /**
     * Set daterecad
     *
     * @param date $daterecad
     */
    public function setDaterecad($daterecad)
    {
        $this->daterecad = $daterecad;
    }

    /**
     * Get daterecad
     *
     * @return date 
     */
    public function getDaterecad()
    {
        return $this->daterecad;
    }

    /**
     * Set dateobj
     *
     * @param date $dateobj
     */
    public function setDateobj($dateobj)
    {
        $this->dateobj = $dateobj;
    }

    /**
     * Get dateobj
     *
     * @return date 
     */
    public function getDateobj()
    {
        return $this->dateobj;
    }

    /**
     * Set agent
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\agent $agent
     */
    public function setAgent(\Phareos\NomadeNetServiceBundle\Entity\agent $agent)
    {
        $this->agent = $agent;
    }

    /**
     * Get agent
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\agent 
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set lieux
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\lieux $lieux
     */
    public function setLieux(\Phareos\NomadeNetServiceBundle\Entity\lieux $lieux)
    {
        $this->lieux = $lieux;
    }

    /**
     * Get lieux
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\lieux 
     */
    public function getLieux()
    {
        return $this->lieux;
    }
	
	/**
     * Set nomsignaltea
     *
     * @param string $nomsignaltea
     */
    public function setNomsignaltea($nomsignaltea)
    {
        $this->nomsignaltea = $nomsignaltea;
    }

    /**
     * Get nomsignaltea
     *
     * @return string 
     */
    public function getNomsignaltea()
    {
        return $this->nomsignaltea;
    }
	
	/**
     * Set nomsignagent
     *
     * @param string $nomsignagent
     */
    public function setNomsignagent($nomsignagent)
    {
        $this->nomsignagent = $nomsignagent;
    }

    /**
     * Get nomsignagent
     *
     * @return string 
     */
    public function getNomsignagent()
    {
        return $this->nomsignagent;
    }
	
	/**
     * Set imgsignaltea
     *
     * @param string $imgsignaltea
     */
    public function setImgsignaltea($imgsignaltea)
    {
        $this->imgsignaltea = $imgsignaltea;
    }

    /**
     * Get imgsignaltea
     *
     * @return string 
     */
    public function getImgsignaltea()
    {
        return $this->imgsignaltea;
    }
	
	/**
     * Set imgsignagent
     *
     * @param string $imgsignagent
     */
    public function setImgsignagent($imgsignagent)
    {
        $this->imgsignagent = $imgsignagent;
    }

    /**
     * Get imgsignagent
     *
     * @return string 
     */
    public function getImgsignagent()
    {
        return $this->imgsignagent;
    }
}