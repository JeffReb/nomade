<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\NomadeNetServiceBundle\Entity\planning
 *
 * @ORM\Table(name="nom_planning")
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\planningRepository")
 */
class planning
{
    /**
	* @ORM\ManyToOne(targetEntity="lieux", inversedBy="planninglieux", cascade={"remove"})
	* @ORM\JoinColumn(name="lieux_id", referencedColumnName="id")
	*/
	protected $lieux;
	
	
	/**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $titre
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string $lundi_t1
     *
     * @ORM\Column(name="lundi_t1", type="string", length=255, nullable=true)
     */
    private $lundi_t1;

    /**
     * @var string $lundi_t2
     *
     * @ORM\Column(name="lundi_t2", type="string", length=255, nullable=true)
     */
    private $lundi_t2;

    /**
     * @var string $lundi_t3
     *
     * @ORM\Column(name="lundi_t3", type="string", length=255, nullable=true)
     */
    private $lundi_t3;

    /**
     * @var string $lundi_t4
     *
     * @ORM\Column(name="lundi_t4", type="string", length=255, nullable=true)
     */
    private $lundi_t4;

    /**
     * @var string $lundi_t5
     *
     * @ORM\Column(name="lundi_t5", type="string", length=255, nullable=true)
     */
    private $lundi_t5;
	
	/**
     * @var string $lundi_t5com
     *
     * @ORM\Column(name="lundi_t5com", type="string", length=25, nullable=true)
     */
    private $lundi_t5com;

    /**
     * @var string $lundi_t6
     *
     * @ORM\Column(name="lundi_t6", type="string", length=255, nullable=true)
     */
    private $lundi_t6;
	
	/**
     * @var string $lundi_t6com
     *
     * @ORM\Column(name="lundi_t6com", type="string", length=25, nullable=true)
     */
    private $lundi_t6com;

    /**
     * @var string $lundi_t7
     *
     * @ORM\Column(name="lundi_t7", type="string", length=255, nullable=true)
     */
    private $lundi_t7;
	
	/**
     * @var string $lundi_t7com
     *
     * @ORM\Column(name="lundi_t7com", type="string", length=25, nullable=true)
     */
    private $lundi_t7com;

    /**
     * @var string $lundi_t8
     *
     * @ORM\Column(name="lundi_t8", type="string", length=255, nullable=true)
     */
    private $lundi_t8;
	
	/**
     * @var string $lundi_t8com
     *
     * @ORM\Column(name="lundi_t8com", type="string", length=25, nullable=true)
     */
    private $lundi_t8com;

    /**
     * @var string $lundi_t9
     *
     * @ORM\Column(name="lundi_t9", type="string", length=255, nullable=true)
     */
    private $lundi_t9;
	
	/**
     * @var string $lundi_t9com
     *
     * @ORM\Column(name="lundi_t9com", type="string", length=25, nullable=true)
     */
    private $lundi_t9com;

    /**
     * @var string $lundi_t10
     *
     * @ORM\Column(name="lundi_t10", type="string", length=255, nullable=true)
     */
    private $lundi_t10;
	
	/**
     * @var string $lundi_t10com
     *
     * @ORM\Column(name="lundi_t10com", type="string", length=25, nullable=true)
     */
    private $lundi_t10com;

    /**
     * @var string $mardi_t1
     *
     * @ORM\Column(name="mardi_t1", type="string", length=255, nullable=true)
     */
    private $mardi_t1;

    /**
     * @var string $mardi_t2
     *
     * @ORM\Column(name="mardi_t2", type="string", length=255, nullable=true)
     */
    private $mardi_t2;

    /**
     * @var string $mardi_t3
     *
     * @ORM\Column(name="mardi_t3", type="string", length=255, nullable=true)
     */
    private $mardi_t3;

    /**
     * @var string $mardi_t4
     *
     * @ORM\Column(name="mardi_t4", type="string", length=255, nullable=true)
     */
    private $mardi_t4;

    /**
     * @var string $mardi_t5
     *
     * @ORM\Column(name="mardi_t5", type="string", length=255, nullable=true)
     */
    private $mardi_t5;
	
	/**
     * @var string $mardi_t5com
     *
     * @ORM\Column(name="mardi_t5com", type="string", length=25, nullable=true)
     */
    private $mardi_t5com;

    /**
     * @var string $mardi_t6
     *
     * @ORM\Column(name="mardi_t6", type="string", length=255, nullable=true)
     */
    private $mardi_t6;
	
	/**
     * @var string $mardi_t6com
     *
     * @ORM\Column(name="mardi_t6com", type="string", length=25, nullable=true)
     */
    private $mardi_t6com;

    /**
     * @var string $mardi_t7
     *
     * @ORM\Column(name="mardi_t7", type="string", length=255, nullable=true)
     */
    private $mardi_t7;
	
	/**
     * @var string $mardi_t7com
     *
     * @ORM\Column(name="mardi_t7com", type="string", length=25, nullable=true)
     */
    private $mardi_t7com;

    /**
     * @var string $mardi_t8
     *
     * @ORM\Column(name="mardi_t8", type="string", length=255, nullable=true)
     */
    private $mardi_t8;
	
	/**
     * @var string $mardi_t8com
     *
     * @ORM\Column(name="mardi_t8com", type="string", length=25, nullable=true)
     */
    private $mardi_t8com;

    /**
     * @var string $mardi_t9
     *
     * @ORM\Column(name="mardi_t9", type="string", length=255, nullable=true)
     */
    private $mardi_t9;
	
	/**
     * @var string $mardi_t9com
     *
     * @ORM\Column(name="mardi_t9com", type="string", length=25, nullable=true)
     */
    private $mardi_t9com;

    /**
     * @var string $mardi_t10
     *
     * @ORM\Column(name="mardi_t10", type="string", length=255, nullable=true)
     */
    private $mardi_t10;
	
	/**
     * @var string $mardi_t10com
     *
     * @ORM\Column(name="mardi_t10com", type="string", length=25, nullable=true)
     */
    private $mardi_t10com;

    /**
     * @var string $mercredi_t1
     *
     * @ORM\Column(name="mercredi_t1", type="string", length=255, nullable=true)
     */
    private $mercredi_t1;

    /**
     * @var string $mercredi_t2
     *
     * @ORM\Column(name="mercredi_t2", type="string", length=255, nullable=true)
     */
    private $mercredi_t2;

    /**
     * @var string $mercredi_t3
     *
     * @ORM\Column(name="mercredi_t3", type="string", length=255, nullable=true)
     */
    private $mercredi_t3;

    /**
     * @var string $mercredi_t4
     *
     * @ORM\Column(name="mercredi_t4", type="string", length=255, nullable=true)
     */
    private $mercredi_t4;

    /**
     * @var string $mercredi_t5
     *
     * @ORM\Column(name="mercredi_t5", type="string", length=255, nullable=true)
     */
    private $mercredi_t5;
	
	/**
     * @var string $mercredi_t5com
     *
     * @ORM\Column(name="mercredi_t5com", type="string", length=25, nullable=true)
     */
    private $mercredi_t5com;

    /**
     * @var string $mercredi_t6
     *
     * @ORM\Column(name="mercredi_t6", type="string", length=255, nullable=true)
     */
    private $mercredi_t6;
	
	/**
     * @var string $mercredi_t6com
     *
     * @ORM\Column(name="mercredi_t6com", type="string", length=25, nullable=true)
     */
    private $mercredi_t6com;

    /**
     * @var string $mercredi_t7
     *
     * @ORM\Column(name="mercredi_t7", type="string", length=255, nullable=true)
     */
    private $mercredi_t7;
	
	/**
     * @var string $mercredi_t7com
     *
     * @ORM\Column(name="mercredi_t7com", type="string", length=25, nullable=true)
     */
    private $mercredi_t7com;

    /**
     * @var string $mercredi_t8
     *
     * @ORM\Column(name="mercredi_t8", type="string", length=255, nullable=true)
     */
    private $mercredi_t8;
	
	/**
     * @var string $mercredi_t8com
     *
     * @ORM\Column(name="mercredi_t8com", type="string", length=25, nullable=true)
     */
    private $mercredi_t8com;

    /**
     * @var string $mercredi_t9
     *
     * @ORM\Column(name="mercredi_t9", type="string", length=255, nullable=true)
     */
    private $mercredi_t9;
	
	/**
     * @var string $mercredi_t9com
     *
     * @ORM\Column(name="mercredi_t9com", type="string", length=25, nullable=true)
     */
    private $mercredi_t9com;

    /**
     * @var string $mercredi_t10
     *
     * @ORM\Column(name="mercredi_t10", type="string", length=255, nullable=true)
     */
    private $mercredi_t10;
	
	/**
     * @var string $mercredi_t10com
     *
     * @ORM\Column(name="mercredi_t10com", type="string", length=25, nullable=true)
     */
    private $mercredi_t10com;

    /**
     * @var string $jeudi_t1
     *
     * @ORM\Column(name="jeudi_t1", type="string", length=255, nullable=true)
     */
    private $jeudi_t1;

    /**
     * @var string $jeudi_t2
     *
     * @ORM\Column(name="jeudi_t2", type="string", length=255, nullable=true)
     */
    private $jeudi_t2;

    /**
     * @var string $jeudi_t3
     *
     * @ORM\Column(name="jeudi_t3", type="string", length=255, nullable=true)
     */
    private $jeudi_t3;

    /**
     * @var string $jeudi_t4
     *
     * @ORM\Column(name="jeudi_t4", type="string", length=255, nullable=true)
     */
    private $jeudi_t4;

    /**
     * @var string $jeudi_t5
     *
     * @ORM\Column(name="jeudi_t5", type="string", length=255, nullable=true)
     */
    private $jeudi_t5;
	
	/**
     * @var string $jeudi_t5com
     *
     * @ORM\Column(name="jeudi_t5com", type="string", length=25, nullable=true)
     */
    private $jeudi_t5com;

    /**
     * @var string $jeudi_t6
     *
     * @ORM\Column(name="jeudi_t6", type="string", length=255, nullable=true)
     */
    private $jeudi_t6;
	
	/**
     * @var string $jeudi_t6com
     *
     * @ORM\Column(name="jeudi_t6com", type="string", length=25, nullable=true)
     */
    private $jeudi_t6com;

    /**
     * @var string $jeudi_t7
     *
     * @ORM\Column(name="jeudi_t7", type="string", length=255, nullable=true)
     */
    private $jeudi_t7;
	
	/**
     * @var string $jeudi_t7com
     *
     * @ORM\Column(name="jeudi_t7com", type="string", length=25, nullable=true)
     */
    private $jeudi_t7com;

    /**
     * @var string $jeudi_t8
     *
     * @ORM\Column(name="jeudi_t8", type="string", length=255, nullable=true)
     */
    private $jeudi_t8;
	
	/**
     * @var string $jeudi_t8com
     *
     * @ORM\Column(name="jeudi_t8com", type="string", length=25, nullable=true)
     */
    private $jeudi_t8com;

    /**
     * @var string $jeudi_t9
     *
     * @ORM\Column(name="jeudi_t9", type="string", length=255, nullable=true)
     */
    private $jeudi_t9;
	
	/**
     * @var string $jeudi_t9com
     *
     * @ORM\Column(name="jeudi_t9com", type="string", length=25, nullable=true)
     */
    private $jeudi_t9com;

    /**
     * @var string $jeudi_t10
     *
     * @ORM\Column(name="jeudi_t10", type="string", length=255, nullable=true)
     */
    private $jeudi_t10;
	
	/**
     * @var string $jeudi_t10com
     *
     * @ORM\Column(name="jeudi_t10com", type="string", length=25, nullable=true)
     */
    private $jeudi_t10com;

    /**
     * @var string $vendredi_t1
     *
     * @ORM\Column(name="vendredi_t1", type="string", length=255, nullable=true)
     */
    private $vendredi_t1;

    /**
     * @var string $vendredi_t2
     *
     * @ORM\Column(name="vendredi_t2", type="string", length=255, nullable=true)
     */
    private $vendredi_t2;

    /**
     * @var string $vendredi_t3
     *
     * @ORM\Column(name="vendredi_t3", type="string", length=255, nullable=true)
     */
    private $vendredi_t3;

    /**
     * @var string $vendredi_t4
     *
     * @ORM\Column(name="vendredi_t4", type="string", length=255, nullable=true)
     */
    private $vendredi_t4;

    /**
     * @var string $vendredi_t5
     *
     * @ORM\Column(name="vendredi_t5", type="string", length=255, nullable=true)
     */
    private $vendredi_t5;
	
	/**
     * @var string $vendredi_t5com
     *
     * @ORM\Column(name="vendredi_t5com", type="string", length=25, nullable=true)
     */
    private $vendredi_t5com;

    /**
     * @var string $vendredi_t6
     *
     * @ORM\Column(name="vendredi_t6", type="string", length=255, nullable=true)
     */
    private $vendredi_t6;
	
	/**
     * @var string $vendredi_t6com
     *
     * @ORM\Column(name="vendredi_t6com", type="string", length=25, nullable=true)
     */
    private $vendredi_t6com;

    /**
     * @var string $vendredi_t7
     *
     * @ORM\Column(name="vendredi_t7", type="string", length=255, nullable=true)
     */
    private $vendredi_t7;
	
	/**
     * @var string $vendredi_t7com
     *
     * @ORM\Column(name="vendredi_t7com", type="string", length=25, nullable=true)
     */
    private $vendredi_t7com;

    /**
     * @var string $vendredi_t8
     *
     * @ORM\Column(name="vendredi_t8", type="string", length=255, nullable=true)
     */
    private $vendredi_t8;
	
	/**
     * @var string $vendredi_t8com
     *
     * @ORM\Column(name="vendredi_t8com", type="string", length=25, nullable=true)
     */
    private $vendredi_t8com;

    /**
     * @var string $vendredi_t9
     *
     * @ORM\Column(name="vendredi_t9", type="string", length=255, nullable=true)
     */
    private $vendredi_t9;
	
	/**
     * @var string $vendredi_t9com
     *
     * @ORM\Column(name="vendredi_t9com", type="string", length=25, nullable=true)
     */
    private $vendredi_t9com;

    /**
     * @var string $vendredi_t10
     *
     * @ORM\Column(name="vendredi_t10", type="string", length=255, nullable=true)
     */
    private $vendredi_t10;
	
	/**
     * @var string $vendredi_t10com
     *
     * @ORM\Column(name="vendredi_t10com", type="string", length=25, nullable=true)
     */
    private $vendredi_t10com;

    /**
     * @var string $samedi_t1
     *
     * @ORM\Column(name="samedi_t1", type="string", length=255, nullable=true)
     */
    private $samedi_t1;

    /**
     * @var string $samedi_t2
     *
     * @ORM\Column(name="samedi_t2", type="string", length=255, nullable=true)
     */
    private $samedi_t2;

    /**
     * @var string $samedi_t3
     *
     * @ORM\Column(name="samedi_t3", type="string", length=255, nullable=true)
     */
    private $samedi_t3;

    /**
     * @var string $samedi_t4
     *
     * @ORM\Column(name="samedi_t4", type="string", length=255, nullable=true)
     */
    private $samedi_t4;

    /**
     * @var string $samedi_t5
     *
     * @ORM\Column(name="samedi_t5", type="string", length=255, nullable=true)
     */
    private $samedi_t5;
	
	/**
     * @var string $samedi_t5com
     *
     * @ORM\Column(name="samedi_t5com", type="string", length=25, nullable=true)
     */
    private $samedi_t5com;

    /**
     * @var string $samedi_t6
     *
     * @ORM\Column(name="samedi_t6", type="string", length=255, nullable=true)
     */
    private $samedi_t6;
	
	/**
     * @var string $samedi_t6com
     *
     * @ORM\Column(name="samedi_t6com", type="string", length=25, nullable=true)
     */
    private $samedi_t6com;

    /**
     * @var string $samedi_t7
     *
     * @ORM\Column(name="samedi_t7", type="string", length=255, nullable=true)
     */
    private $samedi_t7;
	
	/**
     * @var string $samedi_t7com
     *
     * @ORM\Column(name="samedi_t7com", type="string", length=25, nullable=true)
     */
    private $samedi_t7com;

    /**
     * @var string $samedi_t8
     *
     * @ORM\Column(name="samedi_t8", type="string", length=255, nullable=true)
     */
    private $samedi_t8;
	
	/**
     * @var string $samedi_t8com
     *
     * @ORM\Column(name="samedi_t8com", type="string", length=25, nullable=true)
     */
    private $samedi_t8com;

    /**
     * @var string $samedi_t9
     *
     * @ORM\Column(name="samedi_t9", type="string", length=255, nullable=true)
     */
    private $samedi_t9;
	
	/**
     * @var string $samedi_t9com
     *
     * @ORM\Column(name="samedi_t9com", type="string", length=25, nullable=true)
     */
    private $samedi_t9com;

    /**
     * @var string $samedi_t10
     *
     * @ORM\Column(name="samedi_t10", type="string", length=255, nullable=true)
     */
    private $samedi_t10;
	
	/**
     * @var string $samedi_t10com
     *
     * @ORM\Column(name="samedi_t10com", type="string", length=25, nullable=true)
     */
    private $samedi_t10com;

    /**
     * @var time $temps_nr
     *
     * @ORM\Column(name="temps_nr", type="string", length=255, nullable=true)
     */
    private $temps_nr;

    /**
     * @var time $temps_cv
     *
     * @ORM\Column(name="temps_cv", type="string", length=255, nullable=true)
     */
    private $temps_cv;

    /**
     * @var string $temps_nf
     *
     * @ORM\Column(name="temps_nf", type="string", length=255, nullable=true)
     */
    private $temps_nf;

    /**
     * @var date $date_maj
     *
     * @ORM\Column(name="date_maj", type="date")
     */
    private $date_maj;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set lundi_t1
     *
     * @param string $lundiT1
     */
    public function setLundiT1($lundiT1)
    {
        $this->lundi_t1 = $lundiT1;
    }

    /**
     * Get lundi_t1
     *
     * @return string 
     */
    public function getLundiT1()
    {
        return $this->lundi_t1;
    }

    /**
     * Set lundi_t2
     *
     * @param string $lundiT2
     */
    public function setLundiT2($lundiT2)
    {
        $this->lundi_t2 = $lundiT2;
    }

    /**
     * Get lundi_t2
     *
     * @return string 
     */
    public function getLundiT2()
    {
        return $this->lundi_t2;
    }

    /**
     * Set lundi_t3
     *
     * @param string $lundiT3
     */
    public function setLundiT3($lundiT3)
    {
        $this->lundi_t3 = $lundiT3;
    }

    /**
     * Get lundi_t3
     *
     * @return string 
     */
    public function getLundiT3()
    {
        return $this->lundi_t3;
    }

    /**
     * Set lundi_t4
     *
     * @param string $lundiT4
     */
    public function setLundiT4($lundiT4)
    {
        $this->lundi_t4 = $lundiT4;
    }

    /**
     * Get lundi_t4
     *
     * @return string 
     */
    public function getLundiT4()
    {
        return $this->lundi_t4;
    }

    /**
     * Set lundi_t5
     *
     * @param string $lundiT5
     */
    public function setLundiT5($lundiT5)
    {
        $this->lundi_t5 = $lundiT5;
    }

    /**
     * Get lundi_t5
     *
     * @return string 
     */
    public function getLundiT5()
    {
        return $this->lundi_t5;
    }
	
	/**
     * Set lundi_t5com
     *
     * @param string $lundiT5com
     */
    public function setLundiT5com($lundiT5com)
    {
        $this->lundi_t5com = $lundiT5com;
    }

    /**
     * Get lundi_t5com
     *
     * @return string 
     */
    public function getLundiT5com()
    {
        return $this->lundi_t5com;
    }

    /**
     * Set lundi_t6
     *
     * @param string $lundiT6
     */
    public function setLundiT6($lundiT6)
    {
        $this->lundi_t6 = $lundiT6;
    }

    /**
     * Get lundi_t6
     *
     * @return string 
     */
    public function getLundiT6()
    {
        return $this->lundi_t6;
    }
	
	/**
     * Set lundi_t6com
     *
     * @param string $lundiT6com
     */
    public function setLundiT6com($lundiT6com)
    {
        $this->lundi_t6com = $lundiT6com;
    }

    /**
     * Get lundi_t6com
     *
     * @return string 
     */
    public function getLundiT6com()
    {
        return $this->lundi_t6com;
    }

    /**
     * Set lundi_t7
     *
     * @param string $lundiT7
     */
    public function setLundiT7($lundiT7)
    {
        $this->lundi_t7 = $lundiT7;
    }

    /**
     * Get lundi_t7
     *
     * @return string 
     */
    public function getLundiT7()
    {
        return $this->lundi_t7;
    }
	
	/**
     * Set lundi_t7com
     *
     * @param string $lundiT7com
     */
    public function setLundiT7com($lundiT7com)
    {
        $this->lundi_t7com = $lundiT7com;
    }

    /**
     * Get lundi_t7com
     *
     * @return string 
     */
    public function getLundiT7com()
    {
        return $this->lundi_t7com;
    }

    /**
     * Set lundi_t8
     *
     * @param string $lundiT8
     */
    public function setLundiT8($lundiT8)
    {
        $this->lundi_t8 = $lundiT8;
    }

    /**
     * Get lundi_t8
     *
     * @return string 
     */
    public function getLundiT8()
    {
        return $this->lundi_t8;
    }
	
	/**
     * Set lundi_t8com
     *
     * @param string $lundiT8com
     */
    public function setLundiT8com($lundiT8com)
    {
        $this->lundi_t8com = $lundiT8com;
    }

    /**
     * Get lundi_t8com
     *
     * @return string 
     */
    public function getLundiT8com()
    {
        return $this->lundi_t8com;
    }

    /**
     * Set lundi_t9
     *
     * @param string $lundiT9
     */
    public function setLundiT9($lundiT9)
    {
        $this->lundi_t9 = $lundiT9;
    }

    /**
     * Get lundi_t9
     *
     * @return string 
     */
    public function getLundiT9()
    {
        return $this->lundi_t9;
    }
	
	/**
     * Set lundi_t9com
     *
     * @param string $lundiT9com
     */
    public function setLundiT9com($lundiT9com)
    {
        $this->lundi_t9com = $lundiT9com;
    }

    /**
     * Get lundi_t9com
     *
     * @return string 
     */
    public function getLundiT9com()
    {
        return $this->lundi_t9com;
    }

    /**
     * Set lundi_t10
     *
     * @param string $lundiT10
     */
    public function setLundiT10($lundiT10)
    {
        $this->lundi_t10 = $lundiT10;
    }

    /**
     * Get lundi_t10
     *
     * @return string 
     */
    public function getLundiT10()
    {
        return $this->lundi_t10;
    }
	
	/**
     * Set lundi_t10com
     *
     * @param string $lundiT10com
     */
    public function setLundiT10com($lundiT10com)
    {
        $this->lundi_t10com = $lundiT10com;
    }

    /**
     * Get lundi_t10com
     *
     * @return string 
     */
    public function getLundiT10com()
    {
        return $this->lundi_t10com;
    }

    /**
     * Set mardi_t1
     *
     * @param string $mardiT1
     */
    public function setMardiT1($mardiT1)
    {
        $this->mardi_t1 = $mardiT1;
    }

    /**
     * Get mardi_t1
     *
     * @return string 
     */
    public function getMardiT1()
    {
        return $this->mardi_t1;
    }

    /**
     * Set mardi_t2
     *
     * @param string $mardiT2
     */
    public function setMardiT2($mardiT2)
    {
        $this->mardi_t2 = $mardiT2;
    }

    /**
     * Get mardi_t2
     *
     * @return string 
     */
    public function getMardiT2()
    {
        return $this->mardi_t2;
    }

    /**
     * Set mardi_t3
     *
     * @param string $mardiT3
     */
    public function setMardiT3($mardiT3)
    {
        $this->mardi_t3 = $mardiT3;
    }

    /**
     * Get mardi_t3
     *
     * @return string 
     */
    public function getMardiT3()
    {
        return $this->mardi_t3;
    }

    /**
     * Set mardi_t4
     *
     * @param string $mardiT4
     */
    public function setMardiT4($mardiT4)
    {
        $this->mardi_t4 = $mardiT4;
    }

    /**
     * Get mardi_t4
     *
     * @return string 
     */
    public function getMardiT4()
    {
        return $this->mardi_t4;
    }

    /**
     * Set mardi_t5
     *
     * @param string $mardiT5
     */
    public function setMardiT5($mardiT5)
    {
        $this->mardi_t5 = $mardiT5;
    }

    /**
     * Get mardi_t5
     *
     * @return string 
     */
    public function getMardiT5()
    {
        return $this->mardi_t5;
    }
	
	/**
     * Set mardi_t5com
     *
     * @param string $mardiT5com
     */
    public function setMardiT5com($mardiT5com)
    {
        $this->mardi_t5com = $mardiT5com;
    }

    /**
     * Get mardi_t5com
     *
     * @return string 
     */
    public function getMardiT5com()
    {
        return $this->mardi_t5com;
    }

    /**
     * Set mardi_t6
     *
     * @param string $mardiT6
     */
    public function setMardiT6($mardiT6)
    {
        $this->mardi_t6 = $mardiT6;
    }

    /**
     * Get mardi_t6
     *
     * @return string 
     */
    public function getMardiT6()
    {
        return $this->mardi_t6;
    }
	
	/**
     * Set mardi_t6com
     *
     * @param string $mardiT6com
     */
    public function setMardiT6com($mardiT6com)
    {
        $this->mardi_t6com = $mardiT6com;
    }

    /**
     * Get mardi_t6com
     *
     * @return string 
     */
    public function getMardiT6com()
    {
        return $this->mardi_t6com;
    }

    /**
     * Set mardi_t7
     *
     * @param string $mardiT7
     */
    public function setMardiT7($mardiT7)
    {
        $this->mardi_t7 = $mardiT7;
    }

    /**
     * Get mardi_t7
     *
     * @return string 
     */
    public function getMardiT7()
    {
        return $this->mardi_t7;
    }
	
	/**
     * Set mardi_t7com
     *
     * @param string $mardiT7com
     */
    public function setMardiT7com($mardiT7com)
    {
        $this->mardi_t7com = $mardiT7com;
    }

    /**
     * Get mardi_t7com
     *
     * @return string 
     */
    public function getMardiT7com()
    {
        return $this->mardi_t7com;
    }

    /**
     * Set mardi_t8
     *
     * @param string $mardiT8
     */
    public function setMardiT8($mardiT8)
    {
        $this->mardi_t8 = $mardiT8;
    }

    /**
     * Get mardi_t8
     *
     * @return string 
     */
    public function getMardiT8()
    {
        return $this->mardi_t8;
    }
	
	/**
     * Set mardi_t8com
     *
     * @param string $mardiT8com
     */
    public function setMardiT8com($mardiT8com)
    {
        $this->mardi_t8com = $mardiT8com;
    }

    /**
     * Get mardi_t8com
     *
     * @return string 
     */
    public function getMardiT8com()
    {
        return $this->mardi_t8com;
    }

    /**
     * Set mardi_t9
     *
     * @param string $mardiT9
     */
    public function setMardiT9($mardiT9)
    {
        $this->mardi_t9 = $mardiT9;
    }

    /**
     * Get mardi_t9
     *
     * @return string 
     */
    public function getMardiT9()
    {
        return $this->mardi_t9;
    }
	
	/**
     * Set mardi_t9com
     *
     * @param string $mardiT9com
     */
    public function setMardiT9com($mardiT9com)
    {
        $this->mardi_t9com = $mardiT9com;
    }

    /**
     * Get mardi_t9com
     *
     * @return string 
     */
    public function getMardiT9com()
    {
        return $this->mardi_t9com;
    }

    /**
     * Set mardi_t10
     *
     * @param string $mardiT10
     */
    public function setMardiT10($mardiT10)
    {
        $this->mardi_t10 = $mardiT10;
    }

    /**
     * Get mardi_t10
     *
     * @return string 
     */
    public function getMardiT10()
    {
        return $this->mardi_t10;
    }
	
	/**
     * Set mardi_t10com
     *
     * @param string $mardiT10com
     */
    public function setMardiT10com($mardiT10com)
    {
        $this->mardi_t10com = $mardiT10com;
    }

    /**
     * Get mardi_t10com
     *
     * @return string 
     */
    public function getMardiT10com()
    {
        return $this->mardi_t10com;
    }

    /**
     * Set mercredi_t1
     *
     * @param string $mercrediT1
     */
    public function setMercrediT1($mercrediT1)
    {
        $this->mercredi_t1 = $mercrediT1;
    }

    /**
     * Get mercredi_t1
     *
     * @return string 
     */
    public function getMercrediT1()
    {
        return $this->mercredi_t1;
    }

    /**
     * Set mercredi_t2
     *
     * @param string $mercrediT2
     */
    public function setMercrediT2($mercrediT2)
    {
        $this->mercredi_t2 = $mercrediT2;
    }

    /**
     * Get mercredi_t2
     *
     * @return string 
     */
    public function getMercrediT2()
    {
        return $this->mercredi_t2;
    }

    /**
     * Set mercredi_t3
     *
     * @param string $mercrediT3
     */
    public function setMercrediT3($mercrediT3)
    {
        $this->mercredi_t3 = $mercrediT3;
    }

    /**
     * Get mercredi_t3
     *
     * @return string 
     */
    public function getMercrediT3()
    {
        return $this->mercredi_t3;
    }

    /**
     * Set mercredi_t4
     *
     * @param string $mercrediT4
     */
    public function setMercrediT4($mercrediT4)
    {
        $this->mercredi_t4 = $mercrediT4;
    }

    /**
     * Get mercredi_t4
     *
     * @return string 
     */
    public function getMercrediT4()
    {
        return $this->mercredi_t4;
    }

    /**
     * Set mercredi_t5
     *
     * @param string $mercrediT5
     */
    public function setMercrediT5($mercrediT5)
    {
        $this->mercredi_t5 = $mercrediT5;
    }

    /**
     * Get mercredi_t5
     *
     * @return string 
     */
    public function getMercrediT5()
    {
        return $this->mercredi_t5;
    }
	
	/**
     * Set mercredi_t5com
     *
     * @param string $mercrediT5com
     */
    public function setMercrediT5com($mercrediT5com)
    {
        $this->mercredi_t5com = $mercrediT5com;
    }

    /**
     * Get mercredi_t5com
     *
     * @return string 
     */
    public function getMercrediT5com()
    {
        return $this->mercredi_t5com;
    }

    /**
     * Set mercredi_t6
     *
     * @param string $mercrediT6
     */
    public function setMercrediT6($mercrediT6)
    {
        $this->mercredi_t6 = $mercrediT6;
    }

    /**
     * Get mercredi_t6
     *
     * @return string 
     */
    public function getMercrediT6()
    {
        return $this->mercredi_t6;
    }
	
	/**
     * Set mercredi_t6com
     *
     * @param string $mercrediT6com
     */
    public function setMercrediT6com($mercrediT6com)
    {
        $this->mercredi_t6com = $mercrediT6com;
    }

    /**
     * Get mercredi_t6com
     *
     * @return string 
     */
    public function getMercrediT6com()
    {
        return $this->mercredi_t6com;
    }

    /**
     * Set mercredi_t7
     *
     * @param string $mercrediT7
     */
    public function setMercrediT7($mercrediT7)
    {
        $this->mercredi_t7 = $mercrediT7;
    }

    /**
     * Get mercredi_t7
     *
     * @return string 
     */
    public function getMercrediT7()
    {
        return $this->mercredi_t7;
    }
	
	/**
     * Set mercredi_t7com
     *
     * @param string $mercrediT7com
     */
    public function setMercrediT7com($mercrediT7com)
    {
        $this->mercredi_t7com = $mercrediT7com;
    }

    /**
     * Get mercredi_t7com
     *
     * @return string 
     */
    public function getMercrediT7com()
    {
        return $this->mercredi_t7com;
    }

    /**
     * Set mercredi_t8
     *
     * @param string $mercrediT8
     */
    public function setMercrediT8($mercrediT8)
    {
        $this->mercredi_t8 = $mercrediT8;
    }

    /**
     * Get mercredi_t8
     *
     * @return string 
     */
    public function getMercrediT8()
    {
        return $this->mercredi_t8;
    }
	
	/**
     * Set mercredi_t8com
     *
     * @param string $mercrediT8com
     */
    public function setMercrediT8com($mercrediT8com)
    {
        $this->mercredi_t8com = $mercrediT8com;
    }

    /**
     * Get mercredi_t8com
     *
     * @return string 
     */
    public function getMercrediT8com()
    {
        return $this->mercredi_t8com;
    }

    /**
     * Set mercredi_t9
     *
     * @param string $mercrediT9
     */
    public function setMercrediT9($mercrediT9)
    {
        $this->mercredi_t9 = $mercrediT9;
    }

    /**
     * Get mercredi_t9
     *
     * @return string 
     */
    public function getMercrediT9()
    {
        return $this->mercredi_t9;
    }
	
	/**
     * Set mercredi_t9com
     *
     * @param string $mercrediT9com
     */
    public function setMercrediT9com($mercrediT9com)
    {
        $this->mercredi_t9com = $mercrediT9com;
    }

    /**
     * Get mercredi_t9com
     *
     * @return string 
     */
    public function getMercrediT9com()
    {
        return $this->mercredi_t9com;
    }

    /**
     * Set mercredi_t10
     *
     * @param string $mercrediT10
     */
    public function setMercrediT10($mercrediT10)
    {
        $this->mercredi_t10 = $mercrediT10;
    }

    /**
     * Get mercredi_t10
     *
     * @return string 
     */
    public function getMercrediT10()
    {
        return $this->mercredi_t10;
    }
	
	/**
     * Set mercredi_t10com
     *
     * @param string $mercrediT10com
     */
    public function setMercrediT10com($mercrediT10com)
    {
        $this->mercredi_t10com = $mercrediT10com;
    }

    /**
     * Get mercredi_t10com
     *
     * @return string 
     */
    public function getMercrediT10com()
    {
        return $this->mercredi_t10com;
    }

    /**
     * Set jeudi_t1
     *
     * @param string $jeudiT1
     */
    public function setJeudiT1($jeudiT1)
    {
        $this->jeudi_t1 = $jeudiT1;
    }

    /**
     * Get jeudi_t1
     *
     * @return string 
     */
    public function getJeudiT1()
    {
        return $this->jeudi_t1;
    }

    /**
     * Set jeudi_t2
     *
     * @param string $jeudiT2
     */
    public function setJeudiT2($jeudiT2)
    {
        $this->jeudi_t2 = $jeudiT2;
    }

    /**
     * Get jeudi_t2
     *
     * @return string 
     */
    public function getJeudiT2()
    {
        return $this->jeudi_t2;
    }

    /**
     * Set jeudi_t3
     *
     * @param string $jeudiT3
     */
    public function setJeudiT3($jeudiT3)
    {
        $this->jeudi_t3 = $jeudiT3;
    }

    /**
     * Get jeudi_t3
     *
     * @return string 
     */
    public function getJeudiT3()
    {
        return $this->jeudi_t3;
    }

    /**
     * Set jeudi_t4
     *
     * @param string $jeudiT4
     */
    public function setJeudiT4($jeudiT4)
    {
        $this->jeudi_t4 = $jeudiT4;
    }

    /**
     * Get jeudi_t4
     *
     * @return string 
     */
    public function getJeudiT4()
    {
        return $this->jeudi_t4;
    }

    /**
     * Set jeudi_t5
     *
     * @param string $jeudiT5
     */
    public function setJeudiT5($jeudiT5)
    {
        $this->jeudi_t5 = $jeudiT5;
    }

    /**
     * Get jeudi_t5
     *
     * @return string 
     */
    public function getJeudiT5()
    {
        return $this->jeudi_t5;
    }
	
	/**
     * Set jeudi_t5com
     *
     * @param string $jeudiT5com
     */
    public function setJeudiT5com($jeudiT5com)
    {
        $this->jeudi_t5com = $jeudiT5com;
    }

    /**
     * Get jeudi_t5com
     *
     * @return string 
     */
    public function getJeudiT5com()
    {
        return $this->jeudi_t5com;
    }

    /**
     * Set jeudi_t6
     *
     * @param string $jeudiT6
     */
    public function setJeudiT6($jeudiT6)
    {
        $this->jeudi_t6 = $jeudiT6;
    }

    /**
     * Get jeudi_t6
     *
     * @return string 
     */
    public function getJeudiT6()
    {
        return $this->jeudi_t6;
    }
	
	/**
     * Set jeudi_t6com
     *
     * @param string $jeudiT6com
     */
    public function setJeudiT6com($jeudiT6com)
    {
        $this->jeudi_t6com = $jeudiT6com;
    }

    /**
     * Get jeudi_t6com
     *
     * @return string 
     */
    public function getJeudiT6com()
    {
        return $this->jeudi_t6com;
    }

    /**
     * Set jeudi_t7
     *
     * @param string $jeudiT7
     */
    public function setJeudiT7($jeudiT7)
    {
        $this->jeudi_t7 = $jeudiT7;
    }

    /**
     * Get jeudi_t7
     *
     * @return string 
     */
    public function getJeudiT7()
    {
        return $this->jeudi_t7;
    }
	
	/**
     * Set jeudi_t7com
     *
     * @param string $jeudiT7com
     */
    public function setJeudiT7com($jeudiT7com)
    {
        $this->jeudi_t7com = $jeudiT7com;
    }

    /**
     * Get jeudi_t7com
     *
     * @return string 
     */
    public function getJeudiT7com()
    {
        return $this->jeudi_t7com;
    }

    /**
     * Set jeudi_t8
     *
     * @param string $jeudiT8
     */
    public function setJeudiT8($jeudiT8)
    {
        $this->jeudi_t8 = $jeudiT8;
    }

    /**
     * Get jeudi_t8
     *
     * @return string 
     */
    public function getJeudiT8()
    {
        return $this->jeudi_t8;
    }
	
	/**
     * Set jeudi_t8com
     *
     * @param string $jeudiT8com
     */
    public function setJeudiT8com($jeudiT8com)
    {
        $this->jeudi_t8com = $jeudiT8com;
    }

    /**
     * Get jeudi_t8com
     *
     * @return string 
     */
    public function getJeudiT8com()
    {
        return $this->jeudi_t8com;
    }

    /**
     * Set jeudi_t9
     *
     * @param string $jeudiT9
     */
    public function setJeudiT9($jeudiT9)
    {
        $this->jeudi_t9 = $jeudiT9;
    }

    /**
     * Get jeudi_t9
     *
     * @return string 
     */
    public function getJeudiT9()
    {
        return $this->jeudi_t9;
    }
	
	/**
     * Set jeudi_t9com
     *
     * @param string $jeudiT9com
     */
    public function setJeudiT9com($jeudiT9com)
    {
        $this->jeudi_t9com = $jeudiT9com;
    }

    /**
     * Get jeudi_t9com
     *
     * @return string 
     */
    public function getJeudiT9com()
    {
        return $this->jeudi_t9com;
    }

    /**
     * Set jeudi_t10
     *
     * @param string $jeudiT10
     */
    public function setJeudiT10($jeudiT10)
    {
        $this->jeudi_t10 = $jeudiT10;
    }

    /**
     * Get jeudi_t10
     *
     * @return string 
     */
    public function getJeudiT10()
    {
        return $this->jeudi_t10;
    }
	
	/**
     * Set jeudi_t10com
     *
     * @param string $jeudiT10com
     */
    public function setJeudiT10com($jeudiT10com)
    {
        $this->jeudi_t10com = $jeudiT10com;
    }

    /**
     * Get jeudi_t10com
     *
     * @return string 
     */
    public function getJeudiT10com()
    {
        return $this->jeudi_t10com;
    }

    /**
     * Set vendredi_t1
     *
     * @param string $vendrediT1
     */
    public function setVendrediT1($vendrediT1)
    {
        $this->vendredi_t1 = $vendrediT1;
    }

    /**
     * Get vendredi_t1
     *
     * @return string 
     */
    public function getVendrediT1()
    {
        return $this->vendredi_t1;
    }

    /**
     * Set vendredi_t2
     *
     * @param string $vendrediT2
     */
    public function setVendrediT2($vendrediT2)
    {
        $this->vendredi_t2 = $vendrediT2;
    }

    /**
     * Get vendredi_t2
     *
     * @return string 
     */
    public function getVendrediT2()
    {
        return $this->vendredi_t2;
    }

    /**
     * Set vendredi_t3
     *
     * @param string $vendrediT3
     */
    public function setVendrediT3($vendrediT3)
    {
        $this->vendredi_t3 = $vendrediT3;
    }

    /**
     * Get vendredi_t3
     *
     * @return string 
     */
    public function getVendrediT3()
    {
        return $this->vendredi_t3;
    }

    /**
     * Set vendredi_t4
     *
     * @param string $vendrediT4
     */
    public function setVendrediT4($vendrediT4)
    {
        $this->vendredi_t4 = $vendrediT4;
    }

    /**
     * Get vendredi_t4
     *
     * @return string 
     */
    public function getVendrediT4()
    {
        return $this->vendredi_t4;
    }

    /**
     * Set vendredi_t5
     *
     * @param string $vendrediT5
     */
    public function setVendrediT5($vendrediT5)
    {
        $this->vendredi_t5 = $vendrediT5;
    }

    /**
     * Get vendredi_t5
     *
     * @return string 
     */
    public function getVendrediT5()
    {
        return $this->vendredi_t5;
    }
	
	/**
     * Set vendredi_t5com
     *
     * @param string $vendrediT5com
     */
    public function setVendrediT5com($vendrediT5com)
    {
        $this->vendredi_t5com = $vendrediT5com;
    }

    /**
     * Get vendredi_t5com
     *
     * @return string 
     */
    public function getVendrediT5com()
    {
        return $this->vendredi_t5com;
    }

    /**
     * Set vendredi_t6
     *
     * @param string $vendrediT6
     */
    public function setVendrediT6($vendrediT6)
    {
        $this->vendredi_t6 = $vendrediT6;
    }

    /**
     * Get vendredi_t6
     *
     * @return string 
     */
    public function getVendrediT6()
    {
        return $this->vendredi_t6;
    }
	
	/**
     * Set vendredi_t6com
     *
     * @param string $vendrediT6com
     */
    public function setVendrediT6com($vendrediT6com)
    {
        $this->vendredi_t6com = $vendrediT6com;
    }

    /**
     * Get vendredi_t6com
     *
     * @return string 
     */
    public function getVendrediT6com()
    {
        return $this->vendredi_t6com;
    }

    /**
     * Set vendredi_t7
     *
     * @param string $vendrediT7
     */
    public function setVendrediT7($vendrediT7)
    {
        $this->vendredi_t7 = $vendrediT7;
    }

    /**
     * Get vendredi_t7
     *
     * @return string 
     */
    public function getVendrediT7()
    {
        return $this->vendredi_t7;
    }
	
	/**
     * Set vendredi_t7com
     *
     * @param string $vendrediT7com
     */
    public function setVendrediT7com($vendrediT7com)
    {
        $this->vendredi_t7com = $vendrediT7com;
    }

    /**
     * Get vendredi_t7com
     *
     * @return string 
     */
    public function getVendrediT7com()
    {
        return $this->vendredi_t7com;
    }

    /**
     * Set vendredi_t8
     *
     * @param string $vendrediT8
     */
    public function setVendrediT8($vendrediT8)
    {
        $this->vendredi_t8 = $vendrediT8;
    }

    /**
     * Get vendredi_t8
     *
     * @return string 
     */
    public function getVendrediT8()
    {
        return $this->vendredi_t8;
    }
	
	/**
     * Set vendredi_t8com
     *
     * @param string $vendrediT8com
     */
    public function setVendrediT8com($vendrediT8com)
    {
        $this->vendredi_t8com = $vendrediT8com;
    }

    /**
     * Get vendredi_t8com
     *
     * @return string 
     */
    public function getVendrediT8com()
    {
        return $this->vendredi_t8com;
    }

    /**
     * Set vendredi_t9
     *
     * @param string $vendrediT9
     */
    public function setVendrediT9($vendrediT9)
    {
        $this->vendredi_t9 = $vendrediT9;
    }

    /**
     * Get vendredi_t9
     *
     * @return string 
     */
    public function getVendrediT9()
    {
        return $this->vendredi_t9;
    }
	
	/**
     * Set vendredi_t9com
     *
     * @param string $vendrediT9com
     */
    public function setVendrediT9com($vendrediT9com)
    {
        $this->vendredi_t9com = $vendrediT9com;
    }

    /**
     * Get vendredi_t9com
     *
     * @return string 
     */
    public function getVendrediT9com()
    {
        return $this->vendredi_t9com;
    }

    /**
     * Set vendredi_t10
     *
     * @param string $vendrediT10
     */
    public function setVendrediT10($vendrediT10)
    {
        $this->vendredi_t10 = $vendrediT10;
    }

    /**
     * Get vendredi_t10
     *
     * @return string 
     */
    public function getVendrediT10()
    {
        return $this->vendredi_t10;
    }
	
	/**
     * Set vendredi_t10com
     *
     * @param string $vendrediT10com
     */
    public function setVendrediT10com($vendrediT10com)
    {
        $this->vendredi_t10com = $vendrediT10com;
    }

    /**
     * Get vendredi_t10com
     *
     * @return string 
     */
    public function getVendrediT10com()
    {
        return $this->vendredi_t10com;
    }

    /**
     * Set samedi_t1
     *
     * @param string $samediT1
     */
    public function setSamediT1($samediT1)
    {
        $this->samedi_t1 = $samediT1;
    }

    /**
     * Get samedi_t1
     *
     * @return string 
     */
    public function getSamediT1()
    {
        return $this->samedi_t1;
    }

    /**
     * Set samedi_t2
     *
     * @param string $samediT2
     */
    public function setSamediT2($samediT2)
    {
        $this->samedi_t2 = $samediT2;
    }

    /**
     * Get samedi_t2
     *
     * @return string 
     */
    public function getSamediT2()
    {
        return $this->samedi_t2;
    }

    /**
     * Set samedi_t3
     *
     * @param string $samediT3
     */
    public function setSamediT3($samediT3)
    {
        $this->samedi_t3 = $samediT3;
    }

    /**
     * Get samedi_t3
     *
     * @return string 
     */
    public function getSamediT3()
    {
        return $this->samedi_t3;
    }

    /**
     * Set samedi_t4
     *
     * @param string $samediT4
     */
    public function setSamediT4($samediT4)
    {
        $this->samedi_t4 = $samediT4;
    }

    /**
     * Get samedi_t4
     *
     * @return string 
     */
    public function getSamediT4()
    {
        return $this->samedi_t4;
    }

    /**
     * Set samedi_t5
     *
     * @param string $samediT5
     */
    public function setSamediT5($samediT5)
    {
        $this->samedi_t5 = $samediT5;
    }

    /**
     * Get samedi_t5
     *
     * @return string 
     */
    public function getSamediT5()
    {
        return $this->samedi_t5;
    }
	
	/**
     * Set samedi_t5com
     *
     * @param string $samediT5com
     */
    public function setSamediT5com($samediT5com)
    {
        $this->samedi_t5com = $samediT5com;
    }

    /**
     * Get samedi_t5com
     *
     * @return string 
     */
    public function getSamediT5com()
    {
        return $this->samedi_t5com;
    }

    /**
     * Set samedi_t6
     *
     * @param string $samediT6
     */
    public function setSamediT6($samediT6)
    {
        $this->samedi_t6 = $samediT6;
    }

    /**
     * Get samedi_t6
     *
     * @return string 
     */
    public function getSamediT6()
    {
        return $this->samedi_t6;
    }
	
	/**
     * Set samedi_t6com
     *
     * @param string $samediT6com
     */
    public function setSamediT6com($samediT6com)
    {
        $this->samedi_t6com = $samediT6com;
    }

    /**
     * Get samedi_t6com
     *
     * @return string 
     */
    public function getSamediT6com()
    {
        return $this->samedi_t6com;
    }

    /**
     * Set samedi_t7
     *
     * @param string $samediT7
     */
    public function setSamediT7($samediT7)
    {
        $this->samedi_t7 = $samediT7;
    }

    /**
     * Get samedi_t7
     *
     * @return string 
     */
    public function getSamediT7()
    {
        return $this->samedi_t7;
    }
	
	/**
     * Set samedi_t7com
     *
     * @param string $samediT7com
     */
    public function setSamediT7com($samediT7com)
    {
        $this->samedi_t7com = $samediT7com;
    }

    /**
     * Get samedi_t7com
     *
     * @return string 
     */
    public function getSamediT7com()
    {
        return $this->samedi_t7com;
    }

    /**
     * Set samedi_t8
     *
     * @param string $samediT8
     */
    public function setSamediT8($samediT8)
    {
        $this->samedi_t8 = $samediT8;
    }

    /**
     * Get samedi_t8
     *
     * @return string 
     */
    public function getSamediT8()
    {
        return $this->samedi_t8;
    }
	
	/**
     * Set samedi_t8com
     *
     * @param string $samediT8com
     */
    public function setSamediT8com($samediT8com)
    {
        $this->samedi_t8com = $samediT8com;
    }

    /**
     * Get samedi_t8com
     *
     * @return string 
     */
    public function getSamediT8com()
    {
        return $this->samedi_t8com;
    }

    /**
     * Set samedi_t9
     *
     * @param string $samediT9
     */
    public function setSamediT9($samediT9)
    {
        $this->samedi_t9 = $samediT9;
    }

    /**
     * Get samedi_t9
     *
     * @return string 
     */
    public function getSamediT9()
    {
        return $this->samedi_t9;
    }
	
	/**
     * Set samedi_t9com
     *
     * @param string $samediT9com
     */
    public function setSamediT9com($samediT9com)
    {
        $this->samedi_t9com = $samediT9com;
    }

    /**
     * Get samedi_t9com
     *
     * @return string 
     */
    public function getSamediT9com()
    {
        return $this->samedi_t9com;
    }

    /**
     * Set samedi_t10
     *
     * @param string $samediT10
     */
    public function setSamediT10($samediT10)
    {
        $this->samedi_t10 = $samediT10;
    }

    /**
     * Get samedi_t10
     *
     * @return string 
     */
    public function getSamediT10()
    {
        return $this->samedi_t10;
    }
	
	/**
     * Set samedi_t10com
     *
     * @param string $samediT10com
     */
    public function setSamediT10com($samediT10com)
    {
        $this->samedi_t10com = $samediT10com;
    }

    /**
     * Get samedi_t10com
     *
     * @return string 
     */
    public function getSamediT10com()
    {
        return $this->samedi_t10com;
    }

    /**
     * Set temps_nr
     *
     * @param time $tempsNr
     */
    public function setTempsNr($tempsNr)
    {
        $this->temps_nr = $tempsNr;
    }

    /**
     * Get temps_nr
     *
     * @return time 
     */
    public function getTempsNr()
    {
        return $this->temps_nr;
    }

    /**
     * Set temps_cv
     *
     * @param time $tempsCv
     */
    public function setTempsCv($tempsCv)
    {
        $this->temps_cv = $tempsCv;
    }

    /**
     * Get temps_cv
     *
     * @return time 
     */
    public function getTempsCv()
    {
        return $this->temps_cv;
    }

    /**
     * Set temps_nf
     *
     * @param string $tempsNf
     */
    public function setTempsNf($tempsNf)
    {
        $this->temps_nf = $tempsNf;
    }

    /**
     * Get temps_nf
     *
     * @return string 
     */
    public function getTempsNf()
    {
        return $this->temps_nf;
    }

    /**
     * Set date_maj
     *
     * @param date $dateMaj
     */
    public function setDateMaj($dateMaj)
    {
        $this->date_maj = $dateMaj;
    }

    /**
     * Get date_maj
     *
     * @return date 
     */
    public function getDateMaj()
    {
        return $this->date_maj;
    }
	
	/**
     * Set lieux
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\lieux $lieux
     */
    public function setLieux(\Phareos\NomadeNetServiceBundle\Entity\lieux $lieux)
    {
        $this->lieux = $lieux;
    }
	
    /**
     * Get lieux
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\lieux 
     */
    public function getLieux()
    {
        return $this->lieux;
    }
	
	public function __construct()
	{
		$this->temps_nr = '45Min'; //temps nettoyage récurrent
		$this->temps_cv = '20Min'; //temps contrôle visuel
		$this->temps_nf = '1h40Min'; //temps nettoyage de fond
		$this->titre = 'Semaine 1';
		$this->lundi_t1 = 'Aspiration des sols';
		$this->lundi_t2 = 'Lavage des sols';
		$this->lundi_t3 = 'Sanitaires';
		$this->mardi_t1 = 'Aspiration des sols';
		$this->mardi_t2 = 'Lavage des sols';
		$this->mardi_t3 = 'Sanitaires';
		$this->mercredi_t1 = 'Aspiration des sols';
		$this->mercredi_t2 = 'Lavage des sols';
		$this->mercredi_t3 = 'Sanitaires';
		$this->jeudi_t1 = 'Aspiration des sols';
		$this->jeudi_t2 = 'Lavage des sols';
		$this->jeudi_t3 = 'Sanitaires';
		$this->vendredi_t1 = 'Aspiration des sols';
		$this->vendredi_t2 = 'Lavage des sols';
		$this->vendredi_t3 = 'Sanitaires';
		$this->samedi_t1 = 'Aspiration des sols';
		$this->samedi_t2 = 'Lavage des sols';
		$this->samedi_t3 = 'Sanitaires';
		$this->lundi_t5 = 'Linéaires';
		$this->lundi_t5com = 'Ex: 33 à 36 (Linéaires 33 à 36)';
	}
}