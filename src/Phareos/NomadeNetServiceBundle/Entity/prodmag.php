<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\NomadeNetServiceBundle\Entity\prodmag
 *
 * @ORM\Table(name="nom_prodmag")
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\prodmagRepository")
 */
class prodmag
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\OneToMany(targetEntity="audtech", mappedBy="prodmag", cascade={"remove", "persist"})
	 */
	protected $prodmaudtech;
	
	/**
	 * @ORM\ManyToOne(targetEntity="prod", inversedBy="prodprodmag", cascade={"remove"})
	 * @ORM\JoinColumn(name="prod_id", referencedColumnName="id")
	 */
	protected $prod;
	
	
	/**
	 * @ORM\ManyToOne(targetEntity="grand_compte", inversedBy="grdcpteprodmag", cascade={"remove"})
	 * @ORM\JoinColumn(name="grdcompte_id", referencedColumnName="id")
	 */
	protected $grdcompte;
	
	/**
	 * @ORM\ManyToOne(targetEntity="lieux", inversedBy="lieuxprodmag", cascade={"remove"})
	 * @ORM\JoinColumn(name="lieux_id", referencedColumnName="id")
	 */
	protected $lieux;
	
	/**
	 * @ORM\OneToMany(targetEntity="stockmag", mappedBy="prodmag", cascade={"remove", "persist"})
	 */
	protected $prodmagstock;

    /**
     * @var string $nom
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;
	
	
	/**
     * @var string $typemat
     *
     * @ORM\Column(name="typemat", type="string", length=255, nullable=true)
     */
    private $typemat;

    /**
     * @var integer $seuilmag
     *
     * @ORM\Column(name="seuilmag", type="integer")
     */
    private $seuilmag;

    /**
     * @var integer $seuilveh
     *
     * @ORM\Column(name="seuilveh", type="integer")
     */
    private $seuilveh;
	
	/**
     * @var integer $gcid2
     *
     * @ORM\Column(name="gcid2", type="integer", nullable=true)
     */
    private $gcid2;
	
	/**
     * @var integer $lieuxid2
     *
     * @ORM\Column(name="lieuxid2", type="integer", nullable=true)
     */
    private $lieuxid2;

	/**
     * @var boolean $archive
     *
     * @ORM\Column(name="archive", type="boolean", nullable=true)
     */
    private $archive;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }
	
	
	/**
     * Set typemat
     *
     * @param string $typemat
     */
    public function setTypemat($typemat)
    {
        $this->typemat = $typemat;
    }

    /**
     * Get typemat
     *
     * @return string 
     */
    public function getTypemat()
    {
        return $this->typemat;
    }

    /**
     * Set seuilmag
     *
     * @param integer $seuilmag
     */
    public function setSeuilmag($seuilmag)
    {
        $this->seuilmag = $seuilmag;
    }

    /**
     * Get seuilmag
     *
     * @return integer 
     */
    public function getSeuilmag()
    {
        return $this->seuilmag;
    }

    /**
     * Set seuilveh
     *
     * @param integer $seuilveh
     */
    public function setSeuilveh($seuilveh)
    {
        $this->seuilveh = $seuilveh;
    }

    /**
     * Get seuilveh
     *
     * @return integer 
     */
    public function getSeuilveh()
    {
        return $this->seuilveh;
    }
	
	/**
     * Set gcid2
     *
     * @param integer $gcid2
     */
    public function setGcid2($gcid2)
    {
        $this->gcid2 = $gcid2;
    }

    /**
     * Get gcid2
     *
     * @return integer 
     */
    public function getGcid2()
    {
        return $this->gcid2;
    }
	
	/**
     * Set lieuxid2
     *
     * @param integer $lieuxid2
     */
    public function setLieuxid2($lieuxid2)
    {
        $this->lieuxid2 = $lieuxid2;
    }

    /**
     * Get lieuxid2
     *
     * @return integer 
     */
    public function getLieuxid2()
    {
        return $this->lieuxid2;
    }
	
	/**
     * Set archive
     *
     * @param boolean $archive
     */
    public function setArchive($archive)
    {
        $this->archive = $archive;
    }

    /**
     * Get archive
     *
     * @return boolean 
     */
    public function getArchive()
    {
        return $this->archive;
    }
	
    public function __construct()
    {
        $this->prodmagstock = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set grdcompte
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\grand_compte $grdcompte
     */
    public function setGrdcompte(\Phareos\NomadeNetServiceBundle\Entity\grand_compte $grdcompte)
    {
        $this->grdcompte = $grdcompte;
    }

    /**
     * Get grdcompte
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\grand_compte 
     */
    public function getGrdcompte()
    {
        return $this->grdcompte;
    }

    /**
     * Add prodmagstock
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\stockmag $prodmagstock
     */
    public function addstockmag(\Phareos\NomadeNetServiceBundle\Entity\stockmag $prodmagstock)
    {
        $this->prodmagstock[] = $prodmagstock;
    }

    /**
     * Get prodmagstock
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getProdmagstock()
    {
        return $this->prodmagstock;
    }

    /**
     * Add prodmaudtech
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\audtech $prodmaudtech
     */
    public function addaudtech(\Phareos\NomadeNetServiceBundle\Entity\audtech $prodmaudtech)
    {
        $this->prodmaudtech[] = $prodmaudtech;
    }

    /**
     * Get prodmaudtech
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getProdmaudtech()
    {
        return $this->prodmaudtech;
    }

    /**
     * Set lieux
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\lieux $lieux
     */
    public function setLieux(\Phareos\NomadeNetServiceBundle\Entity\lieux $lieux)
    {
        $this->lieux = $lieux;
    }

    /**
     * Get lieux
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\lieux 
     */
    public function getLieux()
    {
        return $this->lieux;
    }

    /**
     * Set prod
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\prod $prod
     */
    public function setProd(\Phareos\NomadeNetServiceBundle\Entity\prod $prod)
    {
        $this->prod = $prod;
    }

    /**
     * Get prod
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\prod 
     */
    public function getProd()
    {
        return $this->prod;
    }
}