<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\NomadeNetServiceBundle\Entity\frequence
 *
 * @ORM\Table(name="nom_frequence")
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\frequenceRepository")
 */
class frequence
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\OneToMany(targetEntity="tachesprog", mappedBy="frequence", cascade={"remove", "persist"})
	 */
	protected $freqtachprog;
	
	/**
	 * @ORM\OneToMany(targetEntity="tachesprogmag", mappedBy="frequence", cascade={"remove", "persist"})
	 */
	protected $freqtachprogmag;
	

    /**
     * @var string $nom
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;
	
	/**
     * @var integer $grdcompte_id
     *
     * @ORM\Column(name="grdcompte_id", type="integer")
     */
    private $grdcompte_id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set grdcompte_id
     *
     * @param integer $grdcompteId
     */
    public function setGrdcompteId($grdcompteId)
    {
        $this->grdcompte_id = $grdcompteId;
    }

    /**
     * Get grdcompte_id
     *
     * @return integer 
     */
    public function getGrdcompteId()
    {
        return $this->grdcompte_id;
    }
    public function __construct()
    {
        $this->freqtachprog = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add freqtachprog
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\tachesprog $freqtachprog
     */
    public function addtachesprog(\Phareos\NomadeNetServiceBundle\Entity\tachesprog $freqtachprog)
    {
        $this->freqtachprog[] = $freqtachprog;
    }

    /**
     * Get freqtachprog
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getFreqtachprog()
    {
        return $this->freqtachprog;
    }

    /**
     * Add freqtachprogmag
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\tachesprogmag $freqtachprogmag
     */
    public function addtachesprogmag(\Phareos\NomadeNetServiceBundle\Entity\tachesprogmag $freqtachprogmag)
    {
        $this->freqtachprogmag[] = $freqtachprogmag;
    }

    /**
     * Get freqtachprogmag
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getFreqtachprogmag()
    {
        return $this->freqtachprogmag;
    }
}