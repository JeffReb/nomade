<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\NomadeNetServiceBundle\Entity\tachesprog
 *
 * @ORM\Table(name="nom_tachesprog")
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\tachesprogRepository")
 */
class tachesprog
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\ManyToOne(targetEntity="frequence", inversedBy="freqtachprog", cascade={"remove"})
	 * @ORM\JoinColumn(name="frequence_id", referencedColumnName="id")
	 */
	protected $frequence;
	
	/**
	 * @ORM\ManyToOne(targetEntity="fonction", inversedBy="fonctachprog", cascade={"remove"})
	 * @ORM\JoinColumn(name="fonction_id", referencedColumnName="id")
	 */
	protected $fonction;
	
	
	/**
	 * @ORM\OneToMany(targetEntity="pocontrqte", mappedBy="tachesprog", cascade={"remove", "persist"})
	 */
	protected $pocontrqtetachesprog;

    /**
     * @var string $tache
     *
     * @ORM\Column(name="tache", type="string", length=255, nullable=true)
     */
    private $tache;

    /**
     * @var time $duree
     *
     * @ORM\Column(name="duree", type="time", nullable=true)
     */
    private $duree;

	
	/**
     * @var string $nomgc
     *
     * @ORM\Column(name="nomgc", type="string", length=255, nullable=true)
     */
    private $nomgc;
	
	/**
     * @var boolean $archive
     *
     * @ORM\Column(name="archive", type="boolean", nullable=true)
     */
    private $archive;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set tache
     *
     * @param string $tache
     */
    public function setTache($tache)
    {
        $this->tache = $tache;
    }

    /**
     * Get tache
     *
     * @return string 
     */
    public function getTache()
    {
        return $this->tache;
    }

    /**
     * Set duree
     *
     * @param time $duree
     */
    public function setDuree($duree)
    {
        $this->duree = $duree;
    }

    /**
     * Get duree
     *
     * @return time 
     */
    public function getDuree()
    {
        return $this->duree;
    }

	
	/**
     * Set nomgc
     *
     * @param string $nomgc
     */
    public function setNomgc($nomgc)
    {
        $this->nomgc = $nomgc;
    }

    /**
     * Get nomgc
     *
     * @return string 
     */
    public function getNomgc()
    {
        return $this->nomgc;
    }
	
	/**
     * Set archive
     *
     * @param boolean $archive
     */
    public function setArchive($archive)
    {
        $this->archive = $archive;
    }

    /**
     * Get archive
     *
     * @return boolean 
     */
    public function getArchive()
    {
        return $this->archive;
    }
	
    public function __construct()
    {
        $this->pocontrqtetachesprog = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add pocontrqtetachesprog
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\pocontrqte $pocontrqtetachesprog
     */
    public function addpocontrqte(\Phareos\NomadeNetServiceBundle\Entity\pocontrqte $pocontrqtetachesprog)
    {
        $this->pocontrqtetachesprog[] = $pocontrqtetachesprog;
    }

    /**
     * Get pocontrqtetachesprog
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPocontrqtetachesprog()
    {
        return $this->pocontrqtetachesprog;
    }

    /**
     * Set frequence
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\frequence $frequence
     */
    public function setFrequence(\Phareos\NomadeNetServiceBundle\Entity\frequence $frequence)
    {
        $this->frequence = $frequence;
    }

    /**
     * Get frequence
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\frequence 
     */
    public function getFrequence()
    {
        return $this->frequence;
    }

    /**
     * Set fonction
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\fonction $fonction
     */
    public function setFonction(\Phareos\NomadeNetServiceBundle\Entity\fonction $fonction)
    {
        $this->fonction = $fonction;
    }

    /**
     * Get fonction
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\fonction 
     */
    public function getFonction()
    {
        return $this->fonction;
    }
}