<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Phareos\NomadeNetServiceBundle\Entity\formation
 *
 * @ORM\Table(name="nom_formation")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\formationRepository")
 */
class formation
{
    /**
	* @ORM\ManyToOne(targetEntity="lieux", inversedBy="formationlieux", cascade={"remove"})
	* @ORM\JoinColumn(name="lieux_id", referencedColumnName="id")
	*/
	protected $lieux;
	
	/**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $site
     *
     * @ORM\Column(name="site", type="string", length=255)
     */
    private $site;

    /**
     * @var date $dateform
     *
     * @ORM\Column(name="dateform", type="date")
     */
    private $dateform;

    /**
     * @var string $adresse1
     *
     * @ORM\Column(name="adresse1", type="string", length=255, nullable=true)
     */
    private $adresse1;

    /**
     * @var string $adresse2
     *
     * @ORM\Column(name="adresse2", type="string", length=255, nullable=true)
     */
    private $adresse2;

    /**
     * @var string $cp
     *
     * @ORM\Column(name="cp", type="string", length=255, nullable=true)
     */
    private $cp;

    /**
     * @var string $ville
     *
     * @ORM\Column(name="ville", type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @var string $nomform
     *
     * @ORM\Column(name="nomform", type="string", length=255, nullable=true)
     */
    private $nomform;

    /**
     * @var string $prenomform
     *
     * @ORM\Column(name="prenomform", type="string", length=255, nullable=true)
     */
    private $prenomform;

    /**
     * @var text $detailform
     *
     * @ORM\Column(name="detailform", type="text", nullable=true)
     */
    private $detailform;

    /**
     * @var string $typeform
     *
     * @ORM\Column(name="typeform", type="string", length=255, nullable=true)
     */
    private $typeform;

    /**
     * @var text $observ
     *
     * @ORM\Column(name="observ", type="text", nullable=true)
     */
    private $observ;

    /**
     * @var string $recommand
     *
     * @ORM\Column(name="recommand", type="string", length=255, nullable=true)
     */
    private $recommand;

    /**
     * @var integer $critere
     *
     * @ORM\Column(name="critere", type="integer", nullable=true)
     */
    private $critere;

    /**
     * @var string $nomagent
     *
     * @ORM\Column(name="nomagent", type="string", length=255, nullable=true)
     */
    private $nomagent;

    /**
     * @var string $prenomagent
     *
     * @ORM\Column(name="prenomagent", type="string", length=255, nullable=true)
     */
    private $prenomagent;

    /**
     * @var string $adr1agent
     *
     * @ORM\Column(name="adr1agent", type="string", length=255, nullable=true)
     */
    private $adr1agent;

    /**
     * @var string $adr2agent
     *
     * @ORM\Column(name="adr2agent", type="string", length=255, nullable=true)
     */
    private $adr2agent;

    /**
     * @var string $cpagent
     *
     * @ORM\Column(name="cpagent", type="string", length=255, nullable=true)
     */
    private $cpagent;

    /**
     * @var string $villeagent
     *
     * @ORM\Column(name="villeagent", type="string", length=255, nullable=true)
     */
    private $villeagent;

    /**
     * @var string $nation
     *
     * @ORM\Column(name="nation", type="string", length=255, nullable=true)
     */
    private $nation;

    /**
     * @var integer $CEE
     *
     * @ORM\Column(name="CEE", type="integer", nullable=true)
     */
    private $CEE;

    /**
     * @var string $numci
     *
     * @ORM\Column(name="numci", type="string", length=255, nullable=true)
     */
    private $numci;

    /**
     * @var string $pref
     *
     * @ORM\Column(name="pref", type="string", length=255, nullable=true)
     */
    private $pref;

    /**
     * @var string $numtsej
     *
     * @ORM\Column(name="numtsej", type="string", length=255, nullable=true)
     */
    private $numtsej;

    /**
     * @var date $datesej
     *
     * @ORM\Column(name="datesej", type="date", nullable=true)
     */
    private $datesej;

    /**
     * @var boolean $autotravsal
     *
     * @ORM\Column(name="autotravsal", type="boolean", nullable=true)
     */
    private $autotravsal;

    /**
     * @var string $pref2
     *
     * @ORM\Column(name="pref2", type="string", length=255, nullable=true)
     */
    private $pref2;
	

    /**
     * @var integer $notepres
     *
     * @ORM\Column(name="notepres", type="integer", nullable=true)
     */
    private $notepres;

    /**
     * @var integer $noteeloc
     *
     * @ORM\Column(name="noteeloc", type="integer", nullable=true)
     */
    private $noteeloc;

    /**
     * @var integer $notedynam
     *
     * @ORM\Column(name="notedynam", type="integer", nullable=true)
     */
    private $notedynam;

    /**
     * @var integer $noteanaly
     *
     * @ORM\Column(name="noteanaly", type="integer", nullable=true)
     */
    private $noteanaly;

    /**
     * @var integer $noteorga
     *
     * @ORM\Column(name="noteorga", type="integer", nullable=true)
     */
    private $noteorga;

    /**
     * @var integer $noteexp
     *
     * @ORM\Column(name="noteexp", type="integer", nullable=true)
     */
    private $noteexp;

    /**
     * @var string $fileimg1
     * @Assert\File( maxSize = "4M", mimeTypesMessage = "SVP Uploader un fichier img")
     * @ORM\Column(name="fileimg1", type="string", length=255, nullable=true)
     */
    private $fileimg1;
	
	/**
     * @var string $fileimg2
     * @Assert\File( maxSize = "4M", mimeTypesMessage = "SVP Uploader un fichier img")
     * @ORM\Column(name="fileimg2", type="string", length=255, nullable=true)
     */
    private $fileimg2;
	
	/**
     * @var string $fileimg3
     * @Assert\File( maxSize = "4M", mimeTypesMessage = "SVP Uploader un fichier img")
     * @ORM\Column(name="fileimg3", type="string", length=255, nullable=true)
     */
    private $fileimg3;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set site
     *
     * @param string $site
     */
    public function setSite($site)
    {
        $this->site = $site;
    }

    /**
     * Get site
     *
     * @return string 
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set dateform
     *
     * @param date $dateform
     */
    public function setDateform($dateform)
    {
        $this->dateform = $dateform;
    }

    /**
     * Get dateform
     *
     * @return date 
     */
    public function getDateform()
    {
        return $this->dateform;
    }

    /**
     * Set adresse1
     *
     * @param string $adresse1
     */
    public function setAdresse1($adresse1)
    {
        $this->adresse1 = $adresse1;
    }

    /**
     * Get adresse1
     *
     * @return string 
     */
    public function getAdresse1()
    {
        return $this->adresse1;
    }

    /**
     * Set adresse2
     *
     * @param string $adresse2
     */
    public function setAdresse2($adresse2)
    {
        $this->adresse2 = $adresse2;
    }

    /**
     * Get adresse2
     *
     * @return string 
     */
    public function getAdresse2()
    {
        return $this->adresse2;
    }

    /**
     * Set cp
     *
     * @param string $cp
     */
    public function setCp($cp)
    {
        $this->cp = $cp;
    }

    /**
     * Get cp
     *
     * @return string 
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set ville
     *
     * @param string $ville
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set nomform
     *
     * @param string $nomform
     */
    public function setNomform($nomform)
    {
        $this->nomform = $nomform;
    }

    /**
     * Get nomform
     *
     * @return string 
     */
    public function getNomform()
    {
        return $this->nomform;
    }

    /**
     * Set prenomform
     *
     * @param string $prenomform
     */
    public function setPrenomform($prenomform)
    {
        $this->prenomform = $prenomform;
    }

    /**
     * Get prenomform
     *
     * @return string 
     */
    public function getPrenomform()
    {
        return $this->prenomform;
    }

    /**
     * Set detailform
     *
     * @param text $detailform
     */
    public function setDetailform($detailform)
    {
        $this->detailform = $detailform;
    }

    /**
     * Get detailform
     *
     * @return text 
     */
    public function getDetailform()
    {
        return $this->detailform;
    }

    /**
     * Set typeform
     *
     * @param string $typeform
     */
    public function setTypeform($typeform)
    {
        $this->typeform = $typeform;
    }

    /**
     * Get typeform
     *
     * @return string 
     */
    public function getTypeform()
    {
        return $this->typeform;
    }

    /**
     * Set observ
     *
     * @param text $observ
     */
    public function setObserv($observ)
    {
        $this->observ = $observ;
    }

    /**
     * Get observ
     *
     * @return text 
     */
    public function getObserv()
    {
        return $this->observ;
    }

    /**
     * Set recommand
     *
     * @param string $recommand
     */
    public function setRecommand($recommand)
    {
        $this->recommand = $recommand;
    }

    /**
     * Get recommand
     *
     * @return string 
     */
    public function getRecommand()
    {
        return $this->recommand;
    }

    /**
     * Set critere
     *
     * @param integer $critere
     */
    public function setCritere($critere)
    {
        $this->critere = $critere;
    }

    /**
     * Get critere
     *
     * @return integer 
     */
    public function getCritere()
    {
        return $this->critere;
    }

    /**
     * Set nomagent
     *
     * @param string $nomagent
     */
    public function setNomagent($nomagent)
    {
        $this->nomagent = $nomagent;
    }

    /**
     * Get nomagent
     *
     * @return string 
     */
    public function getNomagent()
    {
        return $this->nomagent;
    }

    /**
     * Set prenomagent
     *
     * @param string $prenomagent
     */
    public function setPrenomagent($prenomagent)
    {
        $this->prenomagent = $prenomagent;
    }

    /**
     * Get prenomagent
     *
     * @return string 
     */
    public function getPrenomagent()
    {
        return $this->prenomagent;
    }

    /**
     * Set adr1agent
     *
     * @param string $adr1agent
     */
    public function setAdr1agent($adr1agent)
    {
        $this->adr1agent = $adr1agent;
    }

    /**
     * Get adr1agent
     *
     * @return string 
     */
    public function getAdr1agent()
    {
        return $this->adr1agent;
    }

    /**
     * Set adr2agent
     *
     * @param string $adr2agent
     */
    public function setAdr2agent($adr2agent)
    {
        $this->adr2agent = $adr2agent;
    }

    /**
     * Get adr2agent
     *
     * @return string 
     */
    public function getAdr2agent()
    {
        return $this->adr2agent;
    }

    /**
     * Set cpagent
     *
     * @param string $cpagent
     */
    public function setCpagent($cpagent)
    {
        $this->cpagent = $cpagent;
    }

    /**
     * Get cpagent
     *
     * @return string 
     */
    public function getCpagent()
    {
        return $this->cpagent;
    }

    /**
     * Set villeagent
     *
     * @param string $villeagent
     */
    public function setVilleagent($villeagent)
    {
        $this->villeagent = $villeagent;
    }

    /**
     * Get villeagent
     *
     * @return string 
     */
    public function getVilleagent()
    {
        return $this->villeagent;
    }

    /**
     * Set nation
     *
     * @param string $nation
     */
    public function setNation($nation)
    {
        $this->nation = $nation;
    }

    /**
     * Get nation
     *
     * @return string 
     */
    public function getNation()
    {
        return $this->nation;
    }

    /**
     * Set CEE
     *
     * @param integer $cEE
     */
    public function setCEE($cEE)
    {
        $this->CEE = $cEE;
    }

    /**
     * Get CEE
     *
     * @return integer 
     */
    public function getCEE()
    {
        return $this->CEE;
    }

    /**
     * Set numci
     *
     * @param string $numci
     */
    public function setNumci($numci)
    {
        $this->numci = $numci;
    }

    /**
     * Get numci
     *
     * @return string 
     */
    public function getNumci()
    {
        return $this->numci;
    }

    /**
     * Set pref
     *
     * @param string $pref
     */
    public function setPref($pref)
    {
        $this->pref = $pref;
    }

    /**
     * Get pref
     *
     * @return string 
     */
    public function getPref()
    {
        return $this->pref;
    }

    /**
     * Set numtsej
     *
     * @param string $numtsej
     */
    public function setNumtsej($numtsej)
    {
        $this->numtsej = $numtsej;
    }

    /**
     * Get numtsej
     *
     * @return string 
     */
    public function getNumtsej()
    {
        return $this->numtsej;
    }

    /**
     * Set datesej
     *
     * @param date $datesej
     */
    public function setDatesej($datesej)
    {
        $this->datesej = $datesej;
    }

    /**
     * Get datesej
     *
     * @return date 
     */
    public function getDatesej()
    {
        return $this->datesej;
    }

    /**
     * Set autotravsal
     *
     * @param boolean $autotravsal
     */
    public function setAutotravsal($autotravsal)
    {
        $this->autotravsal = $autotravsal;
    }

    /**
     * Get autotravsal
     *
     * @return boolean 
     */
    public function getAutotravsal()
    {
        return $this->autotravsal;
    }

    /**
     * Set pref2
     *
     * @param string $pref2
     */
    public function setPref2($pref2)
    {
        $this->pref2 = $pref2;
    }

    /**
     * Get pref2
     *
     * @return string 
     */
    public function getPref2()
    {
        return $this->pref2;
    }
	

    /**
     * Set notepres
     *
     * @param integer $notepres
     */
    public function setNotepres($notepres)
    {
        $this->notepres = $notepres;
    }

    /**
     * Get notepres
     *
     * @return integer 
     */
    public function getNotepres()
    {
        return $this->notepres;
    }

    /**
     * Set noteeloc
     *
     * @param integer $noteeloc
     */
    public function setNoteeloc($noteeloc)
    {
        $this->noteeloc = $noteeloc;
    }

    /**
     * Get noteeloc
     *
     * @return integer 
     */
    public function getNoteeloc()
    {
        return $this->noteeloc;
    }

    /**
     * Set notedynam
     *
     * @param integer $notedynam
     */
    public function setNotedynam($notedynam)
    {
        $this->notedynam = $notedynam;
    }

    /**
     * Get notedynam
     *
     * @return integer 
     */
    public function getNotedynam()
    {
        return $this->notedynam;
    }

    /**
     * Set noteanaly
     *
     * @param integer $noteanaly
     */
    public function setNoteanaly($noteanaly)
    {
        $this->noteanaly = $noteanaly;
    }

    /**
     * Get noteanaly
     *
     * @return integer 
     */
    public function getNoteanaly()
    {
        return $this->noteanaly;
    }

    /**
     * Set noteorga
     *
     * @param integer $noteorga
     */
    public function setNoteorga($noteorga)
    {
        $this->noteorga = $noteorga;
    }

    /**
     * Get noteorga
     *
     * @return integer 
     */
    public function getNoteorga()
    {
        return $this->noteorga;
    }

    /**
     * Set noteexp
     *
     * @param integer $noteexp
     */
    public function setNoteexp($noteexp)
    {
        $this->noteexp = $noteexp;
    }

    /**
     * Get noteexp
     *
     * @return integer 
     */
    public function getNoteexp()
    {
        return $this->noteexp;
    }

    /**
     * Set fileimg1
     *
     * @param string $fileimg1
     */
    public function setFileimg1($fileimg1)
    {
        $this->fileimg1 = $fileimg1;
    }

    /**
     * Get fileimg1
     *
     * @return string 
     */
    public function getFileimg1()
    {
        return $this->fileimg1;
    }
	
	/**
     * Set fileimg2
     *
     * @param string $fileimg2
     */
    public function setFileimg2($fileimg2)
    {
        $this->fileimg2 = $fileimg2;
    }

    /**
     * Get fileimg2
     *
     * @return string 
     */
    public function getFileimg2()
    {
        return $this->fileimg2;
    }
	
	/**
     * Set fileimg3
     *
     * @param string $fileimg3
     */
    public function setFileimg3($fileimg3)
    {
        $this->fileimg3 = $fileimg3;
    }

    /**
     * Get fileimg3
     *
     * @return string 
     */
    public function getFileimg3()
    {
        return $this->fileimg3;
    }
	
	/**
     * Set lieux
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\lieux $lieux
     */
    public function setLieux(\Phareos\NomadeNetServiceBundle\Entity\lieux $lieux)
    {
        $this->lieux = $lieux;
    }
	
    /**
     * Get lieux
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\lieux 
     */
    public function getLieux()
    {
        return $this->lieux;
    }
	
	public function __construct()
	{
		$this->dateform = new \DateTime('now');
	}
	
	
	public function getFullPdfPath() {
        return null === $this->fileimg1 ? null : $this->getUploadRootDir(). $this->fileimg1;
    }
	
	public function getFullPdfPath2() {
        return null === $this->fileimg2 ? null : $this->getUploadRootDir2(). $this->fileimg2;
    }
	
	public function getFullPdfPath3() {
        return null === $this->fileimg3 ? null : $this->getUploadRootDir3(). $this->fileimg3;
    }
 
    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return $this->getTmpUploadRootDir().$this->getId()."/";
    }
	
	protected function getUploadRootDir2() {
        // the absolute directory path where uploaded documents should be saved
        return $this->getTmpUploadRootDir2().$this->getId()."/";
    }
	
	protected function getUploadRootDir3() {
        // the absolute directory path where uploaded documents should be saved
        return $this->getTmpUploadRootDir3().$this->getId()."/";
    }
 
    protected function getTmpUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/upload/nomade/formation/stagiaire1/';
    }
	
	protected function getTmpUploadRootDir2() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/upload/nomade/formation/stagiaire2/';
    }
	
	protected function getTmpUploadRootDir3() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/upload/nomade/formation/stagiaire3/';
    }
 
    /**
     * @ORM\PrePersist()
     * 
     */
    public function uploadPdf() {
        // the file property can be empty if the field is not required
        if (null === $this->fileimg1) {
            return;
        }
        if(!$this->id){
            $this->fileimg1->move($this->getTmpUploadRootDir(), $this->fileimg1->getClientOriginalName());
        }else{
            $this->fileimg1->move($this->getUploadRootDir(), $this->fileimg1->getClientOriginalName());
        }
        $this->setFileimg1($this->fileimg1->getClientOriginalName());
    }
 
    /**
     * @ORM\PostPersist()
     */
    public function movePdf()
    {
        if (null === $this->fileimg1) {
            return;
        }
        if(!is_dir($this->getUploadRootDir())){
            mkdir($this->getUploadRootDir());
        }
        copy($this->getTmpUploadRootDir().$this->fileimg1, $this->getFullPdfPath());
        unlink($this->getTmpUploadRootDir().$this->fileimg1);
    }
	
	/**
     * @ORM\PrePersist()
     * 
     */
    public function uploadPdf2() {
        // the file property can be empty if the field is not required
        if (null === $this->fileimg2) {
            return;
        }
        if(!$this->id){
            $this->fileimg2->move($this->getTmpUploadRootDir2(), $this->fileimg2->getClientOriginalName());
        }else{
            $this->fileimg2->move($this->getUploadRootDir2(), $this->fileimg2->getClientOriginalName());
        }
        $this->setFileimg2($this->fileimg2->getClientOriginalName());
    }
 
    /**
     * @ORM\PostPersist()
     */
    public function movePdf2()
    {
        if (null === $this->fileimg2) {
            return;
        }
        if(!is_dir($this->getUploadRootDir2())){
            mkdir($this->getUploadRootDir2());
        }
        copy($this->getTmpUploadRootDir2().$this->fileimg2, $this->getFullPdfPath2());
        unlink($this->getTmpUploadRootDir2().$this->fileimg2);
    }
	
	/**
     * @ORM\PrePersist()
     * 
     */
    public function uploadPdf3() {
        // the file property can be empty if the field is not required
        if (null === $this->fileimg3) {
            return;
        }
        if(!$this->id){
            $this->fileimg3->move($this->getTmpUploadRootDir3(), $this->fileimg3->getClientOriginalName());
        }else{
            $this->fileimg3->move($this->getUploadRootDir3(), $this->fileimg3->getClientOriginalName());
        }
        $this->setFileimg3($this->fileimg3->getClientOriginalName());
    }
 
    /**
     * @ORM\PostPersist()
     */
    public function movePdf3()
    {
        if (null === $this->fileimg3) {
            return;
        }
        if(!is_dir($this->getUploadRootDir3())){
            mkdir($this->getUploadRootDir3());
        }
        copy($this->getTmpUploadRootDir3().$this->fileimg3, $this->getFullPdfPath3());
        unlink($this->getTmpUploadRootDir3().$this->fileimg3);
    }
}