<?php

namespace Phareos\NomadeNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\NomadeNetServiceBundle\Entity\planjs
 *
 * @ORM\Table(name="nom_planjs")
 * @ORM\Entity(repositoryClass="Phareos\NomadeNetServiceBundle\Entity\planjsRepository")
 */
class planjs
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
	 * @ORM\ManyToOne(targetEntity="tachesprogmag", inversedBy="tachesproplanjs", cascade={"remove"})
	 * @ORM\JoinColumn(name="tachesprogmag_id", referencedColumnName="id")
	 */
	protected $tachesprogmag;
	
	/**
	 * @ORM\ManyToOne(targetEntity="planingdate", inversedBy="plandplanjs", cascade={"remove"})
	 * @ORM\JoinColumn(name="planingdate_id", referencedColumnName="id")
	 */
	protected $planingdate;
	
	/**
     * @var string $tache
     *
     * @ORM\Column(name="tache", type="string", length=255, nullable=true)
     */
    private $tache;
	
	/**
     * @var string $tljs
     *
     * @ORM\Column(name="tljs", type="string", length=10, nullable=true)
     */
    private $tljs;

    /**
     * @var string $lundi
     *
     * @ORM\Column(name="lundi", type="string", length=10, nullable=true)
     */
    private $lundi;

    /**
     * @var string $mardi
     *
     * @ORM\Column(name="mardi", type="string", length=10, nullable=true)
     */
    private $mardi;

    /**
     * @var string $mercredi
     *
     * @ORM\Column(name="mercredi", type="string", length=10, nullable=true)
     */
    private $mercredi;

    /**
     * @var string $jeudi
     *
     * @ORM\Column(name="jeudi", type="string", length=10, nullable=true)
     */
    private $jeudi;

    /**
     * @var string $vendredi
     *
     * @ORM\Column(name="vendredi", type="string", length=10, nullable=true)
     */
    private $vendredi;

    /**
     * @var string $samedi
     *
     * @ORM\Column(name="samedi", type="string", length=10, nullable=true)
     */
    private $samedi;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tljs
     *
     * @param string $tljs
     */
    public function setTljs($tljs)
    {
        $this->tljs = $tljs;
    }

    /**
     * Get tljs
     *
     * @return string 
     */
    public function getTljs()
    {
        return $this->tljs;
    }
	
	/**
     * Set tache
     *
     * @param string $tache
     */
    public function setTache($tache)
    {
        $this->tache = $tache;
    }

    /**
     * Get tache
     *
     * @return string 
     */
    public function getTache()
    {
        return $this->tache;
    }

    /**
     * Set lundi
     *
     * @param string $lundi
     */
    public function setLundi($lundi)
    {
        $this->lundi = $lundi;
    }

    /**
     * Get lundi
     *
     * @return string 
     */
    public function getLundi()
    {
        return $this->lundi;
    }

    /**
     * Set mardi
     *
     * @param string $mardi
     */
    public function setMardi($mardi)
    {
        $this->mardi = $mardi;
    }

    /**
     * Get mardi
     *
     * @return string 
     */
    public function getMardi()
    {
        return $this->mardi;
    }

    /**
     * Set mercredi
     *
     * @param string $mercredi
     */
    public function setMercredi($mercredi)
    {
        $this->mercredi = $mercredi;
    }

    /**
     * Get mercredi
     *
     * @return string 
     */
    public function getMercredi()
    {
        return $this->mercredi;
    }

    /**
     * Set jeudi
     *
     * @param string $jeudi
     */
    public function setJeudi($jeudi)
    {
        $this->jeudi = $jeudi;
    }

    /**
     * Get jeudi
     *
     * @return string 
     */
    public function getJeudi()
    {
        return $this->jeudi;
    }

    /**
     * Set vendredi
     *
     * @param string $vendredi
     */
    public function setVendredi($vendredi)
    {
        $this->vendredi = $vendredi;
    }

    /**
     * Get vendredi
     *
     * @return string 
     */
    public function getVendredi()
    {
        return $this->vendredi;
    }

    /**
     * Set samedi
     *
     * @param string $samedi
     */
    public function setSamedi($samedi)
    {
        $this->samedi = $samedi;
    }

    /**
     * Get samedi
     *
     * @return string 
     */
    public function getSamedi()
    {
        return $this->samedi;
    }

    /**
     * Set tachesprogmag
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\tachesprogmag $tachesprogmag
     */
    public function setTachesprogmag(\Phareos\NomadeNetServiceBundle\Entity\tachesprogmag $tachesprogmag)
    {
        $this->tachesprogmag = $tachesprogmag;
    }

    /**
     * Get tachesprogmag
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\tachesprogmag 
     */
    public function getTachesprogmag()
    {
        return $this->tachesprogmag;
    }

    /**
     * Set planingdate
     *
     * @param Phareos\NomadeNetServiceBundle\Entity\planingdate $planingdate
     */
    public function setPlaningdate(\Phareos\NomadeNetServiceBundle\Entity\planingdate $planingdate)
    {
        $this->planingdate = $planingdate;
    }

    /**
     * Get planingdate
     *
     * @return Phareos\NomadeNetServiceBundle\Entity\planingdate 
     */
    public function getPlaningdate()
    {
        return $this->planingdate;
    }
}