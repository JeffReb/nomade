<?php

namespace Phareos\NomadeNetServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Phareos\NomadeNetServiceBundle\Entity\audtech;
use Phareos\NomadeNetServiceBundle\Form\audtechType;
use Phareos\NomadeNetServiceBundle\Form\audtecheditType;

use Phareos\NomadeNetServiceBundle\Entity\audtprog;
use Phareos\NomadeNetServiceBundle\Form\audtprogType;

use Phareos\NomadeNetServiceBundle\Entity\stockmag;
use Phareos\NomadeNetServiceBundle\Form\stockmagType;

use Phareos\NomadeNetServiceBundle\Entity\prodmag;
use Phareos\NomadeNetServiceBundle\Form\prodmagType;

use Phareos\NomadeNetServiceBundle\Entity\recadrage;
use Phareos\NomadeNetServiceBundle\Form\recadrageType;

use Phareos\NomadeNetServiceBundle\Form\recadrageeditType;

use Phareos\NomadeNetServiceBundle\Entity\formation;
use Phareos\NomadeNetServiceBundle\Form\formationType;

require_once __DIR__ . '/../Resources/public/signaturepad/signature-to-image.php';

/**
 * audtech controller.
 *
 */
class audtechController extends Controller
{
    /**
     * Lists all audtech entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
		
		$request = $this->get('request');
		$idMag = $request->query->get('idMag');
		$session = $this->get('session');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		

		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		

        $entities = $em->getRepository('PhareosNomadeNetServiceBundle:audtech')->findBy(array('lieux' => $idMag),
                                      array('id' => 'desc'));

        return $this->render('PhareosNomadeNetServiceBundle:audtech:index.html.twig', array(
            'idMag' => $idMag,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'lieux'=> $lieux,
			'site'=>$site,
			'entities' => $entities
        ));
    }
	
	public function indexadminAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

		
		
        
		
		$request = $this->get('request');
		$idMag = $request->query->get('idMag');
		$session = $this->get('session');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		

		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		
		
		// ancien entité tous
		//$entities = $em->getRepository('PhareosNomadeNetServiceBundle:audit')->findBy(array('caisses' => 'Propre'));
		//remplacer par nouvelle pour le filtrage par apartenance mag:
		$entities = $em->getRepository('PhareosNomadeNetServiceBundle:audtech')->findAll();
		
		
		
		
        return $this->render('PhareosNomadeNetServiceBundle:audtech:indexadmin.html.twig', array(
            'idMag' => $idMag,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'lieux'=> $lieux,
			'site'=>$site,
			'entities' => $entities
        ));
    }

    /**
     * Finds and displays a audtech entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:audtech')->find($id);
		
		$request = $this->get('request');
		$session = $this->get('session');
		$idMag = $session->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		

		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find audtech entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
		
		$taches = $em->getRepository('PhareosNomadeNetServiceBundle:tachesprogmag')->findBy(array('lieux' => $idMag, 'archive' => 0));
		
		$audtprogs = $em->getRepository('PhareosNomadeNetServiceBundle:audtprog')->findBy(array('audtech' => $id));
		
		$entitiesrecadrage = $em->getRepository('PhareosNomadeNetServiceBundle:recadrage')->findBy(array('lieux' => $idMag),
                                      array('id' => 'desc'));
									  
		$entitiesstockmag = $em->getRepository('PhareosNomadeNetServiceBundle:stockmag')->findBy(array('lieux' => $idMag),
                                      array('id' => 'desc'));
									 
		$entitiesformation = $em->getRepository('PhareosNomadeNetServiceBundle:formation')->findBy(array('lieux' => $idMag),
                                      array('id' => 'desc'));

        return $this->render('PhareosNomadeNetServiceBundle:audtech:show.html.twig', array(
            'idMag' => $idMag,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'lieux'=> $lieux,
			'site'=>$site,
			'taches' =>$taches,
			'audtprogs' =>$audtprogs,
			'entity'      => $entity,
			'entitiesrecadrage' => $entitiesrecadrage,
			'entitiesstockmag' => $entitiesstockmag,
			'entitiesformation' => $entitiesformation,
            'delete_form' => $deleteForm->createView(),
			

        ));
    }
	
	public function showsingleAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:audtech')->find($id);
		
		$request = $this->get('request');
		$session = $this->get('session');
		$idMag = $session->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		

		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find audtech entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
		
		$taches = $em->getRepository('PhareosNomadeNetServiceBundle:tachesprogmag')->findBy(array('lieux' => $idMag, 'archive' => 0));
		
		$audtprogs = $em->getRepository('PhareosNomadeNetServiceBundle:audtprog')->findBy(array('audtech' => $id));
		
		$entitiesrecadrage = $em->getRepository('PhareosNomadeNetServiceBundle:recadrage')->findBy(array('lieux' => $idMag),
                                      array('id' => 'desc'));
									  
		$entitiesstockmag = $em->getRepository('PhareosNomadeNetServiceBundle:stockmag')->findBy(array('lieux' => $idMag));

        return $this->render('PhareosNomadeNetServiceBundle:audtech:showsingle.html.twig', array(
            'idMag' => $idMag,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'lieux'=> $lieux,
			'site'=>$site,
			'taches' =>$taches,
			'audtprogs' =>$audtprogs,
			'entity'      => $entity,
			'entitiesrecadrage' => $entitiesrecadrage,
			'entitiesstockmag' => $entitiesstockmag,
            'delete_form' => $deleteForm->createView(),
			

        ));
    }
	
	public function showsinglemassAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:audtech')->find($id);
		
		$request = $this->get('request');
		$session = $this->get('session');
		$idMag = $request->query->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		

		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find audtech entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
		
		$taches = $em->getRepository('PhareosNomadeNetServiceBundle:tachesprogmag')->findBy(array('lieux' => $idMag));
		
		$audtprogs = $em->getRepository('PhareosNomadeNetServiceBundle:audtprog')->findBy(array('audtech' => $id));
		
		$entitiesrecadrage = $em->getRepository('PhareosNomadeNetServiceBundle:recadrage')->findBy(array('lieux' => $idMag),
                                      array('id' => 'desc'));
									  
		$entitiesstockmag = $em->getRepository('PhareosNomadeNetServiceBundle:stockmag')->findBy(array('lieux' => $idMag));

        return $this->render('PhareosNomadeNetServiceBundle:audtech:showsingle.html.twig', array(
            'idMag' => $idMag,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'lieux'=> $lieux,
			'site'=>$site,
			'taches' =>$taches,
			'audtprogs' =>$audtprogs,
			'entity'      => $entity,
			'entitiesrecadrage' => $entitiesrecadrage,
			'entitiesstockmag' => $entitiesstockmag,
            'delete_form' => $deleteForm->createView(),
			

        ));
    }

    /**
     * Displays a form to create a new audtech entity.
     *
     */
    public function newAction()
    {
        $session = $this->get('session');
		$request = $this->get('request');
		$idMag = $session->get('idMag');
		$idGC = $session->get('idGC');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		
		$em = $this->getDoctrine()->getEntityManager();
		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		
		$entitiesstockmag = $em->getRepository('PhareosNomadeNetServiceBundle:stockmag')->findBy(array('lieux' => $idMag),
                                      array('id' => 'desc'));
		
		$entity = new audtech();
		$entitystockmag = new stockmag();
		$_SESSION['idLIEUX'] = $idMag;
        $form   = $this->createForm(new audtechType(), $entity);
		$formstockmag   = $this->createForm(new stockmagType(), $entitystockmag);
		
		$entitiesrecadre = $em->getRepository('PhareosNomadeNetServiceBundle:recadrage')->findBy(array('lieux' => $idMag),
                                      array('id' => 'desc'));
									  
		$entityformation = new formation();
        $formformation   = $this->createForm(new formationType(), $entityformation);
		
		$entitiesprodmag = $em->getRepository('PhareosNomadeNetServiceBundle:prodmag')->findBy(array(
											'lieux' => $idMag,
											'archive' => 0));
		
		
		
		$taches = $em->getRepository('PhareosNomadeNetServiceBundle:tachesprogmag')->findBy(array('lieux' => $idMag, 'archive' => 0));

        return $this->render('PhareosNomadeNetServiceBundle:audtech:new.html.twig', array(
            'idMag' => $idMag,
			'lieux'=> $lieux,
			'site'=>$site,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'entitystockmag' => $entitystockmag,
			'entity' => $entity,
			'entitiesstockmag' => $entitiesstockmag,
			'entitiesrecadre' => $entitiesrecadre,
			'entityformation' => $entityformation,
			'taches' => $taches,
			'entitiesprodmag' => $entitiesprodmag,
			'formstockmag'   => $formstockmag->createView(),
			'formformation'   => $formformation->createView(),
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new audtech entity.
     *
     */
    public function createAction()
    {
        $entity  = new audtech();
		
		
		
        $request = $this->getRequest();
        $form    = $this->createForm(new audtechType(), $entity);
        $form->bindRequest($request);
		
		$session = $this->get('session');
		$idGC = $session->get('idGC');
		$idMag = $session->get('idMag');
		$nomGC = $session->get('nomGC');
		$responsecteur = $session->get('nomUSER');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		//$datekeep = $request->request->get('datepicker2');
		//$datekeep2 = new \DateTime($datekeep);
		//$session->set('datekeep', $datekeep2);
		//$session->set('date', $entity->getDateMaj());
		$date_maj = $request->request->get('date_maj');
		$date_maj2 = new \DateTime($date_maj);
		//new \DateTime($date_maj);
		
		$prenomUSER = $session->get('prenomUSER');
		$nomUSER = $session->get('nomUSER');
		
		$respSecteur = $nomUSER . ' - ' . $prenomUSER;
		
		
		$em = $this->getDoctrine()->getEntityManager();
			$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
			$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
			$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
			$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
			$repository_mail = $em->getRepository('PhareosNomadeNetServiceBundle:mailswift');
			
			$mail1 = $repository_mail->find(1)->getAdresse();
			$mail2 = $repository_mail->find(2)->getAdresse();
			$mail3 = $repository_mail->find(3)->getAdresse();
			$mail4 = $repository_mail->find(4)->getAdresse();
			$mail5 = $repository_mail->find(5)->getAdresse();
			
			$lieux = $repository_lieux->find($idMag);
		
			$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
			$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
			
			$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}

        //if ($form->isValid()) {
            
			$monlieux = $this->getDoctrine()->getRepository('PhareosNomadeNetServiceBundle:lieux')->find($idMag);
			$entity->setLieux($monlieux);
			$entity->setDateMaj($date_maj2);
			$entity->setResponsecteur($respSecteur);
			
			$idAudtech = $entity->getId();
			
			
			$matenpanne = $entity->getMatenpanne();
			
			
			$_SESSION['idLIEUX'] = $idMag;
			
			
			
            $em->persist($entity);
            $em->flush();
			
			$traitementprio = $entity->getTraitPrio();
			
			
			
			$taches = $em->getRepository('PhareosNomadeNetServiceBundle:tachesprogmag')->findBy(array('lieux' => $idMag));
			
			
			
			//faire une boucle avec le nombre de taches pour injection des enregistrements
			if ($taches) {
			
				foreach ($taches as $tache)
					{
						$entitycontrole = new audtprog();
				
						$com = $request->request->get('com'.$tache->getId());
						
						$etats = $request->request->get('etat'.$tache->getId());
						
						//'foo[bar]', null, true
						if ($etats){
							foreach ($etats as $etat)
								{
									if ($etat == 'propre'){
										$entitycontrole->setPropre(1);
									}
									else {
										$entitycontrole->setPropre(0);
									}
									if ($etat == 'trace'){
										$entitycontrole->setTraces(1);
									}
									else {
										$entitycontrole->setTraces(0);
									}
									if ($etat == 'poussieres'){
										$entitycontrole->setPoussiere(1);
									}
									else {
										$entitycontrole->setPoussiere(0);
									}
									if ($etat == 'taches'){
										$entitycontrole->setTache(1);
									}
									else {
										$entitycontrole->setTache(0);
									}
									if ($etat == 'use'){
										$entitycontrole->setFatuse(1);
									}
									else {
										$entitycontrole->setFatuse(0);
									}
									$entitycontrole->setCom($com);
								}
						}	
						
						
						//$entitycontrole->setCom($etats);
						$entitycontrole->setTachesprogmag($tache);
						$entitycontrole->setAudtech($entity);
				
						$em->persist($entitycontrole);
						$em->flush();
					
					}
				
				
			//fin de boucle
			}
			
			$monlieux = $this->getDoctrine()->getRepository('PhareosNomadeNetServiceBundle:lieux')->find($idMag);
			
			$ptvente = $nomGC . ' - Num Mag ' . $monlieux->getCODAG() . ' - ' . $monlieux->getNOMVILLE();
			
			$comtraitprio = $entity->getTraitPrioCom();
			
			$matenpanne = $entity->getMatenpanne();
			
			
			
			if ($traitementprio == 1)
				{
					$message2 = \Swift_Message::newInstance()
							->setSubject('Message application Nomade Audit Qualité Traitement Prioritaire')
							//->setFrom('mail.application@phareoscloud.org')
							//->setTo('admin.dev.toolbox@phareoscloud.org')
							//code alexis setfrom et setto
							->setFrom($mail1)
							->setTo($mail2)
							->setBody($this->renderView('PhareosNomadeNetServiceBundle:audtech:showmail.html.twig', array(
																	 'date_maj' => $date_maj,
																	 'rsecteur' => $responsecteur,
																	 'ptvente'=> $ptvente,
																	 'comtraitprio'=> $comtraitprio
																	 )))
					;
					
					$this->get('mailer')->send($message2);
					
					$message3 = \Swift_Message::newInstance()
							->setSubject('Message application Nomade Audit Qualité Traitement Prioritaire')
							//->setFrom('mail.application@phareoscloud.org')
							//->setTo('nomade@altea-proprete.com')
							//code alexis setfrom et setto
							->setFrom($mail1)
							->setTo($mail3)
							->setBody($this->renderView('PhareosNomadeNetServiceBundle:audtech:showmail.html.twig', array(
																	 'date_maj' => $date_maj,
																	 'rsecteur' => $responsecteur,
																	 'ptvente'=> $ptvente,
																	 'comtraitprio'=> $comtraitprio
																	 )))
					;
					
					$this->get('mailer')->send($message3);
				}
				
			if ($matenpanne == 1)
				{
				
					$nommatenpanne = $entity->getProdmag()->getNom();
				
					$message4 = \Swift_Message::newInstance()
							->setSubject('Message application Nomade matériel en panne détecté suite à audit qualité')
							//->setFrom('mail.application@phareoscloud.org')
							//->setTo('admin.dev.toolbox@phareoscloud.org')
							//code alexis setfrom et setto
							->setFrom($mail1)
							->setTo($mail2)
							->setBody($this->renderView('PhareosNomadeNetServiceBundle:audtech:showmail2.html.twig', array(
																	 'date_maj' => $date_maj,
																	 'rsecteur' => $responsecteur,
																	 'ptvente'=> $ptvente,
																	 'matenpanne'=> $nommatenpanne
																	 )))
					;
					
					$this->get('mailer')->send($message4);
					
					$message5 = \Swift_Message::newInstance()
							->setSubject('Message application Nomade matériel en panne détecté suite à audit qualité')
							//->setFrom('mail.application@phareoscloud.org')
							//->setTo('nomade@altea-proprete.com')
							//code alexis setfrom et setto
							->setFrom($mail1)
							->setTo($mail3)
							->setBody($this->renderView('PhareosNomadeNetServiceBundle:audtech:showmail2.html.twig', array(
																	 'date_maj' => $date_maj,
																	 'rsecteur' => $responsecteur,
																	 'ptvente'=> $ptvente,
																	 'matenpanne'=> $nommatenpanne
																	 )))
					;
					
					$this->get('mailer')->send($message5);
				}
			
			
			
            return $this->redirect($this->generateUrl('audtech_show', array(
				'idMag' => $idMag,
				'agent1'=>$agent1,
				'agent2'=>$agent2,
				'agent3'=>$agent3,
				'agent4'=>$agent4,
				'agent5'=>$agent5,
				'agent6'=>$agent6,
				'agent7'=>$agent7,
				'agent8'=>$agent8,
				'agent9'=>$agent9,
				'agent10'=>$agent10,
				'responsmag1'=>$responsmag1,
				'responsmag2'=>$responsmag2,
				'lieux'=> $lieux,
				'site'=>$site,
				'id' => $entity->getId(),
				)));
            
        //}

        //return $this->render('PhareosNomadeNetServiceBundle:audtech:new.html.twig', array(
			//'entity' => $entity,
            //'form'   => $form->createView()
        //));
    }

    /**
     * Displays a form to edit an existing audtech entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:audtech')->find($id);
		
		$request = $this->get('request');
		$session = $this->get('session');
		$idMag = $session->get('idMag');
		$idGC = $session->get('idGC');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		
		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find audtech entity.');
        }

        $_SESSION['idLIEUX'] = $idMag;
		
		$editForm = $this->createForm(new audtecheditType(), $entity);
        $deleteForm = $this->createDeleteForm($id);
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		
		$taches = $em->getRepository('PhareosNomadeNetServiceBundle:tachesprogmag')->findBy(array('lieux' => $idMag, 'archive' => 0));
		$audtprogs = $em->getRepository('PhareosNomadeNetServiceBundle:audtprog')->findBy(array('audtech' => $id));

        return $this->render('PhareosNomadeNetServiceBundle:audtech:edit.html.twig', array(
            'idMag' => $idMag,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'lieux'=> $lieux,
			'site'=>$site,
			'taches' => $taches,
			'audtprogs' =>$audtprogs,
			'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing audtech entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:audtech')->find($id);
		
		$session = $this->get('session');
		$idMag = $session->get('idMag');
		$idGC = $session->get('idGC');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		
		$em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:audtech')->find($id);
		
		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
			$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
			$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
			$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
			
			$lieux = $repository_lieux->find($idMag);
		
			$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
			$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find audtech entity.');
        }

        $_SESSION['idLIEUX'] = $idMag;
		
		$editForm   = $this->createForm(new audtecheditType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		$date_maj = $request->request->get('date_maj');
		$date_maj2 = new \DateTime($date_maj);

        //if ($editForm->isValid()) {
            $monlieux = $this->getDoctrine()->getRepository('PhareosNomadeNetServiceBundle:lieux')->find($idMag);
			$entity->setLieux($monlieux);
			$entity->setDateMaj($date_maj2);
			
			$idAudtech = $entity->getId();
			
            $em->persist($entity);
            $em->flush();
			
			$taches = $em->getRepository('PhareosNomadeNetServiceBundle:tachesprogmag')->findBy(array('lieux' => $idMag, 'archive' => 0));
			
			
			
			//faire une boucle avec le nombre de taches pour injection des enregistrements
			if ($taches) {
			
				foreach ($taches as $tache)
					{
						//$entitycontrole = new audtprog();
						
						$entitycontroleid = $tache->getId();
						
						$entitiescontrole = $em->getRepository('PhareosNomadeNetServiceBundle:audtprog')->findBy(array('audtech' => $idAudtech, 'tachesprogmag' => $entitycontroleid));
				
						$com = $request->request->get('com'.$tache->getId());
						
						$etats = $request->request->get('etat'.$tache->getId());
						
						
						
					foreach ($entitiescontrole as $entitycontrole)
					{
						//'foo[bar]', null, true
						if ($etats){
							foreach ($etats as $etat)
								{
									if ($etat == 'propre'){
										$entitycontrole->setPropre(1);
									}
									else {
										$entitycontrole->setPropre(0);
									}
									if ($etat == 'trace'){
										$entitycontrole->setTraces(1);
									}
									else {
										$entitycontrole->setTraces(0);
									}
									if ($etat == 'poussieres'){
										$entitycontrole->setPoussiere(1);
									}
									else {
										$entitycontrole->setPoussiere(0);
									}
									if ($etat == 'taches'){
										$entitycontrole->setTache(1);
									}
									else {
										$entitycontrole->setTache(0);
									}
									if ($etat == 'use'){
										$entitycontrole->setFatuse(1);
									}
									else {
										$entitycontrole->setFatuse(0);
									}
									$entitycontrole->setCom($com);
								}
						}
					}
						
						
						$entitycontrole->setCom($com);
						//$entitycontrole->setTachesprogmag($tache);
						//$entitycontrole->setAudtech($entity);
				
						$em->persist($entitycontrole);
						$em->flush();
					
					}
				
				
			//fin de boucle
			}

            return $this->redirect($this->generateUrl('audtech_show', array('id' => $id)));
        //}

        //return $this->render('PhareosNomadeNetServiceBundle:audtech:edit.html.twig', array(
           // 'idMag' 	  => $idMag,
			//'agent1'	  =>$agent1,
			//'agent2'	  =>$agent2,
			//'agent3'	  =>$agent3,
			//'agent4'	  =>$agent4,
			//'agent5' 	  =>$agent5,
			//'agent6' 	  =>$agent6,
			//'agent7'	  =>$agent7,
			//'agent8'  	  =>$agent8,
			//'agent9'	  =>$agent9,
			//'agent10'	  =>$agent10,
			//'responsmag1'=>$responsmag1,
			//'responsmag2'=>$responsmag2,
			//'lieux'		  => $lieux,
			//'site'		  => $site,
			//'entity'      => $entity,
            //'edit_form'   => $editForm->createView(),
            //'delete_form' => $deleteForm->createView(),
        //));
    }

    /**
     * Deletes a audtech entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosNomadeNetServiceBundle:audtech')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find audtech entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('audtech'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
	
	public function updatesignAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:audtech')->find($id);
		
		$nomsignaltea = $_POST['nomsignaltea'];
		$nomsignclient = $_POST['nomsignclient'];
		
		$request = $this->getRequest();
		
		$dispodir = $request->request->get('dispodir');
		$pointdirres = $request->request->get('pointdirres');
		
		$pointagent = $request->request->get('pointagent');
		
		$filesignaltea = 'img_altea_'.$id.'.png';
		$filesignclient = 'img_client_'.$id.'.png';

		$json1 = $_POST['output1'];
		$img1 = sigJsonToImage($json1, array('imageSize'=>array(298, 190)));

		imagepng($img1, 'imgsign/audtech/'.$filesignaltea);
		imagedestroy($img1);
		
		$json2 = $_POST['output2'];
		$img2 = sigJsonToImage($json2, array('imageSize'=>array(298, 190)));

		imagepng($img2, 'imgsign/audtech/'.$filesignclient);
		imagedestroy($img2);
		
		$entity->setNomsignaltea($nomsignaltea);
		$entity->setNomsignclient($nomsignclient);
		$entity->setImgsignaltea($filesignaltea);
		$entity->setImgsignclient($filesignclient);
		$entity->setDispodir($dispodir);
		$entity->setPointdirres($pointdirres);
		$entity->setPointagent($pointagent);
		
		
		
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find audit entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
		
		

        //if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('audtech_show', array(
			'id' => $id
			)));
        //}

        
    }
	
	public function showpdfAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:audtech')->find($id);
		
		$request = $this->get('request');
		$session = $this->get('session');
		$idMag = $session->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		

		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find audtech entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
		
		$taches = $em->getRepository('PhareosNomadeNetServiceBundle:tachesprogmag')->findBy(array('lieux' => $idMag, 'archive' => 0));
		
		$audtprogs = $em->getRepository('PhareosNomadeNetServiceBundle:audtprog')->findBy(array('audtech' => $id));
		
		$entitiesrecadrage = $em->getRepository('PhareosNomadeNetServiceBundle:recadrage')->findBy(array('lieux' => $idMag),
                                      array('id' => 'desc'));
									  
		$entitiesstockmag = $em->getRepository('PhareosNomadeNetServiceBundle:stockmag')->findBy(array('lieux' => $idMag));
		
		$idLieuxaudit = $entity->getLieux()->getCodag();
		$adruelieux = $entity->getLieux()->getADRUELIEUX();
		$Nomville = $entity->getLieux()->getNOMVILLE();
		$dateMaj = $entity->getDatemaj()->format('d-m-Y');
		
		//$dateMaj2 = new \DateTime($dateMaj);

        $html = $this->render('PhareosNomadeNetServiceBundle:audtech:show2pdfaudtech.html.twig', array(
			'idMag' => $idMag,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'lieux'=> $lieux,
			'site'=>$site,
			'taches' =>$taches,
			'audtprogs' =>$audtprogs,
			'entity'      => $entity,
			'entitiesrecadrage' => $entitiesrecadrage,
			'entitiesstockmag' => $entitiesstockmag,
            'delete_form' => $deleteForm->createView(),
        ));
		
		$html2pdf = new \Html2Pdf_Html2Pdf('L','A4','fr', false, 'ISO-8859-1');
		//$html2pdf->setModeDebug();
		//$html2pdf->setTestIsImage($mode = true);
		//$html2pdf->setTestIsDeprecated($mode = true);
		//$html2pdf->setTestTdInOnePage($mode = true);
		$html2pdf->pdf->SetDisplayMode('real');
		$html2pdf->setDefaultFont('Arial');
		$html2pdf->writeHTML($html2pdf->getHtmlFromPage(utf8_decode($html)), isset($_GET['vuehtml']));
		//$html2pdf->writeHTML(utf8_decode($html), isset($_GET['vuehtml']));
		$fichier = $html2pdf->Output('Audit_Mag'.$idLieuxaudit.'_'.$dateMaj.'_'.$id.'.pdf', 'D');

		$response = new Response();
		$response->clearHttpHeaders();
		$response->setContent(file_get_contents($fichier));
		$response->headers->set('Content-Type', 'application/force-download');
		$response->headers->set('Content-disposition', 'filename='. $fichier);

		return $response;
    }
}
