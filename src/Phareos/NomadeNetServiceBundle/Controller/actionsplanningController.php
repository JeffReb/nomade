<?php

namespace Phareos\NomadeNetServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\NomadeNetServiceBundle\Entity\actionsplanning;
use Phareos\NomadeNetServiceBundle\Form\actionsplanningType;

/**
 * actionsplanning controller.
 *
 */
class actionsplanningController extends Controller
{
    /**
     * Lists all actionsplanning entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosNomadeNetServiceBundle:actionsplanning')->findAll();

        return $this->render('PhareosNomadeNetServiceBundle:actionsplanning:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a actionsplanning entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:actionsplanning')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find actionsplanning entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosNomadeNetServiceBundle:actionsplanning:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new actionsplanning entity.
     *
     */
    public function newAction()
    {
        $entity = new actionsplanning();
        $form   = $this->createForm(new actionsplanningType(), $entity);

        return $this->render('PhareosNomadeNetServiceBundle:actionsplanning:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new actionsplanning entity.
     *
     */
    public function createAction()
    {
        $entity  = new actionsplanning();
        $request = $this->getRequest();
        $form    = $this->createForm(new actionsplanningType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('actionsplanning_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosNomadeNetServiceBundle:actionsplanning:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing actionsplanning entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:actionsplanning')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find actionsplanning entity.');
        }

        $editForm = $this->createForm(new actionsplanningType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosNomadeNetServiceBundle:actionsplanning:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing actionsplanning entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:actionsplanning')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find actionsplanning entity.');
        }

        $editForm   = $this->createForm(new actionsplanningType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('actionsplanning_edit', array('id' => $id)));
        }

        return $this->render('PhareosNomadeNetServiceBundle:actionsplanning:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a actionsplanning entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosNomadeNetServiceBundle:actionsplanning')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find actionsplanning entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('actionsplanning'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
