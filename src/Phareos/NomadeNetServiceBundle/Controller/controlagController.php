<?php

namespace Phareos\NomadeNetServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\NomadeNetServiceBundle\Entity\controlag;
use Phareos\NomadeNetServiceBundle\Form\controlagType;

use Phareos\NomadeNetServiceBundle\Entity\lieux;
use Phareos\NomadeNetServiceBundle\Entity\auditmensuel;

/**
 * controlag controller.
 *
 */
class controlagController extends Controller
{
    /**
     * Lists all controlag entities.
     *
     */
    public function indexAction()
    {
        
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$request = $this->get('request');
		
		$session = $this->get('session');
		
		$idMag = $session->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		
		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}

        $entities = $em->getRepository('PhareosNomadeNetServiceBundle:controlag')->findAll();

        return $this->render('PhareosNomadeNetServiceBundle:controlag:index.html.twig', array(
            'idMag' => $idMag,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'lieux'=> $lieux,
			'site'=>$site,
			'entities' => $entities
        ));
    }
	
	public function indexacclientAction()
    {
        
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$request = $this->get('request');
		
		$session = $this->get('session');
		
		$idMag = $session->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		
		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}

        $entities = $em->getRepository('PhareosNomadeNetServiceBundle:controlag')->findAll();

        return $this->render('PhareosNomadeNetServiceBundle:controlag:indexacclient.html.twig', array(
            'idMag' => $idMag,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'lieux'=> $lieux,
			'site'=>$site,
			'entities' => $entities
        ));
    }

    /**
     * Finds and displays a controlag entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:controlag')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find controlag entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosNomadeNetServiceBundle:controlag:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new controlag entity.
     *
     */
    public function newAction()
    {
        $entity = new controlag();
        $form   = $this->createForm(new controlagType(), $entity);

        return $this->render('PhareosNomadeNetServiceBundle:controlag:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new controlag entity.
     *
     */
    public function createAction()
    {
        
		$em = $this->getDoctrine()->getEntityManager();
		
		
		
		$entitieslieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux')->findBy(array('MATRICULE_GC' => 'SE', 'respsect' => 'AIT-ABBAS', 'INACTIF' =>0));
		
		$dateanne = new \DateTime();
		$numannee = $dateanne->format('Y');
		
		foreach ($entitieslieux as $entitylieux)
				{
				
					$lieuxId = $entitylieux->getId();
					$respsect = $entitylieux->getRespsect();
					
					for ($i = 1; $i <= 12; $i++) { //pour chaque mois de l'année
						
						if ($i == 1){
							$j = '01';
						}
						else if ($i == 2){
							$j = '02';
						}
						else if ($i == 3){
							$j = '03';
						}
						else if ($i == 4){
							$j = '04';
						}
						else if ($i == 5){
							$j = '05';
						}
						else if ($i == 6){
							$j = '06';
						}
						else if ($i == 7){
							$j = '07';
						}
						else if ($i == 8){
							$j = '08';
						}
						else if ($i == 9){
							$j = '09';
						}
						else if ($i == 10){
							$j = '10';
						}
						else if ($i == 11){
							$j = '11';
						}
						else {
							$j = '12';
						}
						
					
						
						$entity  = new controlag();
						$entity->setMoi($j);
						$entity->setAnnee($numannee);
						$entity->setLieux($entitylieux);
						$entity->setRespsect($respsect);
						
						
						
						$em->persist($entity);
						$em->flush();
				
				
					}
				}
		$entitiesagcont = $em->getRepository('PhareosNomadeNetServiceBundle:controlag')->findBy(array('respsect' => 'AIT-ABBAS'));
		
		foreach ($entitiesagcont as $entityagcont)
			{
				$entityagcontlieuxid = $entityagcont->getLieux()->getId();
				$entityagcontmois = $entityagcont->getMoi();
				
				$entitiesag = $em->getRepository('PhareosNomadeNetServiceBundle:auditmensuel')->findOneBy(array('lieux' => $entityagcontlieuxid, 'nummois' => $entityagcontmois),
																							array('datereunion' => 'desc'));
																						
				if ($entitiesag){//Fiche de rendez vous effectué
				
						$signature = $entitiesag->getNomsignaltea();
						$dispodir = $entitiesag->getDispodir();
						$motifabs = $entitiesag->getMotifindispo();
						$dernumag = $entitiesag->getId();
						
						
						
						$entityagcont->setRdvous(1);
						$entityagcont->setDernumag($dernumag);
						if (!$signature)//pas de signature problème altéa
							{
								$entityagcont->setSigne(0);
							}
						else //signature ou croix
							{
								$entityagcont->setSigne(1);
								if ($dispodir == 1)//directrice indisponible
									{
										$entityagcont->setDispodir(1);
										$entityagcont->setMotifabs($motifabs);
									}
							}
						
						//$entityagcont->setDernumag($entityagcontmois);
								
							
						}
						
						
						$em->persist($entityagcont);
						$em->flush();
			}
		
		
		//$entity  = new controlag();
        //$request = $this->getRequest();
        //$form    = $this->createForm(new controlagType(), $entity);
        //$form->bindRequest($request);

        return $this->redirect($this->generateUrl('controlag'));

        
    }

    /**
     * Displays a form to edit an existing controlag entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:controlag')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find controlag entity.');
        }

        $editForm = $this->createForm(new controlagType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosNomadeNetServiceBundle:controlag:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing controlag entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:controlag')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find controlag entity.');
        }

        $editForm   = $this->createForm(new controlagType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('controlag_edit', array('id' => $id)));
        }

        return $this->render('PhareosNomadeNetServiceBundle:controlag:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a controlag entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosNomadeNetServiceBundle:controlag')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find controlag entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('controlag'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
