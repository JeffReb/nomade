<?php

namespace Phareos\NomadeNetServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\NomadeNetServiceBundle\Entity\tachesprogmag;
use Phareos\NomadeNetServiceBundle\Form\tachesprogmagType;
use Phareos\NomadeNetServiceBundle\Form\tachesprogmagdelType;

use Phareos\NomadeNetServiceBundle\Entity\planingdate;
use Phareos\NomadeNetServiceBundle\Form\planingdateType;

use Phareos\NomadeNetServiceBundle\Entity\planjs;
use Phareos\NomadeNetServiceBundle\Form\planjsType;

/**
 * tachesprogmag controller.
 *
 */
class tachesprogmagController extends Controller
{
    /**
     * Lists all tachesprogmag entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosNomadeNetServiceBundle:tachesprogmag')->findAll();

        return $this->render('PhareosNomadeNetServiceBundle:tachesprogmag:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a tachesprogmag entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:tachesprogmag')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find tachesprogmag entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosNomadeNetServiceBundle:tachesprogmag:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new tachesprogmag entity.
     *
     */
    public function newAction()
    {
        $session = $this->get('session');
		$request = $this->get('request');
		$idMag = $session->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		
		$nomGC = $session->get('nomGC');
		
		
		$em = $this->getDoctrine()->getEntityManager();
		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		
		$idGC = $session->get('idGC');
		
		$entities = $em->getRepository('PhareosNomadeNetServiceBundle:tachesprogmag')->findBy(array('nomgc' => $nomGC, 'lieux' => $idMag, 'archive' => 0));
		
		$entity = new tachesprogmag();
		$_SESSION['idgc'] = $idGC;
        $form   = $this->createForm(new tachesprogmagType(), $entity);
		$editForm = $this->createForm(new tachesprogmagType(), $entity);
		
		$formdel   = $this->createForm(new tachesprogmagdelType(), $entity);

        return $this->render('PhareosNomadeNetServiceBundle:tachesprogmag:index.html.twig', array(
            'idMag' => $idMag,
			'lieux'=> $lieux,
			'site'=> $site,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'entities' => $entities,
			'entity' => $entity,
			'edit_form'   => $editForm->createView(),
            'form'   => $form->createView(),
			'formdel' => $formdel->createView()
        ));
    }
	
	/**
     * Displays a form to create a new tachesprogmag entity.
     *
     */
    public function plannewAction()
    {
        $session = $this->get('session');
		$request = $this->get('request');
		$idMag = $session->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		
		$nomGC = $session->get('nomGC');
		
		
		$em = $this->getDoctrine()->getEntityManager();
		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		
		$idGC = $session->get('idGC');
		
		$entities = $em->getRepository('PhareosNomadeNetServiceBundle:tachesprogmag')->findBy(array('nomgc' => $nomGC, 'lieux' => $idMag, 'archive' => 0));
		$entitiespdate = $em->getRepository('PhareosNomadeNetServiceBundle:planingdate')->findBy(array('lieux' => $idMag),
                                      array('agent2' => 'asc'));
									  
		$enitiespdatefirst = $em->getRepository('PhareosNomadeNetServiceBundle:planingdate')->findOneBy(array('lieux' => $idMag),
                                      array('datedeb' => 'asc'));
									  
		
									  
		$entitypdate = new planingdate();
        $formpdate   = $this->createForm(new planingdateType(), $entitypdate);
		
		$entity = new tachesprogmag();
		$_SESSION['idgc'] = $idGC;
        $form   = $this->createForm(new tachesprogmagType(), $entity);
		$editForm = $this->createForm(new tachesprogmagType(), $entity);
		
		$entityJS = new planjs();
		$_SESSION['idMag'] = $idMag;
        $formJS   = $this->createForm(new planjsType(), $entityJS);

        return $this->render('PhareosNomadeNetServiceBundle:tachesprogmag:plannewindex.html.twig', array(
            'idMag' => $idMag,
			'lieux'=> $lieux,
			'site'=> $site,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'entities' => $entities,
			'entity' => $entity,
			'entitiespdate' => $entitiespdate,
			'entitypdate' => $entitypdate,
			'edit_form'   => $editForm->createView(),
            'form'   => $form->createView(),
			'formpdate' => $formpdate->createView(),
			'entityJS' => $entityJS,
            'formJS'   => $formJS->createView()
        ));
    }

    /**
     * Creates a new tachesprogmag entity.
     *
     */
    public function createAction()
    {
        $session = $this->get('session');
		$nomGC = $session->get('nomGC');
		$idMag = $session->get('idMag');
		
		$entity  = new tachesprogmag();
        $request = $this->getRequest();
        $form    = $this->createForm(new tachesprogmagType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
		
			if ($entity->getDuree() == null){
			}
			else {
			$myTextduree = $entity->getDuree()->format('H:i');// '00:05'
			
			$partsduree = explode(':', $myTextduree);//$partsduree[0]'00':$partsduree[1]'05'
			
			$partsminutes = ($partsduree[0]*60) + $partsduree[1];
			
			
			$dureeTotMinutes = $partsduree[1] * $entity->getQte();
			//$dureeTotMinutes = 5;
			
			//$dureeTotdateString = time('m',$dureeTotMinutes);
			
			$dureeTotdate  = date('c',mktime(0, $dureeTotMinutes, 0, 0, 0, 0));
			
			$dureeTotdateD = new \DateTime($dureeTotdate);
			
			//$date_maj2 = new \DateTime($dureeTotMinutes);
			
			//$dureeHeure = idate('i',$duree);
			//$dureeMinutes = idate('i',$duree);
			
			//$dureetot = $dureeNum * $entity->getQte();
			$entity -> setDureetot($dureeTotdateD);
			$entity -> setDureerest($dureeTotdateD);
			}
			
			
            $em = $this->getDoctrine()->getEntityManager();
			$entitylieu = $em->getRepository('PhareosNomadeNetServiceBundle:lieux')->find($idMag);
			$entity -> setNomgc($nomGC);
			$entity -> setLieux($entitylieu);
			$entity -> setLieuxid2($idMag);
			$entity -> setArchive(0);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tachesprogmag_new', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosNomadeNetServiceBundle:tachesprogmag:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing tachesprogmag entity.
     *
     */
    public function editAction($id)
    {
        $session = $this->get('session');
		$request = $this->get('request');
		$idMag = $session->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		
		$nomGC = $session->get('nomGC');
		
		
		$em = $this->getDoctrine()->getEntityManager();
		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:tachesprogmag')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find tachesprogmag entity.');
        }

        $editForm = $this->createForm(new tachesprogmagType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosNomadeNetServiceBundle:tachesprogmag:edit.html.twig', array(
            'idMag' => $idMag,
			'lieux'=> $lieux,
			'site'=> $site,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing tachesprogmag entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:tachesprogmag')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find tachesprogmag entity.');
        }

        $editForm   = $this->createForm(new tachesprogmagType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
			
			if ($entity->getDuree() == null){
			}
			else {
			
			$myTextduree = $entity->getDuree()->format('H:i');// '00:05'
			
			$partsduree = explode(':', $myTextduree);//$partsduree[0]'00':$partsduree[1]'05'
			
			$partsminutes = ($partsduree[0]*60) + $partsduree[1];
			
			
			$dureeTotMinutes = $partsduree[1] * $entity->getQte();
			//$dureeTotMinutes = 5;
			
			//$dureeTotdateString = time('m',$dureeTotMinutes);
			
			$dureeTotdate  = date('c',mktime(0, $dureeTotMinutes, 0, 0, 0, 0));
			
			$dureeTotdateD = new \DateTime($dureeTotdate);
			
			//$date_maj2 = new \DateTime($dureeTotMinutes);
			
			//$dureeHeure = idate('i',$duree);
			//$dureeMinutes = idate('i',$duree);
			
			//$dureetot = $dureeNum * $entity->getQte();
			$entity -> setDureetot($dureeTotdateD);
			$entity -> setDureerest($dureeTotdateD);
			
			}
		
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tachesprogmag_new', array('id' => $id)));
        }

        return $this->render('PhareosNomadeNetServiceBundle:tachesprogmag:edit.html.twig', array(
			'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a tachesprogmag entity.
     *
     */
    public function deleteAction($id)
    {
        
            $session = $this->get('session');
			$request = $this->get('request');
			$idMag = $session->get('idMag');
			
			$em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosNomadeNetServiceBundle:tachesprogmag')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find tachesprogmag entity.');
            }

            $entity->setArchive(1);
			$em->persist($entity);
            $em->flush();
        

        return $this->redirect($this->generateUrl('PhareosNomadeNetServiceBundle_homemag', array('id' => $idMag)));
    }
	
	public function deletecheckAction()
    {
        
            $session = $this->get('session');
			//$session = $this->get('session');
			//$request = $this->get('request');
			$idMag = $session->get('idMag');
			
			$nomGC = $session->get('nomGC');
			
			$request = $this->getRequest();
			
			$em = $this->getDoctrine()->getEntityManager();
			
			$entities = $em->getRepository('PhareosNomadeNetServiceBundle:tachesprogmag')->findBy(array('nomgc' => $nomGC, 'lieux' => $idMag, 'archive' => 0));
            
			//$entity = $em->getRepository('PhareosNomadeNetServiceBundle:tachesprogmag')->find($id);

            foreach ($entities as $entitytpmag)
				{
			
					$entitytpmagid = $entitytpmag->getId();
					
					$entity = $em->getRepository('PhareosNomadeNetServiceBundle:tachesprogmag')->find($entitytpmagid);
					
					if (!$entity) {
						throw $this->createNotFoundException('Unable to find tachesprogmag entity.');
					}
					
					$checkdel = $request->request->get('checkdel'.$entitytpmagid);
					
					if ($checkdel == 'OK'){

						$entity->setArchive(1);
						$em->persist($entity);
						$em->flush();
					}
					
				}
        return $this->redirect($this->generateUrl('tachesprogmag_new', array('id' => $idMag)));

        //return $this->redirect($this->generateUrl('PhareosNomadeNetServiceBundle_homemag', array('id' => $idMag)));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
