<?php

namespace Phareos\NomadeNetServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\NomadeNetServiceBundle\Entity\testarray;
use Phareos\NomadeNetServiceBundle\Form\testarrayType;

/**
 * testarray controller.
 *
 */
class testarrayController extends Controller
{
    /**
     * Lists all testarray entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosNomadeNetServiceBundle:testarray')->findAll();

        return $this->render('PhareosNomadeNetServiceBundle:testarray:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a testarray entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:testarray')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find testarray entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosNomadeNetServiceBundle:testarray:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new testarray entity.
     *
     */
    public function newAction()
    {
        $entity = new testarray();
        $form   = $this->createForm(new testarrayType(), $entity);

        return $this->render('PhareosNomadeNetServiceBundle:testarray:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new testarray entity.
     *
     */
    public function createAction()
    {
        $entity  = new testarray();
        $request = $this->getRequest();
        $form    = $this->createForm(new testarrayType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('testarray_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosNomadeNetServiceBundle:testarray:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing testarray entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:testarray')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find testarray entity.');
        }

        $editForm = $this->createForm(new testarrayType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosNomadeNetServiceBundle:testarray:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing testarray entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:testarray')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find testarray entity.');
        }

        $editForm   = $this->createForm(new testarrayType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('testarray_edit', array('id' => $id)));
        }

        return $this->render('PhareosNomadeNetServiceBundle:testarray:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a testarray entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosNomadeNetServiceBundle:testarray')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find testarray entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('testarray'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
