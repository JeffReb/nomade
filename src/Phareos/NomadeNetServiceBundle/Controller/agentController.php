<?php

namespace Phareos\NomadeNetServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Response;
use Phareos\NomadeNetServiceBundle\Entity\agent;
use Phareos\NomadeNetServiceBundle\Entity\lieux;
use Phareos\NomadeNetServiceBundle\Form\agentType;

/**
 * agent controller.
 *
 */
class agentController extends Controller
{
    /**
     * Lists all agent entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosNomadeNetServiceBundle:agent')->findAll();

        return $this->render('PhareosNomadeNetServiceBundle:agent:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a agent entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:agent')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find agent entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosNomadeNetServiceBundle:agent:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new agent entity.
     *
     */
    public function newAction()
    {
        $entity = new agent();
        $form   = $this->createForm(new agentType(), $entity);

        return $this->render('PhareosNomadeNetServiceBundle:agent:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new agent entity.
     *
     */
    public function createAction()
    {
        $entity  = new agent();
        $request = $this->getRequest();
        $form    = $this->createForm(new agentType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('agent_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosNomadeNetServiceBundle:agent:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing agent entity.
     *
     */
    public function editAction($id)
    {
        //traitement du block twig en amont (menu de l'application)
		$session = $this->get('session');
		$request = $this->get('request');
		$idMag = $session->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		$lieux = $repository_lieux->find($idMag);
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		// fin du traitement

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:agent')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find agent entity.');
        }

        $editForm = $this->createForm(new agentType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosNomadeNetServiceBundle:agent:edit.html.twig', array(
            'idMag' => $idMag,
			'lieux'=> $lieux,
			'site'=>$site,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing agent entity.
     *
     */
    public function updateAction($id)
    {
        //traitement du block twig en amont (menu de l'application)
		$session = $this->get('session');
		$request = $this->get('request');
		$idMag = $session->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		$lieux = $repository_lieux->find($idMag);
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		// fin du traitement

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:agent')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find agent entity.');
        }

        $editForm   = $this->createForm(new agentType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        //if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('PhareosNomadeNetServiceBundle_homemag', array('id' => $idMag)));
        //}

        //return $this->redirect($this->generateUrl('PhareosNomadeNetServiceBundle_homemag', array('id' => $idMag)));
		//return $this->render('PhareosNomadeNetServiceBundle:agent:edit.html.twig', array(
            //'idMag' => $idMag,
			//'lieux'=> $lieux,
			//'site'=>$site,
			//'agent1'=>$agent1,
			//'agent2'=>$agent2,
			//'agent3'=>$agent3,
			//'agent4'=>$agent4,
			//'agent5'=>$agent5,
			//'agent6'=>$agent6,
			//'agent7'=>$agent7,
			//'agent8'=>$agent8,
			//'agent9'=>$agent9,
			//'agent10'=>$agent10,
			//'responsmag1'=>$responsmag1,
			//'responsmag2'=>$responsmag2,
			//'entity'      => $entity,
            //'edit_form'   => $editForm->createView(),
            //'delete_form' => $deleteForm->createView(),
        //));
    }

    /**
     * Deletes a agent entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosNomadeNetServiceBundle:agent')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find agent entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('agent'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
	
	public function newaffectationAction()
    {
			
		// On récupère l'EntityManager.
		$em = $this->getDoctrine()->getEntityManager();
		
		//traitement du block twig en amont (menu de l'application)
		$session = $this->get('session');
		$idMag = $session->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		

		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		
		
		
		//on recup les entites agents
        $entities = $em->getRepository('PhareosNomadeNetServiceBundle:agent')->findAll();

        return $this->render('PhareosNomadeNetServiceBundle:agent:affectation.index.html.twig', array(
            'idMag' => $idMag,
			'lieux'=> $lieux,
			'site'=>$site,
			'entities' => $entities,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2
        ));
	
	}
	
	public function createaffectationAction()
    {
		
		$session = $this->get('session');
		$request = $this->get('request');
		$idMag = $session->get('idMag');
		$idAgent = $request->query->get('idAgent');
		
		
		
		
		

		// On récupère l'EntityManager.
		$em = $this->getDoctrine()->getEntityManager();
		
		
		
		// On récupère le mag selectionner par son id.
		$entity = $em->getRepository('PhareosNomadeNetServiceBundle:lieux')->find($idMag);
		
		// On test si le mag existe .... normalement Yes !!! ;)
		if (!$entity) {
            throw $this->createNotFoundException('Unable to find lieux entity.');
        }
		$IdAgentPresence1 = $entity->getAgent1Id();
		$IdAgentPresence2 = $entity->getAgent2Id();
		$IdAgentPresence3 = $entity->getAgent3Id();
		$IdAgentPresence4 = $entity->getAgent4Id();
		$IdAgentPresence5 = $entity->getAgent5Id();
		$IdAgentPresence6 = $entity->getAgent6Id();
		$IdAgentPresence7 = $entity->getAgent7Id();
		$IdAgentPresence8 = $entity->getAgent8Id();
		$IdAgentPresence9 = $entity->getAgent9Id();
		$IdAgentPresence10 = $entity->getAgent10Id();
		
		// on récup l'agent sélectionner !!
		$agents = $em->getRepository('PhareosNomadeNetServiceBundle:agent')->find($idAgent);

		
		
		if(empty($IdAgentPresence1) or $IdAgentPresence1 == 1)
		{
			//On applique la modif sur le champ!!! lol le champ Agent1Id
			$entity->setAgent1Id($idAgent);
		}
		
		else if(empty($IdAgentPresence2) or $IdAgentPresence2 == 1 )
		{
			$entity->setAgent2Id($idAgent);
		}
		else if(empty($IdAgentPresence3) or $IdAgentPresence3 == 1)
		{
			$entity->setAgent3Id($idAgent);
		}
		else if(empty($IdAgentPresence4) or $IdAgentPresence4 == 1)
		{
			$entity->setAgent4Id($idAgent);
		}
		else if(empty($IdAgentPresence5) or $IdAgentPresence5 == 1)
		{
			$entity->setAgent5Id($idAgent);
		}
		else if(empty($IdAgentPresence6) or $IdAgentPresence6 == 1)
		{
			$entity->setAgent6Id($idAgent);
		}
		else if(empty($IdAgentPresence7) or $IdAgentPresence7 == 1)
		{
			$entity->setAgent7Id($idAgent);
		}
		else if(empty($IdAgentPresence8) or $IdAgentPresence8 == 1)
		{
			$entity->setAgent8Id($idAgent);
		}
		else if(empty($IdAgentPresence9) or $IdAgentPresence9 == 1)
		{
			$entity->setAgent9Id($idAgent);
		}
		else if (empty($IdAgentPresence10) or $IdAgentPresence10 == 1)
		{
			$entity->setAgent10Id($idAgent);
		}
		else
		{
			throw $this->createNotFoundException('Impossible de créer un nouvelle agent vous devez en supprimer un.');
		}
		
		
		
		

		

		// On persiste juste l'agent1id du Mag
		
		$em->persist($entity);

		// On déclenche l'enregistrement.
		$em->flush();

		//return new Response('OK');
		return $this->redirect($this->generateUrl('PhareosNomadeNetServiceBundle_homemag', array('id' => $idMag,)));
	
	}
	
	public function deleteaffectationAction()
    {
			
		// On récupère l'EntityManager.
		$em = $this->getDoctrine()->getEntityManager();
		
		//traitement du block twig en amont (menu de l'application)
		$session = $this->get('session');
		$idMag = $session->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		
		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		
		
		//on recup les entites agents du magasin
		
        $entity1 = $em->getRepository('PhareosNomadeNetServiceBundle:agent')->find($idAgent1);
		$entity2 = $em->getRepository('PhareosNomadeNetServiceBundle:agent')->find($idAgent2);
		$entity3 = $em->getRepository('PhareosNomadeNetServiceBundle:agent')->find($idAgent3);
		$entity4 = $em->getRepository('PhareosNomadeNetServiceBundle:agent')->find($idAgent4);
		$entity5 = $em->getRepository('PhareosNomadeNetServiceBundle:agent')->find($idAgent5);
		$entity6 = $em->getRepository('PhareosNomadeNetServiceBundle:agent')->find($idAgent6);
		$entity7 = $em->getRepository('PhareosNomadeNetServiceBundle:agent')->find($idAgent7);
		$entity8 = $em->getRepository('PhareosNomadeNetServiceBundle:agent')->find($idAgent8);
		$entity9 = $em->getRepository('PhareosNomadeNetServiceBundle:agent')->find($idAgent9);
		$entity10 = $em->getRepository('PhareosNomadeNetServiceBundle:agent')->find($idAgent10);
		
        return $this->render('PhareosNomadeNetServiceBundle:agent:deleteaffectation.index.html.twig', array(
            'idMag' => $idMag,
			'lieux'=> $lieux,
			'site'=>$site,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2
        ));
	
	}
	
	public function deleteconfirmaffectationAction()
    {
		$session = $this->get('session');
		$request = $this->get('request');//NumidAgent
		$idMag = $session->get('idMag');
		$NumidAgent = $request->query->get('NumidAgent');
		
		$em = $this->getDoctrine()->getEntityManager();
		$entity = $em->getRepository('PhareosNomadeNetServiceBundle:lieux')->find($idMag);
		
		if ($NumidAgent == 1)
		{
			$entity->setAgent1Id(1);
		}
		else if ($NumidAgent == 2)
		{
			$entity->setAgent2Id(1);
		}
		else if ($NumidAgent == 3)
		{
			$entity->setAgent3Id(1);
		}
		else if ($NumidAgent == 4)
		{
			$entity->setAgent4Id(1);
		}
		else if ($NumidAgent == 5)
		{
			$entity->setAgent5Id(1);
		}
		else if ($NumidAgent == 6)
		{
			$entity->setAgent6Id(1);
		}
		else if ($NumidAgent == 7)
		{
			$entity->setAgent7Id(1);
		}
		else if ($NumidAgent == 8)
		{
			$entity->setAgent8Id(1);
		}
		else if ($NumidAgent == 9)
		{
			$entity->setAgent9Id(1);
		}
		else
		{
			$entity->setAgent10Id(1);
		}
		
		$em->persist($entity);
		
		$em->flush();
		
		return $this->redirect($this->generateUrl('PhareosNomadeNetServiceBundle_homemag', array('id' => $idMag,)));
		
	}
	
	public function replaceconfirmaffectationAction()
    {
		$session = $this->get('session');
		$request = $this->get('request');//NumidAgent
		$idMag = $session->get('idMag');
		$NumidAgent = $request->query->get('NumidAgent');
		
		$em = $this->getDoctrine()->getEntityManager();
		$entity = $em->getRepository('PhareosNomadeNetServiceBundle:lieux')->find($idMag);
		
		if ($NumidAgent == 1)
		{
			$entity->setAgent1Id(1);
		}
		else if ($NumidAgent == 2)
		{
			$entity->setAgent2Id(1);
		}
		else if ($NumidAgent == 3)
		{
			$entity->setAgent3Id(1);
		}
		else if ($NumidAgent == 4)
		{
			$entity->setAgent4Id(1);
		}
		else if ($NumidAgent == 5)
		{
			$entity->setAgent5Id(1);
		}
		else if ($NumidAgent == 6)
		{
			$entity->setAgent6Id(1);
		}
		else if ($NumidAgent == 7)
		{
			$entity->setAgent7Id(1);
		}
		else if ($NumidAgent == 8)
		{
			$entity->setAgent8Id(1);
		}
		else if ($NumidAgent == 9)
		{
			$entity->setAgent9Id(1);
		}
		else
		{
			$entity->setAgent10Id(1);
		}
		
		$em->persist($entity);
		
		$em->flush();
		
		return $this->redirect($this->generateUrl('agent_newaffectation', array('id' => $idMag,)));
		
	}
	
	
}
