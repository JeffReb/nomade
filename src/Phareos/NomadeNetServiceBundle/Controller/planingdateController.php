<?php

namespace Phareos\NomadeNetServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\NomadeNetServiceBundle\Entity\planingdate;
use Phareos\NomadeNetServiceBundle\Form\planingdateType;

use Phareos\NomadeNetServiceBundle\Entity\planjs;
use Phareos\NomadeNetServiceBundle\Form\planjsType;

/**
 * planingdate controller.
 *
 */
class planingdateController extends Controller
{
    /**
     * Lists all planingdate entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosNomadeNetServiceBundle:planingdate')->findAll();

        return $this->render('PhareosNomadeNetServiceBundle:planingdate:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a planingdate entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:planingdate')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find planingdate entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosNomadeNetServiceBundle:planingdate:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new planingdate entity.
     *
     */
    public function newAction($id)
    {
        
		$session = $this->get('session');
		$request = $this->get('request');
		$idGrdCompte = $session->get('idGC');
		$idMag = $session->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		
		$em = $this->getDoctrine()->getEntityManager();
		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		
		$entitypdate = new planingdate();
        $form   = $this->createForm(new planingdateType(), $entitypdate);

        return $this->render('PhareosNomadeNetServiceBundle:planingdate:new.html.twig', array(
            'idMag' => $idMag,
			'lieux'=> $lieux,
			'site'=>$site,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'entitypdate' => $entitypdate,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new planingdate entity.
     *
     */
    public function createAction()
    {
        $session = $this->get('session');
		$idMag = $session->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		
		$em = $this->getDoctrine()->getEntityManager();
			$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
			$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
			$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
			$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
			
			$lieux = $repository_lieux->find($idMag);
		
			$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
			$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
			
			$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		
		$entitypdate  = new planingdate();
        $request = $this->getRequest();
        $formpdate    = $this->createForm(new planingdateType(), $entitypdate);
        $formpdate->bindRequest($request);
		
		$datedeb = $request->request->get('datedeb');
		$datedeb2 = new \DateTime($datedeb);
		
		$entitylieu = $em->getRepository('PhareosNomadeNetServiceBundle:lieux')->find($idMag);

        if ($formpdate->isValid()) {
			$entitypdate->setDatedeb($datedeb2);
			$entitypdate->setLieux($entitylieu);
			
			$semaine = date('W', strtotime($datedeb));
			
			$mardi = date('d-m-Y',strtotime(date('d-m-Y', strtotime($datedeb)) . " +1 day")); 
			$mardi2 = new \DateTime($mardi);
			
			$mercredi = date('d-m-Y',strtotime(date('d-m-Y', strtotime($datedeb)) . " +2 day")); 
			$mercredi2 = new \DateTime($mercredi);
			
			$jeudi = date('d-m-Y',strtotime(date('d-m-Y', strtotime($datedeb)) . " +3 day")); 
			$jeudi2 = new \DateTime($jeudi);
			
			$vendredi = date('d-m-Y',strtotime(date('d-m-Y', strtotime($datedeb)) . " +4 day")); 
			$vendredi2 = new \DateTime($vendredi);
			
			$samedi = date('d-m-Y',strtotime(date('d-m-Y', strtotime($datedeb)) . " +5 day")); 
			$samedi2 = new \DateTime($samedi);
			
			$pdateId = $entitypdate->getId();
			
			//$mardid  = mktime(0, 0, 0, date("m")  , date("d")+1, date("Y"));
			
			$entitypdate->setLundid($datedeb2);
			$entitypdate->setMardid($mardi2);
			$entitypdate->setMercredid($mercredi2);
			$entitypdate->setJeudid($jeudi2);
			$entitypdate->setVendredid($vendredi2);
			$entitypdate->setSamedid($samedi2);
			$entitypdate->setLieuxid2($idMag);
			$entitypdate->setSemaine($semaine);
            
            $em->persist($entitypdate);
            $em->flush();
			
			//génération auto de tous les tachesprogmag de tous les jours et lessivage 1
			
			$entitiesTachesprogmag = $em->getRepository('PhareosNomadeNetServiceBundle:tachesprogmag')->findBy(array('lieux' => $idMag, 'archive' => 0));
			
			$entitypdate2 = $em->getRepository('PhareosNomadeNetServiceBundle:planingdate')->find($pdateId);
			
			
			
			foreach ($entitiesTachesprogmag as $entityTachesprogmag)
			{
				$entityNomfrequence = $entityTachesprogmag->getFrequence()->getNom();
				$entityNomfonction = $entityTachesprogmag->getFonction()->getNom();
				$entityNom = $entityTachesprogmag->getTache();
				if ($entityNomfrequence == 'Tous les jours' and $entityNomfonction == '1 - Lessivage Récurrent'){
					//injection des tachesprogs de tous les jours
					
					$entityplanjs  = new planjs();
					$entityplanjs->setTache($entityNom);
					$entityplanjs->setLundi('Oui');
					$entityplanjs->setMardi('Oui');
					$entityplanjs->setMercredi('Oui');
					$entityplanjs->setJeudi('Oui');
					$entityplanjs->setVendredi('Oui');
					$entityplanjs->setSamedi('Oui');
					$entityplanjs->setTachesprogmag($entityTachesprogmag);
					$entityplanjs->setPlaningdate($entitypdate2);
					
					$em->persist($entityplanjs);
					$em->flush();
					
			
				}
			}
			foreach ($entitiesTachesprogmag as $entityTachesprogmag)
			{
				$entityNomfrequence = $entityTachesprogmag->getFrequence()->getNom();
				$entityNomfonction = $entityTachesprogmag->getFonction()->getNom();
				$entityNom = $entityTachesprogmag->getTache();
				if ($entityNomfrequence == 'Tous les jours' and $entityNomfonction == '2 - Nettoyage Simple'){
					//injection des tachesprogs de tous les jours
					
					$entityplanjs  = new planjs();
					$entityplanjs->setTache($entityNom);
					$entityplanjs->setLundi('Oui');
					$entityplanjs->setMardi('Oui');
					$entityplanjs->setMercredi('Oui');
					$entityplanjs->setJeudi('Oui');
					$entityplanjs->setVendredi('Oui');
					$entityplanjs->setSamedi('Oui');
					$entityplanjs->setTachesprogmag($entityTachesprogmag);
					$entityplanjs->setPlaningdate($entitypdate2);
					
					$em->persist($entityplanjs);
					$em->flush();
					
			
				}
			}
			

            return $this->redirect($this->generateUrl('tachesprogmag_plannew', array(
				'idMag' => $idMag,
				'agent1'=>$agent1,
				'agent2'=>$agent2,
				'agent3'=>$agent3,
				'agent4'=>$agent4,
				'agent5'=>$agent5,
				'agent6'=>$agent6,
				'agent7'=>$agent7,
				'agent8'=>$agent8,
				'agent9'=>$agent9,
				'agent10'=>$agent10,
				'responsmag1'=>$responsmag1,
				'responsmag2'=>$responsmag2,
				'lieux'=> $lieux,
				'site'=>$site,
				'id' => $entitypdate->getId())));
            
        }

        return $this->render('PhareosNomadeNetServiceBundle:planingdate:new.html.twig', array(
            'entitypdate' => $entitypdate,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing planingdate entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:planingdate')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find planingdate entity.');
        }

        $editForm = $this->createForm(new planingdateType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosNomadeNetServiceBundle:planingdate:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing planingdate entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:planingdate')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find planingdate entity.');
        }

        $editForm   = $this->createForm(new planingdateType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('planingdate_edit', array('id' => $id)));
        }

        return $this->render('PhareosNomadeNetServiceBundle:planingdate:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a planingdate entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosNomadeNetServiceBundle:planingdate')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find planingdate entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('planingdate'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
