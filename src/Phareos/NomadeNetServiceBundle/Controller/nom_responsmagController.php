<?php

namespace Phareos\NomadeNetServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Response;
use Phareos\NomadeNetServiceBundle\Entity\nom_responsmag;
use Phareos\NomadeNetServiceBundle\Entity\lieux;
use Phareos\NomadeNetServiceBundle\Form\nom_responsmagType;

/**
 * nom_responsmag controller.
 *
 */
class nom_responsmagController extends Controller
{
    /**
     * Lists all nom_responsmag entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag')->findAll();

        return $this->render('PhareosNomadeNetServiceBundle:nom_responsmag:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a nom_responsmag entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find nom_responsmag entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosNomadeNetServiceBundle:nom_responsmag:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new nom_responsmag entity.
     *
     */
    public function newAction()
    {
        //traitement du block twig en amont (menu de l'application)
		$session = $this->get('session');
		$request = $this->get('request');
		$idMag = $request->query->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		$em = $this->getDoctrine()->getEntityManager();
		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		$lieux = $repository_lieux->find($idMag);
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		// fin du traitement
		
		$entity = new nom_responsmag();
        $form   = $this->createForm(new nom_responsmagType(), $entity);

        return $this->render('PhareosNomadeNetServiceBundle:nom_responsmag:new.html.twig', array(
            'idMag' => $idMag,
			'lieux'=> $lieux,
			'site'=>$site,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new nom_responsmag entity.
     *
     */
    public function createAction()
    {
        $entity  = new nom_responsmag();
        $request = $this->getRequest();
        $form    = $this->createForm(new nom_responsmagType(), $entity);
        $form->bindRequest($request);
		
		//traitement du block twig en amont (menu de l'application)
		$session = $this->get('session');
		$idMag = $session->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		$em = $this->getDoctrine()->getEntityManager();
			$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
			$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
			$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
			$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
			
			$lieux = $repository_lieux->find($idMag);
		
			$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
			$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
			
			$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		
		// Fin du traitement
		

        if ($form->isValid()) {
            
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('PhareosNomadeNetServiceBundle_homemag', array('id' => $idMag)));
            
        }

        return $this->render('PhareosNomadeNetServiceBundle:nom_responsmag:new.html.twig', array(
            'idMag' => $idMag,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'lieux'=> $lieux,
			'site'=>$site,
			'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing nom_responsmag entity.
     *
     */
    public function editAction($id)
    {
        //traitement du block twig en amont (menu de l'application)
		$session = $this->get('session');
		$request = $this->get('request');
		$idMag = $session->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		$lieux = $repository_lieux->find($idMag);
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		// fin du traitement

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find nom_responsmag entity.');
        }

        $editForm = $this->createForm(new nom_responsmagType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosNomadeNetServiceBundle:nom_responsmag:edit.html.twig', array(
            'idMag' => $idMag,
			'lieux'=> $lieux,
			'site'=>$site,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2,
			'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing nom_responsmag entity.
     *
     */
    public function updateAction($id)
    {
        //traitement du block twig en amont (menu de l'application)
		$session = $this->get('session');
		$request = $this->get('request');
		$idMag = $session->get('idMag');
		
		$em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find nom_responsmag entity.');
        }

        $editForm   = $this->createForm(new nom_responsmagType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('PhareosNomadeNetServiceBundle_homemag', array('id' => $idMag)));
        }

        return $this->render('PhareosNomadeNetServiceBundle:nom_responsmag:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a nom_responsmag entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find nom_responsmag entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('nom_responsmag'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
	public function newaffectationAction()
    {
			
		// On récupère l'EntityManager.
		$em = $this->getDoctrine()->getEntityManager();
		
		//traitement du block twig en amont (menu de l'application)
		$session = $this->get('session');
		$idMag = $session->get('idMag');
		$idAgent1 = $session->get('idAgent1');
		$idAgent2 = $session->get('idAgent2');
		$idAgent3 = $session->get('idAgent3');
		$idAgent4 = $session->get('idAgent4');
		$idAgent5 = $session->get('idAgent5');
		$idAgent6 = $session->get('idAgent6');
		$idAgent7 = $session->get('idAgent7');
		$idAgent8 = $session->get('idAgent8');
		$idAgent9 = $session->get('idAgent9');
		$idAgent10 = $session->get('idAgent10');
		$idResponsMag1 = $session->get('idResponsMag1');
		$idResponsMag2 = $session->get('idResponsMag2');
		

		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		$repository_site = $em->getRepository('PhareosNomadeNetServiceBundle:site');
		$repository_agent = $em->getRepository('PhareosNomadeNetServiceBundle:agent');
		$repository_responsmag = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag');
		
		$lieux = $repository_lieux->find($idMag);
		
		$siteMatriculeLieux = $lieux->getMATRICULELIEUX();
		$site = $repository_site->findOneBy(array ('MATRICULE_LIEUX' => $siteMatriculeLieux));
		
		$idAgent1 = $lieux->getAgent1Id();
		if(empty($idAgent1))
		{
			$agent1 = $repository_agent->find(1);
		}
		else
		{
			$agent1 = $repository_agent->find($idAgent1);
		}
		$idAgent2 = $lieux->getAgent2Id();
		if(empty($idAgent2))
		{
			$agent2 = $repository_agent->find(1);
		}
		else
		{
			$agent2 = $repository_agent->find($idAgent2);
		}
		$idAgent3 = $lieux->getAgent3Id();
		if(empty($idAgent3))
		{
			$agent3 = $repository_agent->find(1);
		}
		else
		{
			$agent3 = $repository_agent->find($idAgent3);
		}
		$idAgent4 = $lieux->getAgent4Id();
		if(empty($idAgent4))
		{
			$agent4 = $repository_agent->find(1);
		}
		else
		{
			$agent4 = $repository_agent->find($idAgent4);
		}
		$idAgent5 = $lieux->getAgent5Id();
		if(empty($idAgent5))
		{
			$agent5 = $repository_agent->find(1);
		}
		else
		{
			$agent5 = $repository_agent->find($idAgent5);
		}
		$idAgent6 = $lieux->getAgent6Id();
		if(empty($idAgent6))
		{
			$agent6 = $repository_agent->find(1);
		}
		else
		{
			$agent6 = $repository_agent->find($idAgent6);
		}
		$idAgent7 = $lieux->getAgent7Id();
		if(empty($idAgent7))
		{
			$agent7 = $repository_agent->find(1);
		}
		else
		{
			$agent7 = $repository_agent->find($idAgent7);
		}
		$idAgent8 = $lieux->getAgent8Id();
		if(empty($idAgent8))
		{
			$agent8 = $repository_agent->find(1);
		}
		else
		{
			$agent8 = $repository_agent->find($idAgent8);
		}
		$idAgent9 = $lieux->getAgent9Id();
		if(empty($idAgent9))
		{
			$agent9 = $repository_agent->find(1);
		}
		else
		{
			$agent9 = $repository_agent->find($idAgent9);
		}
		$idAgent10 = $lieux->getAgent10Id();
		if(empty($idAgent10))
		{
			$agent10 = $repository_agent->find(1);
		}
		else
		{
			$agent10 = $repository_agent->find($idAgent10);
		}
		
		$idResponsMag1 = $lieux->getResponsmag1Id();
		if(empty($idResponsMag1))
		{
			$responsmag1 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag1 = $repository_responsmag->find($idResponsMag1);
		}
		
		$idResponsMag2 = $lieux->getResponsmag2Id();
		if(empty($idResponsMag2))
		{
			$responsmag2 = $repository_responsmag->find(1);
		}
		else
		{
			$responsmag2 = $repository_responsmag->find($idResponsMag2);
		}
		
		
		
		//on recup les entites responsmag
        $entities = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag')->findAll();

        return $this->render('PhareosNomadeNetServiceBundle:nom_responsmag:affectation.index.html.twig', array(
            'idMag' => $idMag,
			'lieux'=> $lieux,
			'site'=>$site,
			'entities' => $entities,
			'agent1'=>$agent1,
			'agent2'=>$agent2,
			'agent3'=>$agent3,
			'agent4'=>$agent4,
			'agent5'=>$agent5,
			'agent6'=>$agent6,
			'agent7'=>$agent7,
			'agent8'=>$agent8,
			'agent9'=>$agent9,
			'agent10'=>$agent10,
			'responsmag1'=>$responsmag1,
			'responsmag2'=>$responsmag2
        ));
	
	}
	
	public function createaffectationAction()
    {
		
		$session = $this->get('session');
		$request = $this->get('request');
		$idMag = $session->get('idMag');
		$idResponsmag = $request->query->get('idResponsmag');
		
		
		
		

		// On récupère l'EntityManager.
		$em = $this->getDoctrine()->getEntityManager();
		
		// On récupère le mag selectionner par son id.
		$entity = $em->getRepository('PhareosNomadeNetServiceBundle:lieux')->find($idMag);
		
		// On test si le mag existe .... normalement Yes !!! ;)
		if (!$entity) {
            throw $this->createNotFoundException('Unable to find lieux entity.');
        }
		$IdResponsmagPresence1 = $entity->getResponsmag1Id();
		$IdResponsmagPresence2 = $entity->getResponsmag2Id();
		
		
		// on récup le responsable sélectionner !!
		$responsmags = $em->getRepository('PhareosNomadeNetServiceBundle:nom_responsmag')->find($idResponsmag);

		
		
		if(empty($IdResponsmagPresence1) or $IdResponsmagPresence1 == 1)
		{
			//On applique la modif sur le champ!!! lol le champ responsmag1Id
			$entity->setResponsmag1Id($idResponsmag);
		}
		
		else if(empty($IdResponsmagPresence2) or $IdResponsmagPresence2 == 1 )
		{
			$entity->setResponsmag2Id($idResponsmag);
		}
		else
		{
			throw $this->createNotFoundException('Impossible de créer un nouvelle agent vous devez en supprimer un.');
		}
		
		
		
		

		

		// On persiste juste le responsable1id du Mag
		
		$em->persist($entity);

		// On déclenche l'enregistrement.
		$em->flush();

		//return new Response('OK');
		return $this->redirect($this->generateUrl('PhareosNomadeNetServiceBundle_homemag', array('id' => $idMag,)));
	
	}
	
	public function replaceconfirmaffectationAction()
    {
		$session = $this->get('session');
		$request = $this->get('request');//NumidAgent
		$idMag = $session->get('idMag');
		$NumidResp = $request->query->get('NumidResp');
		
		$em = $this->getDoctrine()->getEntityManager();
		$entity = $em->getRepository('PhareosNomadeNetServiceBundle:lieux')->find($idMag);
		
		if ($NumidResp == 1)
		{
			$entity->setResponsmag1Id(1);
		}	
		else
		{
			$entity->setResponsmag2Id(1);
		}
		
		$em->persist($entity);
		
		$em->flush();
		
		return $this->redirect($this->generateUrl('nom_responsmag_newaffectation', array('id' => $idMag,)));
		
	}
	
}
