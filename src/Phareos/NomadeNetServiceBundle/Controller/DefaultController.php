<?php

namespace Phareos\NomadeNetServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Phareos\NomadeNetServiceBundle\Entity\ville;
use Phareos\NomadeNetServiceBundle\Entity\lieux;
use Phareos\NomadeNetServiceBundle\Entity\site;

class DefaultController extends Controller
{
    
    public function indexAction()
    {
        $id = 1560; // ID de la table lieux
		
		//$ville = new ville;
		//$ville->setCODEVILLE('10001');
		//$ville->setVILLE('LE CHAMBON FEUGEROLLES');
		//$ville->setCPVILLE('42500');
		//$ville->setLATITUDE('25');
		//$ville->setLONGITUDE('32');
		//$ville->setELOIGNEMENT('50');
		
		$request = $this->get('request');
		
		$session = $this->get('session');
		
		$societeUSER = $session->get('societeUSER');
		
		
		
		$em = $this->getDoctrine()->getEntityManager();
		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		
		//$repository_ville = $em->getRepository('PhareosNomadeNetServiceBundle:ville');

		$repository_grand_compte = $em->getRepository('PhareosNomadeNetServiceBundle:grand_compte');
		
		
		
		
		//$lieuxVille = $repository_lieux->getLieuxAvecVille();
		//$ville = $repository_ville->findAll();
		//$lieux->getVille();
		//$ville = $this->$lieux->getVille();
		//$em->persist($ville);
		//$em->flush();
		
		//$lieuxSite = $repository_lieux->getLieuxAvecSite();
		//$grand_compteLieux = $repository_lieux->getLieuxAvecGC();
		//$id_GCPL = $grand_compteLieux->findAll();
		//$grand_comptePourUnLieux = $repository_grand_compte->findID($id_GCPL);
		//$NomGCompte = $repository_grand_compte->findOneBy(array ('MATRICULE_LIEUX' => $grand_compteLieux));;
		$ListeGC = $repository_grand_compte->findBy(array('societeuser' => $societeUSER), array('NOM_GC' => 'asc'));
		//$ListeGC = $repository_grand_compte->findAll();
		
		foreach($ListeGC as $grand_compte)
			{
				// $article est une instance de Article
				$idLienGCLieux = $grand_compte->getid();
			}
		
		return $this->render('PhareosNomadeNetServiceBundle::default_layout.html.twig', array(
			'listeGC'=>$ListeGC
			));
    }
	
	
	
	public function magselectAction($idMagSelect)
    {
        $id = 1560; // ID de la table lieux
		
		//$ville = new ville;
		//$ville->setCODEVILLE('10001');
		//$ville->setVILLE('LE CHAMBON FEUGEROLLES');
		//$ville->setCPVILLE('42500');
		//$ville->setLATITUDE('25');
		//$ville->setLONGITUDE('32');
		//$ville->setELOIGNEMENT('50');
		
		$session = $this->get('session');
		
		$nomUSER = $session->get('nomUSER');
		
		$em = $this->getDoctrine()->getEntityManager();
		$repository_lieux = $em->getRepository('PhareosNomadeNetServiceBundle:lieux');
		//$repository_ville = $em->getRepository('PhareosNomadeNetServiceBundle:ville');
		//$lieuxSelect = $repository_lieux->findBy(array('MATRICULE_GC' => $idMagSelect));

		$repository_grand_compte = $em->getRepository('PhareosNomadeNetServiceBundle:grand_compte');
		
		$nomGC = $repository_grand_compte->find($idMagSelect);
		$nomGC2 = $nomGC->getNOMGC();
		$nomID = $nomGC->getId();
		
		$session->set('nomGC', $nomGC2);
		$session->set('idGC', $nomID);
		
		$lieuxVillespec = $repository_lieux->findBy(array('lieux' => $idMagSelect, 'respsect' => $nomUSER),
													array('NOM_VILLE' => 'asc'));
		$lieuxVille = $repository_lieux->findBy(array('lieux' => $idMagSelect),
													array('NOM_VILLE' => 'asc'));
		//$ville = $repository_ville->findAll();
		//$lieux->getVille();
		//$ville = $this->$lieux->getVille();
		//$em->persist($ville);
		//$em->flush();
		
		//$lieuxSite = $repository_lieux->getLieuxAvecSite();
		//$grand_compteLieux = $repository_lieux->getLieuxAvecGC();
		//$id_GCPL = $grand_compteLieux->findAll();
		//$grand_comptePourUnLieux = $repository_grand_compte->findID($id_GCPL);
		//$NomGCompte = $repository_grand_compte->findOneBy(array ('MATRICULE_LIEUX' => $grand_compteLieux));;
		//$ListeGC = $repository_grand_compte->findAll();
		
		//foreach($ListeGC as $grand_compte)
			//{
				// $article est une instance de Article
				//$idLienGCLieux = $grand_compte->getid();
			//}
		
		return $this->render('PhareosNomadeNetServiceBundle::default_layout_magselectpage.html.twig', array(
			'lieuxville'=>$lieuxVille,
			'lieuxvillespec'=>$lieuxVillespec
			));
    }
}
