<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class prodmagType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('seuilmag')
            ->add('seuilveh')
			->add('typemat', 'choice', array('choices' => array('Materiels' => "Materiels", 'Consommables' => "Consommables", 'Produits' => "Produits"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => true
                                            ))
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_prodmagtype';
    }
}
