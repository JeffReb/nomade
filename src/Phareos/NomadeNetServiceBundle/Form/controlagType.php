<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class controlagType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('moi')
            ->add('annee')
            ->add('signe')
            ->add('rdvous')
            ->add('respsect')
            ->add('active')
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_controlagtype';
    }
}
