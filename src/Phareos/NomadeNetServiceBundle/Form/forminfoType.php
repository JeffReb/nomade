<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class forminfoType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('prenom')
            ->add('adressse1')
            ->add('adresse2')
            ->add('codepostal')
            ->add('ville')
            ->add('nationa')
            ->add('ressortce')
            ->add('carteident')
            ->add('delivrpar')
            ->add('titresejour')
            ->add('datevalide')
            ->add('autotravil', 'choice', array('choices' => array(1 => "Oui", 0 => "Non"), 
                                            'multiple' => false, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('delivattrav')
            ->add('notrepresent')
            ->add('notepres')
            ->add('noteeloc')
            ->add('notedynam')
            ->add('noteanaly')
            ->add('noteorga')
            ->add('noteexp')
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_forminfotype';
    }
}
