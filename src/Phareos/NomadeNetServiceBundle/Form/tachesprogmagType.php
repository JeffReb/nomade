<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

use Phareos\NomadeNetServiceBundle\Entity\fonction;
use Phareos\NomadeNetServiceBundle\Entity\fonctionRepository;

use Phareos\NomadeNetServiceBundle\Entity\frequence;
use Phareos\NomadeNetServiceBundle\Entity\frequenceRepository;

class tachesprogmagType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $idGC = $_SESSION['idgc'];
		
		$builder
            ->add('tache')
            ->add('duree')
			->add('frequence', 'entity', array('class' => 'PhareosNomadeNetServiceBundle:frequence',
												'property' => 'nom',
												'query_builder' => function(frequenceRepository $er) {
												return $er->createQueryBuilder ('u')
												->where ('u.grdcompte_id = :idGC')
												->setParameter('idGC', $_SESSION['idgc'])
												->orderBY ('u.nom', 'ASC');
												},
												'multiple' => false,
												'expanded' => false
												))
			->add('fonction', 'entity', array('class' => 'PhareosNomadeNetServiceBundle:fonction',
												'property' => 'nom',
												'query_builder' => function(fonctionRepository $er) {
												return $er->createQueryBuilder ('u')
												->where ('u.grdcompte_id = :idGC')
												->setParameter('idGC', $_SESSION['idgc'])
												->orderBY ('u.nom', 'ASC');
												},
												'multiple' => false,
												'expanded' => false
												))
            ->add('qte')
			->add('marqtype')
			//->add('audtech', 'entity' , array('class'    => 'PhareosNomadeNetServiceBundle:audtech' ,
												//'property' => 'date_maj' ,
												//'expanded' => true ,
												//'multiple' => true , 
												//))
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_tachesprogmagtype';
    }
}
