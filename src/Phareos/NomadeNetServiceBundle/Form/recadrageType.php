<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

use Phareos\NomadeNetServiceBundle\Entity\agent;
use Phareos\NomadeNetServiceBundle\Entity\agentRepository;

class recadrageType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            //->add('object', 'checkbox', array("label" => "Objectif à atteindre ?", "required" => false, "value" => "ValeurCheckbox"))
            //->add('objatt', 'checkbox', array("label" => "Objectif atteint ?", "required" => false, "value" => "ValeurCheckbox"))
            ->add('objectcom')
            //->add('daterecad')
            //->add('dateobj')
			->add('agent', 'entity', array('class' => 'PhareosNomadeNetServiceBundle:agent',
												'property' => 'NOM_PERS',
												'query_builder' => function(agentRepository $er) {
												return $er->createQueryBuilder ('u')
												->orderBY ('u.NOM_PERS', 'ASC');
												},
												))
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_recadragetype';
    }
}
