<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

use Phareos\NomadeNetServiceBundle\Entity\prodmag;
use Phareos\NomadeNetServiceBundle\Entity\prodmagRepository;

class audtechType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $idLIEUX = $_SESSION['idLIEUX'];
		
		$builder
            ->add('trait_prio_com')
			->add('trait_prio', 'checkbox', array("label" => "Publier l'actu ?", "required" => false, "value" => "ValeurCheckbox"))
            //->add('poin_mag', 'checkbox', array("label" => "Publier l'actu ? poin_mag", "required" => false, "value" => "ValeurCheckbox"))
            //->add('reapro', 'checkbox', array("label" => "Publier l'actu ? reapro", "required" => false, "value" => "ValeurCheckbox"))
			->add('matenpanne', 'checkbox', array("label" => "Publier l'actu ?", "required" => false, "value" => "ValeurCheckbox"))
			//->add('bonvit', 'checkbox', array("label" => "Publier l'actu ? bonvit", "required" => false, "value" => "ValeurCheckbox"))
			//->add('bonvitval', 'checkbox', array("label" => "Publier l'actu ? bonvitval", "required" => false, "value" => "ValeurCheckbox"))
            //->add('form_po_agen', 'checkbox', array("label" => "Publier l'actu ? form_po_agen", "required" => false, "value" => "ValeurCheckbox"))
			->add('carnetbord', 'checkbox', array("label" => "Publier l'actu ?", "required" => false, "value" => "ValeurCheckbox"))
			->add('prodmag', 'entity', array('class' => 'PhareosNomadeNetServiceBundle:prodmag',
												'property' => 'nom',
												'query_builder' => function(prodmagRepository $er) {
												return $er->createQueryBuilder ('u')
												->where ('u.lieuxid2 = :idLIEUX')
												->setParameter('idLIEUX', $_SESSION['idLIEUX'])
												->andWhere ('u.typemat = :typeMat')
												->setParameter('typeMat', 'Materiels')
												->andWhere ('u.archive = :archive')
												->setParameter('archive', 0)
												->orderBY ('u.nom', 'ASC');
												},
												'multiple' => false,
												'expanded' => false
												))
			->add('filebv1')
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_audtechtype';
    }
}
