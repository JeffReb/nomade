<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class reaproType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            //->add('site')
            //->add('nomrespsect')
            ->add('datedemande')
            ->add('prod01', 'choice', array('choices' => array('Produits sol et poussières' => "Produits sol et poussières", 'Désinfectant wc' => "Désinfectant wc", 'Détartrant' => "Détartrant", 'Economia R50' => "Economia R50", 'Booster' => "Booster", 'Produit auto laveuse' => "Produit auto laveuse", 'Chiffons microfibre' => "Chiffons microfibre", 'Franges microfibre' => "Franges microfibre", 'Chiffons rose sanitaire' => "Chiffons rose sanitaire", 'Frange traditionnel' => "Frange traditionnel", 'Eponge' => "Eponge"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('prod01qte')
            ->add('prod02', 'choice', array('choices' => array('Produits sol et poussières' => "Produits sol et poussières", 'Désinfectant wc' => "Désinfectant wc", 'Détartrant' => "Détartrant", 'Economia R50' => "Economia R50", 'Booster' => "Booster", 'Produit auto laveuse' => "Produit auto laveuse", 'Chiffons microfibre' => "Chiffons microfibre", 'Franges microfibre' => "Franges microfibre", 'Chiffons rose sanitaire' => "Chiffons rose sanitaire", 'Frange traditionnel' => "Frange traditionnel", 'Eponge' => "Eponge"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('prod02qte')
            ->add('prod03', 'choice', array('choices' => array('Produits sol et poussières' => "Produits sol et poussières", 'Désinfectant wc' => "Désinfectant wc", 'Détartrant' => "Détartrant", 'Economia R50' => "Economia R50", 'Booster' => "Booster", 'Produit auto laveuse' => "Produit auto laveuse", 'Chiffons microfibre' => "Chiffons microfibre", 'Franges microfibre' => "Franges microfibre", 'Chiffons rose sanitaire' => "Chiffons rose sanitaire", 'Frange traditionnel' => "Frange traditionnel", 'Eponge' => "Eponge"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('prod03qte')
            ->add('prod04', 'choice', array('choices' => array('Produits sol et poussières' => "Produits sol et poussières", 'Désinfectant wc' => "Désinfectant wc", 'Détartrant' => "Détartrant", 'Economia R50' => "Economia R50", 'Booster' => "Booster", 'Produit auto laveuse' => "Produit auto laveuse", 'Chiffons microfibre' => "Chiffons microfibre", 'Franges microfibre' => "Franges microfibre", 'Chiffons rose sanitaire' => "Chiffons rose sanitaire", 'Frange traditionnel' => "Frange traditionnel", 'Eponge' => "Eponge"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('prod04qte')
            ->add('prod05', 'choice', array('choices' => array('Produits sol et poussières' => "Produits sol et poussières", 'Désinfectant wc' => "Désinfectant wc", 'Détartrant' => "Détartrant", 'Economia R50' => "Economia R50", 'Booster' => "Booster", 'Produit auto laveuse' => "Produit auto laveuse", 'Chiffons microfibre' => "Chiffons microfibre", 'Franges microfibre' => "Franges microfibre", 'Chiffons rose sanitaire' => "Chiffons rose sanitaire", 'Frange traditionnel' => "Frange traditionnel", 'Eponge' => "Eponge"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('prod05qte')
            ->add('mat01', 'choice', array('choices' => array('Aspirateur' => "Aspirateur", 'Bac presse' => "Bac presse", 'Presse seul' => "Presse seul", 'Balai à frange' => "Balai à frange", 'Manche balai à frange seul' => "Manche balai à frange seul", 'Perche télescopique 2 mètres' => "Perche télescopique 2 mètres", 'Perche télescopique 4 mètres' => "Perche télescopique 4 mètres", 'Grattoir sol' => "Grattoir sol", 'Grattoir vitre' => "Grattoir vitre", 'Sceau blanc poussière' => "Sceau blanc poussière"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mat01qte')
            ->add('mat02', 'choice', array('choices' => array('Aspirateur' => "Aspirateur", 'Bac presse' => "Bac presse", 'Presse seul' => "Presse seul", 'Balai à frange' => "Balai à frange", 'Manche balai à frange seul' => "Manche balai à frange seul", 'Perche télescopique 2 mètres' => "Perche télescopique 2 mètres", 'Perche télescopique 4 mètres' => "Perche télescopique 4 mètres", 'Grattoir sol' => "Grattoir sol", 'Grattoir vitre' => "Grattoir vitre", 'Sceau blanc poussière' => "Sceau blanc poussière"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mat02qte')
            ->add('mat03', 'choice', array('choices' => array('Aspirateur' => "Aspirateur", 'Bac presse' => "Bac presse", 'Presse seul' => "Presse seul", 'Balai à frange' => "Balai à frange", 'Manche balai à frange seul' => "Manche balai à frange seul", 'Perche télescopique 2 mètres' => "Perche télescopique 2 mètres", 'Perche télescopique 4 mètres' => "Perche télescopique 4 mètres", 'Grattoir sol' => "Grattoir sol", 'Grattoir vitre' => "Grattoir vitre", 'Sceau blanc poussière' => "Sceau blanc poussière"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mat03qte')
            ->add('mat04', 'choice', array('choices' => array('Aspirateur' => "Aspirateur", 'Bac presse' => "Bac presse", 'Presse seul' => "Presse seul", 'Balai à frange' => "Balai à frange", 'Manche balai à frange seul' => "Manche balai à frange seul", 'Perche télescopique 2 mètres' => "Perche télescopique 2 mètres", 'Perche télescopique 4 mètres' => "Perche télescopique 4 mètres", 'Grattoir sol' => "Grattoir sol", 'Grattoir vitre' => "Grattoir vitre", 'Sceau blanc poussière' => "Sceau blanc poussière"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mat04qte')
            ->add('mat05', 'choice', array('choices' => array('Aspirateur' => "Aspirateur", 'Bac presse' => "Bac presse", 'Presse seul' => "Presse seul", 'Balai à frange' => "Balai à frange", 'Manche balai à frange seul' => "Manche balai à frange seul", 'Perche télescopique 2 mètres' => "Perche télescopique 2 mètres", 'Perche télescopique 4 mètres' => "Perche télescopique 4 mètres", 'Grattoir sol' => "Grattoir sol", 'Grattoir vitre' => "Grattoir vitre", 'Sceau blanc poussière' => "Sceau blanc poussière"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('mat05qte')
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_reaprotype';
    }
}
