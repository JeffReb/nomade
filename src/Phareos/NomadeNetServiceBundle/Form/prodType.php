<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class prodType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('seuilmag')
            ->add('seuilveh')
            ->add('typemat', 'choice', array('choices' => array('Materiels' => "Materiels", 'Consommables' => "Consommables", 'Produits' => "Produits"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => true
                                            ))
			->add('affectglobal', 'checkbox', array("label" => "Publier l'actu ?", "required" => false, "value" => "ValeurCheckbox"))
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_prodtype';
    }
}
