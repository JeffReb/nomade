<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

use Phareos\NomadeNetServiceBundle\Entity\planingdate;
use Phareos\NomadeNetServiceBundle\Entity\planingdateRepository;

use Phareos\NomadeNetServiceBundle\Entity\tachesprogmag;
use Phareos\NomadeNetServiceBundle\Entity\tachesprogmagRepository;

class planjsType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $idMag = $_SESSION['idMag'];
		
		$builder
            ->add('tljs', 'choice', array('choices' => array('Oui' => " "), 
                                            'multiple' => false, 
                                            'expanded' => true,
											'required' => false,
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => -1
                                            ))
            ->add('lundi', 'choice', array('choices' => array('Oui' => " "), 
                                            'multiple' => false, 
                                            'expanded' => true,
											'required' => false,
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => -1
                                            ))
            ->add('mardi', 'choice', array('choices' => array('Oui' => " "), 
                                            'multiple' => false, 
                                            'expanded' => true,
											'required' => false,
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => -1
                                            ))
            ->add('mercredi', 'choice', array('choices' => array('Oui' => " "), 
                                            'multiple' => false, 
                                            'expanded' => true,
											'required' => false,
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => -1
                                            ))
            ->add('jeudi', 'choice', array('choices' => array('Oui' => " "), 
                                            'multiple' => false, 
                                            'expanded' => true,
											'required' => false,
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => -1
                                            ))
            ->add('vendredi', 'choice', array('choices' => array('Oui' => " "), 
                                            'multiple' => false, 
                                            'expanded' => true,
											'required' => false,
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => -1
                                            ))
            ->add('samedi', 'choice', array('choices' => array('Oui' => " "), 
                                            'multiple' => false, 
                                            'expanded' => true,
											'required' => false,
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => -1
                                            ))
			->add('planingdate', 'entity', array('class' => 'PhareosNomadeNetServiceBundle:planingdate',
												'property' => 'semaine',
												'query_builder' => function(planingdateRepository $er) {
												return $er->createQueryBuilder ('u')
												->where ('u.lieuxid2 = :idMag')
												->setParameter('idMag', $_SESSION['idMag'])
												->orderBY ('u.semaine', 'ASC');
												},
												'multiple' => false,
												'expanded' => false
												))
			->add('tachesprogmag', 'entity', array('class' => 'PhareosNomadeNetServiceBundle:tachesprogmag',
												'property' => 'tache',
												'query_builder' => function(tachesprogmagRepository $er) {
												return $er->createQueryBuilder ('u')
												->where ('u.lieuxid2 = :idMag')
												->setParameter('idMag', $_SESSION['idMag'])
												->andWhere ('u.archive = :archive')
												->setParameter('archive', 0)
												->orderBY ('u.tache', 'ASC');
												},
												'multiple' => false,
												'expanded' => false
												))
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_planjstype';
    }
}
