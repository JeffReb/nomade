<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class audtprogType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('propre')
			->add('traces')
			->add('poussiere')
			->add('tache')
			->add('fatuse')
			->add('com')
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_audtprogtype';
    }
}
