<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class testarrayType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('arr1', 'choice', array('choices' => array('Propre' => "Propre", 'Traces' => "Traces", 'Poussières' => "Poussières", 'Tâches' => "Tâches", 'Usé' => "Usé" ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_testarraytype';
    }
}
