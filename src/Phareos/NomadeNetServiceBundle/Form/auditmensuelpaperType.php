<?php

namespace Phareos\NomadeNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class auditmensuelpaperType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('fileimgpaper')
        ;
    }

    public function getName()
    {
        return 'phareos_nomadenetservicebundle_auditmensuelpapertype';
    }
}
