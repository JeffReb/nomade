<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\LogisToolBoxBundle\Entity\expeditioncom;
use Phareos\LogisToolBoxBundle\Form\expeditioncomType;

/**
 * expeditioncom controller.
 *
 */
class expeditioncomController extends Controller
{
    /**
     * Lists all expeditioncom entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:expeditioncom')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:expeditioncom:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a expeditioncom entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:expeditioncom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find expeditioncom entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:expeditioncom:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new expeditioncom entity.
     *
     */
    public function newAction()
    {
        $entity = new expeditioncom();
        $form   = $this->createForm(new expeditioncomType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:expeditioncom:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new expeditioncom entity.
     *
     */
    public function createAction()
    {
        $entity  = new expeditioncom();
        $request = $this->getRequest();
        $form    = $this->createForm(new expeditioncomType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('expeditioncom_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosLogisToolBoxBundle:expeditioncom:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing expeditioncom entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:expeditioncom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find expeditioncom entity.');
        }

        $editForm = $this->createForm(new expeditioncomType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:expeditioncom:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing expeditioncom entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:expeditioncom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find expeditioncom entity.');
        }

        $editForm   = $this->createForm(new expeditioncomType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('expeditioncom_edit', array('id' => $id)));
        }

        return $this->render('PhareosLogisToolBoxBundle:expeditioncom:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a expeditioncom entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosLogisToolBoxBundle:expeditioncom')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find expeditioncom entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('expeditioncom'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
