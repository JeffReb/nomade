<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\LogisToolBoxBundle\Entity\articlescom;
use Phareos\LogisToolBoxBundle\Form\articlescomType;

/**
 * articlescom controller.
 *
 */
class articlescomController extends Controller
{
    /**
     * Lists all articlescom entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:articlescom')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:articlescom:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a articlescom entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:articlescom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find articlescom entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:articlescom:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new articlescom entity.
     *
     */
    public function newAction()
    {
        $entity = new articlescom();
        $form   = $this->createForm(new articlescomType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:articlescom:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new articlescom entity.
     *
     */
    public function createAction()
    {
        $entity  = new articlescom();
        $request = $this->getRequest();
        $form    = $this->createForm(new articlescomType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('articlescom_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosLogisToolBoxBundle:articlescom:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing articlescom entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:articlescom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find articlescom entity.');
        }

        $editForm = $this->createForm(new articlescomType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:articlescom:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing articlescom entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:articlescom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find articlescom entity.');
        }

        $editForm   = $this->createForm(new articlescomType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('articlescom_edit', array('id' => $id)));
        }

        return $this->render('PhareosLogisToolBoxBundle:articlescom:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a articlescom entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosLogisToolBoxBundle:articlescom')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find articlescom entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('articlescom'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
