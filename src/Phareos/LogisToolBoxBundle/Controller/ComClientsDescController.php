<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Phareos\LogisToolBoxBundle\Entity\comclientscom;
use Phareos\LogisToolBoxBundle\Form\comclientscomType;

class ComClientsDescController extends Controller
{
    
    public function indexAction()
    {
		$em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:comclientscom')->findAll();
		
		$entity = new comclientscom();
        $form   = $this->createForm(new comclientscomType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:comclientscom:index.html.twig', array(
            'entities' => $entities,
			'entity' => $entity,
            'form'   => $form->createView()
        ));
		//return $this->render('PhareosLogisToolBoxBundle:Reapro:index.html.twig');
    }
	
	public function createAction()
    {
		$entity  = new comclientscom();
        $request = $this->getRequest();
        $form    = $this->createForm(new comclientscomType(), $entity);
        $form->bindRequest($request);
		//$date = new \DateTime('now');
		$auteur = $this->container->get('security.context')->getToken()->getUser();
		
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
			//$entity->setDate($date);
			$entity->setAuteur($auteur);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Comclientsdesc'));
            
        }

        return $this->render('Comclientsdesc', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }
}