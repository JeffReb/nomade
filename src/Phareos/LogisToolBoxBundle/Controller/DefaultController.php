<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Phareos\LogisToolBoxBundle\Entity\comclients;

class DefaultController extends Controller
{
    
    public function indexAction()
    {
		$user = $this->container->get('security.context')->getToken()->getUser();
		$username = $user->getusername();
		
		$session = $this->get('session');
		$societeUSER = $session->get('societeUSER');
		$applicationUSER = $session->get('applicationUSER');
		
		if ($applicationUSER == 'ToolboxClient'){
		
		$em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('client' => $societeUSER, 'archive' => 0), array('numop' => 'desc'));
		$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
		$entitiescomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->findBy(array('iduser' => $userid));
		if ($entitiescomarticlestmp) {

        
		foreach ($entitiescomarticlestmp as $articlestmp)
				{
					$idArticletmp = $articlestmp->getID();
					$entitycomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->find($idArticletmp);
			
					$em->remove($entitycomarticlestmp);
					$em->flush();
				}
			}
        return $this->render('PhareosLogisToolBoxBundle:comclients:index.html.twig', array(
            'entities' => $entities
        ));
		
		
		}
		elseif ($applicationUSER == 'ToolboxInterv')
		{
			$em = $this->getDoctrine()->getEntityManager();
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('install' => 1), array('numop' => 'desc'));
			return $this->render('PhareosLogisToolBoxBundle:comclients:index.html.twig', array(
            'entities' => $entities
        ));
		}
		
		else {
		$em = $this->getDoctrine()->getEntityManager();
		$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('archive' => 0));
		return $this->render('PhareosLogisToolBoxBundle:comclients:index.html.twig', array(
            'entities' => $entities
        ));
		//return $this->render('PhareosLogisToolBoxBundle:Default:index.html.twig');
		}
    }
}
