<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Phareos\LogisToolBoxBundle\Entity\prepacom;
use Phareos\LogisToolBoxBundle\Form\prepacomType;

class PreparationController extends Controller
{
    
    public function indexAction()
    {
		$em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:prepacom')->findAll();
		
		$entity = new prepacom();
        $form   = $this->createForm(new prepacomType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:Preparation:index.html.twig', array(
            'entities' => $entities,
			'entity' => $entity,
            'form'   => $form->createView()
        ));
		//return $this->render('PhareosLogisToolBoxBundle:Preparation:index.html.twig');
    }
	
	public function createAction()
    {
		$entity  = new prepacom();
        $request = $this->getRequest();
        $form    = $this->createForm(new prepacomType(), $entity);
        $form->bindRequest($request);
		//$date = new \DateTime('now');
		$auteur = $this->container->get('security.context')->getToken()->getUser();
		
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
			//$entity->setDate($date);
			$entity->setAuteur($auteur);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Preparation'));
            
        }

        return $this->render('Preparation', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }
}