<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Phareos\LogisToolBoxBundle\Entity\recepcom;
use Phareos\LogisToolBoxBundle\Form\recepcomType;

class ReceptionController extends Controller
{
    
    public function indexAction()
    {
		$em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:recepcom')->findAll();
		
		$entity = new recepcom();
        $form   = $this->createForm(new recepcomType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:Reception:index.html.twig', array(
            'entities' => $entities,
			'entity' => $entity,
            'form'   => $form->createView()
        ));
		//return $this->render('PhareosLogisToolBoxBundle:Reception:index.html.twig');
    }
	
	public function createAction()
    {
		$entity  = new recepcom();
        $request = $this->getRequest();
        $form    = $this->createForm(new recepcomType(), $entity);
        $form->bindRequest($request);
		//$date = new \DateTime('now');
		$auteur = $this->container->get('security.context')->getToken()->getUser();
		
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
			//$entity->setDate($date);
			$entity->setAuteur($auteur);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Reception'));
            
        }

        return $this->render('Reception', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }
}