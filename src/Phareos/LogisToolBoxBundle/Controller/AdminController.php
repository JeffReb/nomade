<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Phareos\LogisToolBoxBundle\Entity\admincom;
use Phareos\LogisToolBoxBundle\Form\admincomType;

class AdminController extends Controller
{
    
    public function indexAction()
    {
		$em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:admincom')->findAll();
		
		$entity = new admincom();
        $form   = $this->createForm(new admincomType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:Admin:index.html.twig', array(
            'entities' => $entities,
			'entity' => $entity,
            'form'   => $form->createView()
        ));
		
		
		//return $this->render('PhareosLogisToolBoxBundle:Admin:index.html.twig');
    }
	
	public function createAction()
    {
		$entity  = new admincom();
        $request = $this->getRequest();
        $form    = $this->createForm(new admincomType(), $entity);
        $form->bindRequest($request);
		//$date = new \DateTime('now');
		$auteur = $this->container->get('security.context')->getToken()->getUser();
		
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
			//$entity->setDate($date);
			$entity->setAuteur($auteur);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Admin'));
            
        }

        return $this->render('Admin', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }
}