<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\LogisToolBoxBundle\Entity\stockcom;
use Phareos\LogisToolBoxBundle\Form\stockcomType;

/**
 * stockcom controller.
 *
 */
class stockcomController extends Controller
{
    /**
     * Lists all stockcom entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:stockcom')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:stockcom:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a stockcom entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:stockcom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find stockcom entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:stockcom:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new stockcom entity.
     *
     */
    public function newAction()
    {
        $entity = new stockcom();
        $form   = $this->createForm(new stockcomType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:stockcom:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new stockcom entity.
     *
     */
    public function createAction()
    {
        $entity  = new stockcom();
        $request = $this->getRequest();
        $form    = $this->createForm(new stockcomType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('stockcom_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosLogisToolBoxBundle:stockcom:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing stockcom entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:stockcom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find stockcom entity.');
        }

        $editForm = $this->createForm(new stockcomType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:stockcom:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing stockcom entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:stockcom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find stockcom entity.');
        }

        $editForm   = $this->createForm(new stockcomType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('stockcom_edit', array('id' => $id)));
        }

        return $this->render('PhareosLogisToolBoxBundle:stockcom:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a stockcom entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosLogisToolBoxBundle:stockcom')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find stockcom entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('stockcom'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
