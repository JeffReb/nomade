<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Phareos\LogisToolBoxBundle\Entity\commanderecip;
use Phareos\LogisToolBoxBundle\Form\commanderecipType;
use Phareos\LogisToolBoxBundle\Form\commanderecipAdminType;
use Phareos\LogisToolBoxBundle\Entity\com_recip;
//use Phareos\LogisToolBoxBundle\Entity\destination;
use Phareos\LogisToolBoxBundle\Entity\articles;
//use Phareos\LogisToolBoxBundle\Form\comclientsAdminType;

use Phareos\LogisToolBoxBundle\Entity\ordrecstock;

/**
 * comclients controller.
 *
 */
class commanderecipController extends Controller
{
    /**
     * Lists all comclients entities.
     *
     */
    public function indexAction()
    {
        
		$session = $this->get('session');
		$societeUSER = $session->get('societeUSER');
		$applicationUSER = $session->get('applicationUSER');
		
		$em = $this->getDoctrine()->getEntityManager();

        if ($applicationUSER == 'Toolbox')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:commanderecip')->findBy(array('archive' => 0));
			$entitiesarticlescom = $em->getRepository('PhareosLogisToolBoxBundle:com_recip')->findAll();
			$articles = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findAll();
		}
		else
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:commanderecip')->findBy(array('client' => $societeUSER, 'archive' => 0), array('numor' => 'desc'));
			$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
			$entitiescomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->findBy(array('iduser' => $userid));
			$entitiesarticlescom = $em->getRepository('PhareosLogisToolBoxBundle:com_recip')->findAll();
			$articles = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findAll();
			if ($entitiescomarticlestmp) {

        
			foreach ($entitiescomarticlestmp as $articlestmp)
				{
					$idArticletmp = $articlestmp->getID();
					$entitycomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->find($idArticletmp);
			
					$em->remove($entitycomarticlestmp);
					$em->flush();
				}
			}
		}
		
		//$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:commanderecip:index.html.twig', array(
            'entities' => $entities,
			'entitiesarticlescom' => $entitiesarticlescom,
			'articles' => $articles
        ));
    }
	
	public function indexpartielAction()
    {
        
		$session = $this->get('session');
		$societeUSER = $session->get('societeUSER');
		$applicationUSER = $session->get('applicationUSER');
		
		$em = $this->getDoctrine()->getEntityManager();

        if ($applicationUSER == 'Toolbox')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:commanderecip')->findBy(array('etat' => 'Réception Partielle', 'archive' => 0));
			$entitiesarticlescom = $em->getRepository('PhareosLogisToolBoxBundle:com_recip')->findAll();
			$articles = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findAll();
		}
		else
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:commanderecip')->findBy(array('etat' => 'Réception Partielle', 'client' => $societeUSER, 'archive' => 0, ), array('numor' => 'desc'));
			$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
			$entitiescomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->findBy(array('iduser' => $userid));
			$entitiesarticlescom = $em->getRepository('PhareosLogisToolBoxBundle:com_recip')->findAll();
			$articles = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findAll();
			if ($entitiescomarticlestmp) {

        
			foreach ($entitiescomarticlestmp as $articlestmp)
				{
					$idArticletmp = $articlestmp->getID();
					$entitycomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->find($idArticletmp);
			
					$em->remove($entitycomarticlestmp);
					$em->flush();
				}
			}
		}
		
		//$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:commanderecip:index.html.twig', array(
            'entities' => $entities,
			'entitiesarticlescom' => $entitiesarticlescom,
			'articles' => $articles
        ));
    }
	
	public function indexrecepAction()
    {
        
		$session = $this->get('session');
		$societeUSER = $session->get('societeUSER');
		$applicationUSER = $session->get('applicationUSER');
		
		$em = $this->getDoctrine()->getEntityManager();

        if ($applicationUSER == 'Toolbox')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:commanderecip')->findBy(array('etat' => 'Réceptionné', 'archive' => 0));
			$entitiesarticlescom = $em->getRepository('PhareosLogisToolBoxBundle:com_recip')->findAll();
			$articles = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findAll();
			$entitiesop = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('etat' => 'En cours de préparation', 'archive' => 0, 'valid' =>0));
			$entitiesarticlesop = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findAll();
			
		}
		else
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:commanderecip')->findBy(array('etat' => 'Réceptionné', 'client' => $societeUSER, 'archive' => 0, ), array('numor' => 'desc'));
			$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
			$entitiescomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->findBy(array('iduser' => $userid));
			$entitiesarticlescom = $em->getRepository('PhareosLogisToolBoxBundle:com_recip')->findAll();
			$articles = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findAll();
			$entitiesop = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findBy(array('etat' => 'En cours de préparation', 'archive' => 0, 'client' => $societeUSER, 'valid' =>0));
			$entitiesarticlesop = $em->getRepository('PhareosLogisToolBoxBundle:com_articles')->findAll();
			
			if ($entitiescomarticlestmp) {

        
			foreach ($entitiescomarticlestmp as $articlestmp)
				{
					$idArticletmp = $articlestmp->getID();
					$entitycomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->find($idArticletmp);
			
					$em->remove($entitycomarticlestmp);
					$em->flush();
				}
			}
		}
		
		//$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:commanderecip:index.html.twig', array(
            'entities' => $entities,
			'entitiesarticlescom' => $entitiesarticlescom,
			'articles' => $articles,
			'entitiesarticlesop' => $entitiesarticlesop,
			'entitiesop' => $entitiesop
        ));
    }
	
	public function indexattenteAction()
    {
        
		$session = $this->get('session');
		$societeUSER = $session->get('societeUSER');
		$applicationUSER = $session->get('applicationUSER');
		
		$em = $this->getDoctrine()->getEntityManager();

        if ($applicationUSER == 'Toolbox')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:commanderecip')->findBy(array('etat' => 'En attente de réception', 'archive' => 0));
			$entitiesarticlescom = $em->getRepository('PhareosLogisToolBoxBundle:com_recip')->findAll();
			$articles = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findAll();
		}
		else
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:commanderecip')->findBy(array('etat' => 'En attente de réception', 'client' => $societeUSER, 'archive' => 0, ), array('numor' => 'desc'));
			$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
			$entitiescomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->findBy(array('iduser' => $userid));
			$entitiesarticlescom = $em->getRepository('PhareosLogisToolBoxBundle:com_recip')->findAll();
			$articles = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findAll();
			if ($entitiescomarticlestmp) {

        
			foreach ($entitiescomarticlestmp as $articlestmp)
				{
					$idArticletmp = $articlestmp->getID();
					$entitycomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->find($idArticletmp);
			
					$em->remove($entitycomarticlestmp);
					$em->flush();
				}
			}
		}
		
		//$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:commanderecip:index.html.twig', array(
            'entities' => $entities,
			'entitiesarticlescom' => $entitiesarticlescom,
			'articles' => $articles
        ));
    }
	
	public function indexarchiveAction()
    {
        
		$session = $this->get('session');
		$societeUSER = $session->get('societeUSER');
		$applicationUSER = $session->get('applicationUSER');
		
		$em = $this->getDoctrine()->getEntityManager();

        if ($applicationUSER == 'Toolbox')
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:commanderecip')->findBy(array('archive' => 1));
			$entitiesarticlescom = $em->getRepository('PhareosLogisToolBoxBundle:com_recip')->findAll();
			$articles = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findAll();
		}
		else
		{
			$entities = $em->getRepository('PhareosLogisToolBoxBundle:commanderecip')->findBy(array('client' => $societeUSER, 'archive' => 1), array('numor' => 'desc'));
			$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
			$entitiescomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->findBy(array('iduser' => $userid));
			$entitiesarticlescom = $em->getRepository('PhareosLogisToolBoxBundle:com_recip')->findAll();
			$articles = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findAll();
			if ($entitiescomarticlestmp) {

        
			foreach ($entitiescomarticlestmp as $articlestmp)
				{
					$idArticletmp = $articlestmp->getID();
					$entitycomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->find($idArticletmp);
			
					$em->remove($entitycomarticlestmp);
					$em->flush();
				}
			}
		}
		
		//$entities = $em->getRepository('PhareosLogisToolBoxBundle:comclients')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:commanderecip:index.html.twig', array(
            'entitiesarticlescom' => $entitiesarticlescom,
			'articles' => $articles,
			'entities' => $entities
        ));
    }
	

	
	public function visualisationstockrecipAction()
    {
        
		$session = $this->get('session');
		$session->set('liste_erreurs_session', '');
		
		$societeUSER = $session->get('societeUSER');
		$applicationUSER = $session->get('applicationUSER');
		
		$em = $this->getDoctrine()->getEntityManager();

		if ($applicationUSER == 'Toolbox')
		{
			$entitiesarticles = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findAll();
		}
		else
		{
			$entitiesarticles = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findBy(array('client' => $societeUSER));
		}
		
		
		//$entitiesarticles = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findAll();
        

		
		
        return $this->render('PhareosLogisToolBoxBundle:comclients:visualisationstock.html.twig', array(
            'entities' => $entitiesarticles
        ));
    }
	
	

    /**
     * Finds and displays a comclients entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:commanderecip')->find($id);
		
		$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
		
		$entitiescomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->findBy(array('iduser' => $userid));
		
		//on recup l'id de livraison
		//$entityIdLivr = $entity->getAdlivrid();
		
		//on recup l'entité livraison destination
		//$entitylivraison = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($entityIdLivr);
		
		//on recup les articles commandés (tous les enregistrement ayant l'id de la commande
		$entitiesarticlescom = $em->getRepository('PhareosLogisToolBoxBundle:com_recip')->findBy(array('idcomrecips' => $id));
		
		//on recup les articles commandés
		$cpt=0;
		//$qTTarAY=array(NULL);
		$idArticlearAY=array(NULL,NULL);
		//$designatArticlarAY=array(NULL);
		//$comptId=array(NULL);
		$resultCount = count($entitiesarticlescom);

		//echo $resultCount;
		if ($resultCount > 1) //recherche si plusieurs articles
		{
		
		foreach ($entitiesarticlescom as $articlescom) //pour tous les articles de la commande
			{
			
			
			//qtte commandé
			$qtteArticleCom = $articlescom->getQtte();
			$qttereception = $articlescom->getQttereception();
			$qtteattente = $articlescom->getQtteattente();
			
			//id de l'article
			$idArticle = $articlescom->getIdarticles();
			
			//on recup l'enregistrement de l'artcile
			$Article = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticle);
			
			
			
			//designation de l'article
			$designatArticle = $Article->getDesignat();
			$idArticle = $Article->getId();
			
			
			
			//$qTTarAY[$cpt] = $qtteArticleCom;
			//$qTTarAY[$cpt] = $qtteArticleCom;
			//$designatArticlarAY[$cpt] = $designatArticle;
			//$comptId[$cpt] = $cpt;
			
			$idArticlearAY[$cpt] = array(
				'qtte' => $qtteArticleCom,
				'designat' => $designatArticle,
				'qttereception' => $qttereception,
				'qtteattente' => $qtteattente,
				'identArticle' => $idArticle,
			);
			
			//$cpttot = $cpt;
			
			$cpt = $cpt + 1;
			}
			
			foreach ($entitiescomarticlestmp as $articlestmp)
			{
				$idArticletmp = $articlestmp->getID();
				$entitycomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->find($idArticletmp);
			
				$em->remove($entitycomarticlestmp);
				$em->flush();
			}
			
			return $this->render('PhareosLogisToolBoxBundle:commanderecip:show.html.twig', array(
			'resultcount' => $resultCount,
			'articles' => $Article,
			'idarticle'=> $idArticlearAY,
			'qttereception' => $qttereception,
			'qtteattente' => $qtteattente,
			//'destination' => $entitylivraison,
			'entity'      => $entity,
			));
			$deleteForm = $this->createDeleteForm($id);
		}
		else //si un seul article
		{
			$entityarticlecom = $em->getRepository('PhareosLogisToolBoxBundle:com_recip')->findOneBy(array('idcomrecips' => $id));
			$qtteArticleCom = $entityarticlecom->getQtte();
			$qttereception = $entityarticlecom->getQttereception();
			$qtteattente = $entityarticlecom->getQtteattente();
			
			$idArticle = $entityarticlecom->getIdarticles();
			$Article = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticle);
			$designatArticle = $Article->getDesignat();
			$idArticle = $Article->getId();
			
			
			$idArticlearAY = array(
				'qtte' => $qtteArticleCom,
				'designat' => $designatArticle,
				'qttereception' => $qttereception,
				'qtteattente' => $qtteattente,
				'identArticle' => $idArticle,
			);
			
			foreach ($entitiescomarticlestmp as $articlestmp)
			{
				$idArticletmp = $articlestmp->getID();
				$entitycomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->find($idArticletmp);
			
				$em->remove($entitycomarticlestmp);
				$em->flush();
			}
			
			return $this->render('PhareosLogisToolBoxBundle:commanderecip:show.html.twig', array(
			'resultcount' => $resultCount,
			'articles' => $Article,
			'qtte'		  => $qtteArticleCom,
			'qttereception'		  => $qttereception,
			'qtteattente'		  => $qtteattente,
			'designat'	  => $designatArticle,
			'identArticle' => $idArticle,
			//'destination' => $entitylivraison,
			'entity'      => $entity,
			));
			
			$deleteForm = $this->createDeleteForm($id);
		}
		
		foreach ($entitiescomarticlestmp as $articlestmp)
			{
				$idArticletmp = $articlestmp->getID();
				$entitycomarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->find($idArticletmp);
			
				$em->remove($entitycomarticlestmp);
				$em->flush();
			}
		

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find comclients entity.');
        }

        
    }

    /**
     * Displays a form to create a new comclients entity.
     *
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
		$session = $this->get('session');
		
		$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
		
		$idDestSelect = $session->get('idDestSelectSession');
		//$entitydestselect = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($idDestSelect);
		$entitiesarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->findBy(array('iduser' => $userid));
		
		$entity = new commanderecip();
        $form   = $this->createForm(new commanderecipType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:commanderecip:new.html.twig', array(
            'entitiesarticlestmp' => $entitiesarticlestmp,
			//'entitydestselect' => $entitydestselect,
			'entity' => $entity,
            'form'   => $form->createView()
        ));
    }
	
	public function newpanAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
		$session = $this->get('session');
		
		$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
		
		$idDestSelect = $session->get('idDestSelectSession');
		//$entitydestselect = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($idDestSelect);
		$entitiesarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->findBy(array('iduser' => $userid));
		
		$entity = new commanderecip();
        $form   = $this->createForm(new commanderecipType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:commanderecip:new.html.twig', array(
            'entitiesarticlestmp' => $entitiesarticlestmp,
			//'entitydestselect' => $entitydestselect,
			'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new commanderecip entity.
     *
     */
    public function createAction()
    {
        //on recupère l'ID de la destination
		$session = $this->get('session');
		//$idDestSelect = $session->get('idDestSelectSession');
		
		//on recup l'utilisateur qui passe commande
			$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
		
		//on recup le nom du site
		$em = $this->getDoctrine()->getEntityManager();
		//$entitysite = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($idDestSelect);
		//$nomdusite = $entitysite->getSite();
		
		//on recup la societe qui passe la commande
		$societeUSER = $session->get('societeUSER');
		
		$i = 0;
		
		//on récupère tous les enregistrement de la table temporaire com_recip_tmp de l'utilisateur $userid
		$entitiesarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip_tmp')->findBy(array('iduser' => $userid));
			foreach ($entitiesarticlestmp as $articlestmp) //pour chaque enregistrement de la table temporaire
			{
		
		// on creer un nouvel enregistrement commandeclients
		$entity  = new commanderecip();
        $request = $this->getRequest();
        $form    = $this->createForm(new commanderecipType(), $entity);
        $form->bindRequest($request);
		$datelivrprev = $request->request->get('datelivrprev');
		$datelivrprev2 = new \DateTime($datelivrprev);
		
		$i = $i + 1;
		
		$numOR = $entity->getDatecom();
		$numOR2 = $numOR->format('YmdHi').'M'.$i;

        //if ($form->isValid()) {
            
			
			//$entity->setNomdusite($nomdusite);
			//$entity->setAdlivrid($idDestSelect);
			$entity->setArchive(0);
			$entity->setNumor($numOR2);
			$entity->setDatelivrprev($datelivrprev2);
			$entity->setEtat('En attente de réception');
			$entity->setClient($societeUSER);
			
			//upload the file
			//$entity->uploadPdf();
			
            $em->persist($entity);
			$em->flush();
			
            
			
			//on récupère l'id de la commande commandeclients
			$idcommande = $entity->getId();
			$session->set('idcommande', $idcommande);
			
			
				//on creer un nouvelle enregistrement com_recip
				$entitycomarticles = new com_recip();
				
				//on récupère la qtte de l'article commandé en temporaire
				$qTteArticletmp = $articlestmp->getQtte();
				
				//on récuprère le nom de l'article de la table temporaire
				$nOMArticletmp = $articlestmp->getNomarticle();
				
				//on recherche dans la table article l'enregistrement correspondant au nom trouvé
				$articlesentities = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findOneBy(array ('designat' => $nOMArticletmp));
				
				//on récupère l'idArticle de cet enregistrement
				$articleIdfromarticlesentities = $articlesentities->getId();
				
				//on récupère la qtte attente de réception de cet enregistrement: quantité encours
				$articleQteencoursfromarticlesentities = $articlesentities->getQteencours();
				
				//on calcul le stock en attente de réception
				$articleQteencoursnouveauStock = $articleQteencoursfromarticlesentities + $qTteArticletmp;
				
				//on récupère l'article dont on doit modifier le stock en attente de recep
				$entityArticle = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($articleIdfromarticlesentities);
				
				//on modifie le stock de l'article concerné
				$entityArticle->setQteencours($articleQteencoursnouveauStock);
				
				$entitycomarticles->setIdcomrecips($idcommande);
				$entitycomarticles->setIdarticles($articleIdfromarticlesentities);
				$entitycomarticles->setQtte($qTteArticletmp);
				$entitycomarticles->setQttereception(0);
				$entitycomarticles->setQtteattente($qTteArticletmp);
				$em->persist($entitycomarticles);
				//$em->persist($entityArticle);
				$em->flush();
				
				
				
				
				
			}
			
			
			

            return $this->redirect($this->generateUrl('commanderecip'));
            
        //}

        //return $this->render('PhareosLogisToolBoxBundle:commanderecip:new.html.twig', array(
        //    'entity' => $entity,
        //    'form'   => $form->createView()
        //));
    }

    /**
     * Displays a form to edit an existing commanderecip entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:commanderecip')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find commanderecip entity.');
        }
		
		$entitiesarticlescom = $em->getRepository('PhareosLogisToolBoxBundle:com_recip')->findBy(array ('idcomrecips' => $id));
		
		
		$articles = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findAll();

        $editForm = $this->createForm(new commanderecipAdminType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:commanderecip:edit.html.twig', array(
            'entitiesarticlescom' => $entitiesarticlescom,
			'articles' => $articles,
			'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing commanderecip entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:commanderecip')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find commanderecip entity.');
        }

        $editForm   = $this->createForm(new commanderecipAdminType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);
		
		$etat = $entity->getEtat();
		
		$datereception = $request->request->get('datereception');
		$datereception2 = new \DateTime($datereception);

        //if ($editForm->isValid()) {
		
            $em->persist($entity);
            $em->flush();
		
		if ($etat == 'Réceptionné') {
		
		//on enregistre la date de réception
		$entity->setDatereception($datereception2);
		$em->persist($entity);
        $em->flush();
		
		//on crée l'enregistrement ORS
		$entityORS = new ordrecstock();
		$numOR = $entity->getNumor();
		$dateornum = new \DateTime();
		$numORS2 = $dateornum->format('mdHis');
		$numORS = $numOR . 'S' . $numORS2;
		$entityORS->setNumors($numORS);
		$entityORS->setDaterecep($datereception2);
		$entityORS->setStock(0);
		$entityORS->setCommanderecip($entity);
		
		
		
		//on récupère tous les enregistrement de la table article suivant la commande réception
		$entitiesarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip')->findBy(array('idcomrecips' => $id));
		foreach ($entitiesarticlestmp as $articlestmp) //pour chaque enregistrement de la table commande reception com_recip
			{
				
				
				//on récupère la qtte de l'article commandé de com_recip
				$qTteArticletmp = $articlestmp->getQtte();
				
				//on récupère l'id du com_recip
				$idComRecip = $articlestmp->getId();
				
				//on récupère la quantité en attente de réception dans com_recip
				$qtteattente = $articlestmp->getQtteattente();
				
				//on récupère la quantité déjà réceptionnée dans com_recip
				$qttereception = $articlestmp->getQttereception();
				
				
				//on récupère l'article de la table com_recip
				$idArticlerecu = $articlestmp->getIdarticles();
				
				//on recherche dans la table article l'enregistrement correspondant a l'id trouvé
				$articlesentities = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticlerecu);
				
				$entityORS->setArticles($articlesentities);
				$entityORS->setQteastocker($qtteattente);
				$entityORS->setQtestockee(0);
				
								
				//on récupère la qtte de cet enregistrement: quantité total en stock (stock visu client)
				$articleQtetotfromarticlesentities = $articlesentities->getQtetot();
				
				//on récupère la qtte de cet enregistrement: quantité total en stock (stock physique)
				$articleQtephysfromarticlesentities = $articlesentities->getQtephys();
				
				//on récupère la qtte en cours de cet enregistrement: quantité en cours
				$articleQteencoursfromarticlesentities = $articlesentities->getQteencours();
				
				//on calcul le stock restant (stock visu client)
				$articleQtetotnouveauStock = $articleQtetotfromarticlesentities + $qtteattente;
				
				//on calcul le stock restant (stock phys)
				$articleQtephysnouveauStock = $articleQtephysfromarticlesentities + $qtteattente;
				
				//on calcul le stock en cours à modifier
				$articleQteencoursnouveauStock = $articleQteencoursfromarticlesentities - $qtteattente;
				
				
				
				//on modifie le stock de l'article concerné (stock visu client)
				$articlesentities->setQtetot($articleQtetotnouveauStock);
				
				//on modifie le stock de l'article concerné (stock physique)
				$articlesentities->setQtephys($articleQtephysnouveauStock);
				
				//on modifie le stock en cours de l'article concerné
				$articlesentities->setQteencours($articleQteencoursnouveauStock);
				
				
				$em->persist($articlesentities);
				$em->flush();
				
				//on calcul le stock restant de la qtte en attente de réception ds com_recip
				$nouvqtteattente = 0;
				
				// on calcul le stock restant de la qtte déjà receptionné ds com_recip
				$nouvqttereception = $qTteArticletmp;
				
				
				$entitycomrecip = $em->getRepository('PhareosLogisToolBoxBundle:com_recip')->find($idComRecip);
				
				//on modifie la quantité de stock en attente dans com_recip
				$entitycomrecip->setQtteattente($nouvqtteattente);
				
				//on modifie la quantité de stock déjà receptionné dans com_recip
				$entitycomrecip->setQttereception($nouvqttereception);
				
				$em->persist($entitycomrecip);
				$em->flush();
				
				
				
			}
			$em->persist($entityORS);
			$em->flush();
		}
		else if ($etat == 'Réception Partielle') {
		
		//on enregistre la date de réception
		$entity->setDatereception($datereception2);
		$em->persist($entity);
        $em->flush();
		
		//on crée l'enregistrement ORS
		$entityORS = new ordrecstock();
		$numOR = $entity->getNumor();
		$dateornum = new \DateTime();
		$numORS2 = $dateornum->format('mdHis');
		$numORS = $numOR . 'S' . $numORS2;
		$entityORS->setNumors($numORS);
		$entityORS->setDaterecep($datereception2);
		$entityORS->setStock(0);
		$entityORS->setCommanderecip($entity);
		
		//on récupère tous les enregistrement de la table article suivant la commande réception
		$entitiesarticlestmp = $em->getRepository('PhareosLogisToolBoxBundle:com_recip')->findBy(array('idcomrecips' => $id));
		foreach ($entitiesarticlestmp as $articlestmp) //pour chaque enregistrement de la table commande reception com_recip
			{
				
				
				//on récupère la qtte de l'article commandé de com_recip
				$qTteArticletmp = $articlestmp->getQtte();
				
				//on récupère l'id du com_recip
				$idComRecip = $articlestmp->getId();
				
				//on récupère la quantité réceptionné si réception partielle qtte saisie
				$qttereception2 = 'qttereception'.$idComRecip;
				$qTterecepartielle = $request->request->get($qttereception2);
				
				//on récupère la quantité en attente de réception dans com_recip
				$qtteattente = $articlestmp->getQtteattente();
				
				if ($qTterecepartielle == $qtteattente)
				{
					$entity->setEtat('Réceptionné');
					$em->persist($entity);
					$em->flush();
				}
				
				//on récupère la quantité déjà réceptionnée dans com_recip
				$qttereception = $articlestmp->getQttereception();
				
				//on récupère l'article de la table com_recip
				$idArticlerecu = $articlestmp->getIdarticles();
				
				//on recherche dans la table article l'enregistrement correspondant a l'id trouvé
				$articlesentities = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($idArticlerecu);
				
								
				//on récupère la qtte de cet enregistrement: quantité total en stock (stock visu client)
				$articleQtetotfromarticlesentities = $articlesentities->getQtetot();
				
				//on récupère la qtte de cet enregistrement: quantité total en stock (stock physique)
				$articleQtephysfromarticlesentities = $articlesentities->getQtephys();
				
				//on récupère la qtte en cours de cet enregistrement: quantité en cours
				$articleQteencoursfromarticlesentities = $articlesentities->getQteencours();
				
				//on calcul le stock restant (stock visu client)
				$articleQtetotnouveauStock = $articleQtetotfromarticlesentities + $qTterecepartielle;
				
				//on calcul le stock restant (stock phys)
				$articleQtephysnouveauStock = $articleQtephysfromarticlesentities + $qTterecepartielle;
				
				//on calcul le stock en cours à modifier
				$articleQteencoursnouveauStock = $articleQteencoursfromarticlesentities - $qTterecepartielle;
				
				$entityORS->setArticles($articlesentities);
				$entityORS->setQteastocker($qTterecepartielle);
				$entityORS->setQtestockee(0);
				
				//on modifie le stock de l'article concerné (stock visu client)
				$articlesentities->setQtetot($articleQtetotnouveauStock);
				
				//on modifie le stock de l'article concerné (stock physique)
				$articlesentities->setQtephys($articleQtephysnouveauStock);
				
				//on modifie le stock en cours de l'article concerné
				$articlesentities->setQteencours($articleQteencoursnouveauStock);
				
				
				$em->persist($articlesentities);
				$em->flush();
				
				
				//on calcul le stock restant de la qtte en attente de réception ds com_recip
				$nouvqtteattente = $qtteattente - $qTterecepartielle;
				
				// on calcul le stock restant de la qtte déjà receptionné ds com_recip
				$nouvqttereception = $qttereception + $qTterecepartielle;
				
				
				$entitycomrecip = $em->getRepository('PhareosLogisToolBoxBundle:com_recip')->find($idComRecip);
				
				//on modifie la quantité de stock en attente dans com_recip
				$entitycomrecip->setQtteattente($nouvqtteattente);
				
				//on modifie la quantité de stock déjà receptionné dans com_recip
				$entitycomrecip->setQttereception($nouvqttereception);
				
				$em->persist($entitycomrecip);
				$em->flush();
				
				
				
				
			}
		
			$em->persist($entityORS);
			$em->flush();
		
		}
		
		
            return $this->redirect($this->generateUrl('commanderecip'));
        //}

        //return $this->render('PhareosLogisToolBoxBundle:commanderecip:edit.html.twig', array(
           // 'entity'      => $entity,
           // 'edit_form'   => $editForm->createView(),
           // 'delete_form' => $deleteForm->createView(),
        //));
    }

    /**
     * Deletes a commanderecip entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosLogisToolBoxBundle:commanderecip')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find commanderecip entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('commanderecip'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
