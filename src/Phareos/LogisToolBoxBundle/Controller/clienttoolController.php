<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\LogisToolBoxBundle\Entity\clienttool;
use Phareos\LogisToolBoxBundle\Form\clienttoolType;
use Phareos\LogisToolBoxBundle\Form\clienttooleditType;

/**
 * clienttool controller.
 *
 */
class clienttoolController extends Controller
{
    /**
     * Lists all clienttool entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:clienttool')->findAll();

        return $this->render('PhareosLogisToolBoxBundle:clienttool:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a clienttool entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:clienttool')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find clienttool entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:clienttool:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new clienttool entity.
     *
     */
    public function newAction()
    {
        $entity = new clienttool();
        $form   = $this->createForm(new clienttoolType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:clienttool:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new clienttool entity.
     *
     */
    public function createAction()
    {
        $entity  = new clienttool();
        $request = $this->getRequest();
        $form    = $this->createForm(new clienttoolType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('clienttool_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosLogisToolBoxBundle:clienttool:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing clienttool entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:clienttool')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find clienttool entity.');
        }

        $editForm = $this->createForm(new clienttooleditType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:clienttool:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing clienttool entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:clienttool')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find clienttool entity.');
        }

        $editForm   = $this->createForm(new clienttooleditType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('clienttool_show', array('id' => $id)));
        }

        return $this->render('PhareosLogisToolBoxBundle:clienttool:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a clienttool entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosLogisToolBoxBundle:clienttool')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find clienttool entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('clienttool'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
