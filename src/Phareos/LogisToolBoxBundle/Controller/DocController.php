<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Phareos\LogisToolBoxBundle\Entity\doccom;
use Phareos\LogisToolBoxBundle\Form\doccomType;

class DocController extends Controller
{
    
    public function indexAction()
    {
		$em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosLogisToolBoxBundle:doccom')->findAll();
		
		$entity = new doccom();
        $form   = $this->createForm(new doccomType(), $entity);

        return $this->render('PhareosLogisToolBoxBundle:Doc:index.html.twig', array(
            'entities' => $entities,
			'entity' => $entity,
            'form'   => $form->createView()
        ));
		
		//return $this->render('PhareosLogisToolBoxBundle:Doc:index.html.twig');
    }
	
	public function createAction()
    {
		$entity  = new doccom();
        $request = $this->getRequest();
        $form    = $this->createForm(new doccomType(), $entity);
        $form->bindRequest($request);
		//$date = new \DateTime('now');
		$auteur = $this->container->get('security.context')->getToken()->getUser();
		
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
			//$entity->setDate($date);
			$entity->setAuteur($auteur);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Doc'));
            
        }

        return $this->render('Doc', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }
}