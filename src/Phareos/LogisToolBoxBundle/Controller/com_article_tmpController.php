<?php

namespace Phareos\LogisToolBoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\LogisToolBoxBundle\Entity\com_article_tmp;
use Phareos\LogisToolBoxBundle\Form\com_article_tmpType;
use Symfony\Component\HttpFoundation\Response;

/**
 * com_article_tmp controller.
 *
 */
class com_article_tmpController extends Controller
{
    /**
     * Lists all com_article_tmp entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

		$session = $this->get('session');
		$request = $this->get('request');
		$idDestSelect = $request->query->get('idDestSelect');
		
		$session->set('idDestSelectSession', $idDestSelect);
		
		$societeUSER = $session->get('societeUSER');
		
		$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
		
        $entities = $em->getRepository('PhareosLogisToolBoxBundle:com_article_tmp')->findBy(array('iduser' => $userid));
		$entitydestselect = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($idDestSelect);
		$entitiesarticles = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findBy(array('client' => $societeUSER),
                                      array('designat' => 'ASC'));
		
		//session_start();
		$_SESSION['societe'] = $societeUSER;
		
		$entity = new com_article_tmp();
        $form   = $this->createForm(new com_article_tmpType(), $entity);
		
		
		
        return $this->render('PhareosLogisToolBoxBundle:com_article_tmp:index.html.twig', array(
            'entitiesarticles' => $entitiesarticles,
			'entities' => $entities,
			'entitydestselect' => $entitydestselect,
			'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Finds and displays a com_article_tmp entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:com_article_tmp')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find com_article_tmp entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:com_article_tmp:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new com_article_tmp entity.
     *
     */
    public function newAction()
    {
        $entity = new com_article_tmp();
        $form   = $this->createForm(new com_article_tmpType(), $entity);

		
		
        return $this->render('PhareosLogisToolBoxBundle:com_article_tmp:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new com_article_tmp entity.
     *
     */
    public function createAction()
    {
        $entity  = new com_article_tmp();
        $request = $this->getRequest();
        $form    = $this->createForm(new com_article_tmpType(), $entity);
        $form->bindRequest($request);
		
		$session = $this->get('session');
		$session->set('liste_erreurs_session', 'ok');
		
		$em = $this->getDoctrine()->getEntityManager();
		$idDestSelect = $session->get('idDestSelectSession');
		$entities = $em->getRepository('PhareosLogisToolBoxBundle:com_article_tmp')->findAll();
		
		$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
		
		
		$entitydestselect = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($idDestSelect);

		// On recup le nom de l'article
		//$nomarticle = $entity->getNomarticle();
		$nomarticle = $_POST['nomarticle2'];
		
		
		//On recup la quantité en stock de l'article
		$articlesentities = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findOneBy(array ('designat' => $nomarticle));
		$qttearticledispo = $articlesentities->getQtetot();
		$nomarticleenstock = $articlesentities->getDesignat();
		
		//On recup la quantité demandé
		$qttedemande = $entity->getQtte();
		
		//On recup la quantité en attente de l'article
		$qtteattente = $articlesentities->getQteencours();
		
		//On vérifi si l'article na pas déjà été commandé
		$commtemp = $em->getRepository('PhareosLogisToolBoxBundle:com_article_tmp')->findOneBy(array ('nomarticle' => $nomarticle));
		
		
		
		//on calcul la qantitée restante
		$qttearticledispo = ($qttearticledispo + $qtteattente) - $qttedemande;
		
		
		
        //if(($qttearticledispo < 0)) //test si la quantité est supp au stock dispo
        //{
        //    $message_erreur = "Quantité invalide par rapport au Stock votre quantité en attente de réception n'est pas suffisante vous devez passer un ordre de réception d'au moins ".abs($qttearticledispo)." articles";
		//	$session->set('liste_erreurs_session', $message_erreur);
			
			
			
		//	return $this->redirect($this->generateUrl('comarticletmp', array(			
		//		'id' => $entity->getId(),
		//		'idDestSelect' => $idDestSelect,
		//		'entities' => $entities,
		//		'entitydestselect' => $entitydestselect
		//		)));
			
			//return new Response(print_r($liste_erreurs, true));
        //}
        //elseif (($commtemp))
		if (($commtemp))
        {
            $message_erreur = "Article déjà commandé !!!";
			$session->set('liste_erreurs_session', $message_erreur);
			
			
            return $this->redirect($this->generateUrl('comarticletmp', array(
				'id' => $entity->getId(),
				'idDestSelect' => $idDestSelect,
				'entities' => $entities,
				'entitydestselect' => $entitydestselect
				)));
        }
		else
		{
			$entity->setIduser($userid);
			$entity->setNomarticle($nomarticle);
			$em->persist($entity);
            $em->flush();
			$session->set('liste_erreurs_session', 'ok');
            return $this->redirect($this->generateUrl('comarticletmp', array(
				'id' => $entity->getId(),
				'idDestSelect' => $idDestSelect,
				'entities' => $entities,
				'entitydestselect' => $entitydestselect
				)));
		}
        
    }
	
	public function createpanAction()
    {
        $request = $this->getRequest();
		$session = $this->get('session');
		$idDestSelect = $session->get('idDestSelectSession');
		$em = $this->getDoctrine()->getEntityManager();
		//$nomarticle = $_POST['nomarticle2'];
		
		$entities = $em->getRepository('PhareosLogisToolBoxBundle:com_article_tmp')->findAll();
		
		$product_listcontrole = $request->request->get('products_selected');
		
		if(isset($product_listcontrole)){
			$product_list = $_POST['products_selected'];
		
		
			if(isset($product_list)){
				foreach($product_list as $p_list){
				
					$entity  = new com_article_tmp();
					
					$chunks = explode('|',$p_list);
					$plist_id = $chunks[0];
					$plist_qty = $chunks[1];
					
					$request = $this->getRequest();
					$form    = $this->createForm(new com_article_tmpType(), $entity);
					$form->bindRequest($request);
					
					$idDestSelect = $session->get('idDestSelectSession');
					
					
					$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
					
					$entitydestselect = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($idDestSelect);
					
					// On recup le nom de l'article 
					//$nomarticle = $entity->getNomarticle();
					$articlepan = $em->getRepository('PhareosLogisToolBoxBundle:articles')->find($plist_id);
					
					$nomarticle = $articlepan->getDesignat();
					
					$entity->setIduser($userid);
					$entity->setIdarticle($plist_id);
					$entity->setNomarticle($nomarticle);
					$entity->setQtte($plist_qty);
					$em->persist($entity);
					$em->flush();
				}
				
				$session->set('liste_erreurs_session', 'ok');
			
				$entitydestselect = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($idDestSelect);
			
				return $this->redirect($this->generateUrl('comclients_newpan', array(
					'id' => $entity->getId(),
					'idDestSelect' => $idDestSelect,
					'entities' => $entities,
					'entitydestselect' => $entitydestselect
					)));
			
			
			}
			
			
		
			
		}
		
		
		
		
		
		$session = $this->get('session');
		$request = $this->get('request');
		$idDestSelect = $session->get('idDestSelectSession');
		
		
		
		$societeUSER = $session->get('societeUSER');
		
		$userid = $this->container->get('security.context')->getToken()->getUser()->getid();
		
        $entities = $em->getRepository('PhareosLogisToolBoxBundle:com_article_tmp')->findBy(array('iduser' => $userid));
		$entitydestselect = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($idDestSelect);
		$entitiesarticles = $em->getRepository('PhareosLogisToolBoxBundle:articles')->findBy(array('client' => $societeUSER),
                                      array('designat' => 'ASC'));
		
		//session_start();
		$_SESSION['societe'] = $societeUSER;
		
		$entity = new com_article_tmp();
        $form   = $this->createForm(new com_article_tmpType(), $entity);
		
		
		
        return $this->render('PhareosLogisToolBoxBundle:com_article_tmp:index.html.twig', array(
            'entitiesarticles' => $entitiesarticles,
			'entities' => $entities,
			'entitydestselect' => $entitydestselect,
			'entity' => $entity,
            'form'   => $form->createView()
        ));
		
		
        
    }

    /**
     * Displays a form to edit an existing com_article_tmp entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:com_article_tmp')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find com_article_tmp entity.');
        }

        $editForm = $this->createForm(new com_article_tmpType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosLogisToolBoxBundle:com_article_tmp:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing com_article_tmp entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosLogisToolBoxBundle:com_article_tmp')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find com_article_tmp entity.');
        }

        $editForm   = $this->createForm(new com_article_tmpType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('com_article_tmp_edit', array('id' => $id)));
        }

        return $this->render('PhareosLogisToolBoxBundle:com_article_tmp:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a com_article_tmp entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();
		$session = $this->get('session');
		$em = $this->getDoctrine()->getEntityManager();

        $form->bindRequest($request);
		
		$idDestSelect = $session->get('idDestSelectSession');
		$entities = $em->getRepository('PhareosLogisToolBoxBundle:com_article_tmp')->findAll();
		$entitydestselect = $em->getRepository('PhareosLogisToolBoxBundle:destination')->find($idDestSelect);
		

        //if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosLogisToolBoxBundle:com_article_tmp')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find com_article_tmp entity.');
            }

            $em->remove($entity);
            $em->flush();
        //}

        return $this->redirect($this->generateUrl('comarticletmp', array(			
				//'id' => $entity->getId(),
				'idDestSelect' => $idDestSelect,
				'entities' => $entities,
				'entitydestselect' => $entitydestselect
				)));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
