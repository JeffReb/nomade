<?php

namespace Phareos\LogisToolBoxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class com_recipType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('idcomrecips')
            ->add('idarticles')
            ->add('qtte')
        ;
    }

    public function getName()
    {
        return 'phareos_logistoolboxbundle_com_reciptype';
    }
}
