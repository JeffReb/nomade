<?php

namespace Phareos\LogisToolBoxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class comclientsType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('install', 'choice', array('choices' => array(1 => "Intervention à prévoir", 0 => "Livraison simple"), 
                                            'multiple' => false, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => false,
                                            'empty_data'  => null,
											'required'   => true
                                            ))
            ->add('comm')
            ->add('compdf')
			->add('compdf2')
			->add('livrimp')
			->add('numcommaint')
			->add('datelivrend')
			->add('autoor')
        ;
    }

    public function getName()
    {
        return 'phareos_logistoolboxbundle_comclientstype';
    }
}
