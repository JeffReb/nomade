<?php

namespace Phareos\LogisToolBoxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Phareos\LogisToolBoxBundle\Entity\clienttool;
use Phareos\LogisToolBoxBundle\Entity\clienttoolRepository;
use Phareos\LogisToolBoxBundle\Entity\categorie;
use Phareos\LogisToolBoxBundle\Entity\categorieRepository;
use Phareos\LogisToolBoxBundle\Entity\fournisseur;
use Phareos\LogisToolBoxBundle\Entity\fournisseurRepository;

class articlesType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        //$societeUSER = $builder->getData()->getClient();//le parametre à passer à la fonction
		$builder
            ->add('nom', 'hidden')
            ->add('designat', 'hidden')
            ->add('reference')
			->add('fourniss', 'entity', array('class' => 'PhareosLogisToolBoxBundle:fournisseur',
												'property' => 'nom',
												'query_builder' => function(fournisseurRepository $er) {
												return $er->createQueryBuilder ('u')
												->orderBY ('u.nom', 'ASC');
												},
												))
			->add('categorie', 'entity', array('class' => 'PhareosLogisToolBoxBundle:categorie',
												'property' => 'categorie',
												'query_builder' => function(categorieRepository $er) {
												return $er->createQueryBuilder ('u')
												->orderBY ('u.categorie', 'ASC');
												},
												))
			->add('client', 'entity', array('class' => 'PhareosLogisToolBoxBundle:clienttool',
												'property' => 'nom',
												'query_builder' => function(clienttoolRepository $er) {
												return $er->createQueryBuilder ('u')
												->orderBY ('u.nom', 'ASC');
												},
												))
			//->add('client', 'choice', array('choices' => array('Travo Services +' => "Travo Services +"), 
                                            //'multiple' => false, 
                                            //'expanded' => false, 
                                            //'preferred_choices' => array(0),
                                            //'empty_value' => '- Choisissez une option -',
                                            //'empty_data'  => null,
											//'required' => false
                                            //))
            //->add('typeunit', 'choice', array('choices' => array('Palettes' => "Palettes", 'Cartons' => "Cartons", 'Boites' => "Boites"), 
                                            //'multiple' => false, 
                                            //'expanded' => false, 
                                            //'preferred_choices' => array(2),
                                            //'empty_value' => '- Choisissez une option -',
                                            //'empty_data'  => null,
											//'required' => false
                                            //))
            ->add('poids')
            ->add('haut')
            ->add('large')
            ->add('profond')
            ->add('qtetot')
			->add('nbpalette')
            //->add('qteencours')
            //->add('qtequarant')
            //->add('daterecep')
            //->add('dateprepa')
        ;
    }

    public function getName()
    {
        return 'phareos_logistoolboxbundle_articlestype';
    }
}
