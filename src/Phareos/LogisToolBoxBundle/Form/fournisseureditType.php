<?php

namespace Phareos\LogisToolBoxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class fournisseureditType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('nom', 'hidden')
            ->add('contact')
            ->add('adres1')
            ->add('adres2')
            ->add('cp')
            ->add('ville')
            ->add('tel')
            ->add('fax')
            ->add('email')
            ->add('idclient', 'hidden')
        ;
    }

    public function getName()
    {
        return 'phareos_logistoolboxbundle_fournisseuredittype';
    }
}
