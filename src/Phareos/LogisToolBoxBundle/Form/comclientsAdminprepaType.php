<?php

namespace Phareos\LogisToolBoxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class comclientsAdminprepaType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            //->add('install')
            //->add('comm')
            //->add('compdf')
			->add('etat', 'choice', array('choices' => array('En attente de préparation' => "En attente de préparation", 'En cours de préparation' => "En cours de préparation"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array('En attente de préparation'),
                                            'empty_value' => false,
											'required' => false
                                            ))
        ;
    }

    public function getName()
    {
        return 'phareos_logistoolboxbundle_comclientsAdminprepaType';
    }
}
