<?php

namespace Phareos\LogisToolBoxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class comclientsAdminatexpType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            //->add('install')
            //->add('comm')
            //->add('compdf')
			->add('etat', 'choice', array('choices' => array('Préparé en attente expédition' => "Préparé en attente expédition", 'En cours de livraison' => "En cours de livraison"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array('Préparé en attente expédition'),
                                            'empty_value' => false,
											'required' => false
                                            ))
        ;
    }

    public function getName()
    {
        return 'phareos_logistoolboxbundle_comclientsAdminatexpType';
    }
}
