<?php

namespace Phareos\LogisToolBoxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class ordrecstockType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('numors')
            ->add('daterecep')
            ->add('datestock')
            ->add('stock')
            ->add('qtestock')
            ->add('commanderecip')
            ->add('articles')
        ;
    }

    public function getName()
    {
        return 'phareos_logistoolboxbundle_ordrecstocktype';
    }
}
