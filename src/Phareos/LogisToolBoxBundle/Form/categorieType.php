<?php

namespace Phareos\LogisToolBoxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Phareos\LogisToolBoxBundle\Entity\clienttool;
use Phareos\LogisToolBoxBundle\Entity\clienttoolRepository;

class categorieType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('categorie')
			->add('client', 'entity', array('class' => 'PhareosLogisToolBoxBundle:clienttool',
												'property' => 'nom',
												'query_builder' => function(clienttoolRepository $er) {
												return $er->createQueryBuilder ('u')
												->orderBY ('u.nom', 'ASC');
												},
												))
        ;
    }

    public function getName()
    {
        return 'phareos_logistoolboxbundle_categorietype';
    }
}
