<?php

namespace Phareos\LogisToolBoxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class comclientsAdminencprepaType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('palettes23')
			->add('qttepal23')
			->add('palettes13')
			->add('qttepal13')
			->add('cartonneuf')
			->add('qttecarton')
			->add('etat', 'choice', array('choices' => array('En cours de préparation' => "En cours de préparation", 'Préparé en attente expédition' => "Préparé en attente expédition"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array('En cours de préparation'),
                                            'empty_value' => false,
											'required' => false
                                            ))
        ;
    }

    public function getName()
    {
        return 'phareos_logistoolboxbundle_comclientsAdminencprepaType';
    }
}
