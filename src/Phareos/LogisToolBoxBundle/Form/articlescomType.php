<?php

namespace Phareos\LogisToolBoxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class articlescomType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('com')
        ;
    }

    public function getName()
    {
        return 'phareos_logistoolboxbundle_articlescomtype';
    }
}
