<?php

namespace Phareos\LogisToolBoxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class commanderecipAdminType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('etat', 'choice', array('choices' => array('En attente de réception' => "En attente de réception", 'Réception Partielle' => "Réception Partielle", 'Réceptionné' => "Réceptionné"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array('En attente de réception'),
                                            'empty_value' => false,
											'required' => false
                                            ))
			//->add('comm')
            //->add('compdf')
			//->add('datelivrprev')
        ;
    }

    public function getName()
    {
        return 'phareos_logistoolboxbundle_commanderecipadmintype';
    }
}
