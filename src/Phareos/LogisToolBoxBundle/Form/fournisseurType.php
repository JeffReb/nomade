<?php

namespace Phareos\LogisToolBoxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Phareos\LogisToolBoxBundle\Entity\clienttool;
use Phareos\LogisToolBoxBundle\Entity\clienttoolRepository;

class fournisseurType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('contact')
            ->add('adres1')
            ->add('adres2')
            ->add('cp')
            ->add('ville')
            ->add('tel')
            ->add('fax')
            ->add('email')
            ->add('idclient', 'entity', array('class' => 'PhareosLogisToolBoxBundle:clienttool',
												'property' => 'nom',
												'query_builder' => function(clienttoolRepository $er) {
												return $er->createQueryBuilder ('u')
												->orderBY ('u.nom', 'ASC');
												},
												))
        ;
    }

    public function getName()
    {
        return 'phareos_logistoolboxbundle_fournisseurtype';
    }
}
