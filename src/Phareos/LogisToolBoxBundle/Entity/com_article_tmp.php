<?php

namespace Phareos\LogisToolBoxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Phareos\LogisToolBoxBundle\Entity\com_article_tmp
 *
 * @ORM\Table(name="too_com_article_tmp")
 * @ORM\Entity(repositoryClass="Phareos\LogisToolBoxBundle\Entity\com_article_tmpRepository")
 */
class com_article_tmp
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer $idarticle
     *
     * @ORM\Column(name="idarticle", type="integer", nullable=true)
     */
    private $idarticle;
	
	/**
     * @var string $nomarticle
     *
     * @ORM\Column(name="nomarticle", type="string", length=255, nullable=true)
     */
    private $nomarticle;

    /**
     * @var integer $qtte
     *
     * @ORM\Column(name="qtte", type="integer")
	 * 
     */
    private $qtte;

    /**
     * @var integer $iduser
     *
     * @ORM\Column(name="iduser", type="integer", nullable=true)
     */
    private $iduser;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idarticle
     *
     * @param integer $idarticle
     */
    public function setIdarticle($idarticle)
    {
        $this->idarticle = $idarticle;
    }

    /**
     * Get idarticle
     *
     * @return integer 
     */
    public function getIdarticle()
    {
        return $this->idarticle;
    }
	
	/**
     * Set nomarticle
     *
     * @param string $nomarticle
     */
    public function setNomarticle($nomarticle)
    {
        $this->nomarticle = $nomarticle;
    }

    /**
     * Get nomarticle
     *
     * @return string 
     */
    public function getNomarticle()
    {
        return $this->nomarticle;
    }

    /**
     * Set qtte
     *
     * @param integer $qtte
     */
    public function setQtte($qtte)
    {
        $this->qtte = $qtte;
    }

    /**
     * Get qtte
     *
     * @return integer 
     */
    public function getQtte()
    {
        return $this->qtte;
    }

    /**
     * Set iduser
     *
     * @param integer $iduser
     */
    public function setIduser($iduser)
    {
        $this->iduser = $iduser;
    }

    /**
     * Get iduser
     *
     * @return integer 
     */
    public function getIduser()
    {
        return $this->iduser;
    }
}