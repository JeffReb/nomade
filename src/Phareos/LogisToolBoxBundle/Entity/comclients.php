<?php

namespace Phareos\LogisToolBoxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Phareos\LogisToolBoxBundle\Entity\comclients
 *
 * @ORM\Table(name="too_comclients")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Phareos\LogisToolBoxBundle\Entity\comclientsRepository")
 */
class comclients
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $datecom
     *
     * @ORM\Column(name="datecom", type="datetime", nullable=true)
     */
    private $datecom;
	
	/**
     * @var string $numop
     *
     * @ORM\Column(name="numop", type="string", length=255, nullable=true)
     */
    private $numop;
	
	/**
     * @var string $nummag
     *
     * @ORM\Column(name="nummag", type="string", length=255, nullable=true)
     */
    private $nummag;
	
	/**
     * @var string $compte
     *
     * @ORM\Column(name="compte", type="string", length=255, nullable=true)
     */
    private $compte;
	
	/**
     * @var string $numcommaint
     *
     * @ORM\Column(name="numcommaint", type="string", length=255, nullable=true)
     */
    private $numcommaint;

    /**
     * @var integer $adlivrid
     *
     * @ORM\Column(name="adlivrid", type="integer", nullable=true)
     */
    private $adlivrid;
	
	/**
     * @var string $nomdusite
     *
     * @ORM\Column(name="nomdusite", type="string", nullable=true)
     */
    private $nomdusite;

    /**
     * @var boolean $install
     *
     * @ORM\Column(name="install", type="boolean", nullable=true)
     */
    private $install;
	
	/**
     * @var boolean $valid
     *
     * @ORM\Column(name="valid", type="boolean", nullable=true)
     */
    private $valid;
	
	/**
     * @var boolean $validor
     *
     * @ORM\Column(name="validor", type="boolean", nullable=true)
     */
    private $validor;
	
	/**
     * @var boolean $palettes23
     *
     * @ORM\Column(name="palettes23", type="boolean", nullable=true)
     */
    private $palettes23;
	
	/**
     * @var boolean $palettes13
     *
     * @ORM\Column(name="palettes13", type="boolean", nullable=true)
     */
    private $palettes13;
	
	/**
     * @var boolean $cartonneuf
     *
     * @ORM\Column(name="cartonneuf", type="boolean", nullable=true)
     */
    private $cartonneuf;
	
	/**
     * @var boolean $fournitadh
     *
     * @ORM\Column(name="fournitadh", type="boolean", nullable=true)
     */
    private $fournitadh;
	
	/**
     * @var integer $qttepal23
     *
     * @ORM\Column(name="qttepal23", type="integer", nullable=true)
     */
    private $qttepal23;
	
	/**
     * @var integer $qttepal13
     *
     * @ORM\Column(name="qttepal13", type="integer", nullable=true)
     */
    private $qttepal13;
	
	/**
     * @var integer $qttecarton
     *
     * @ORM\Column(name="qttecarton", type="integer", nullable=true)
     */
    private $qttecarton;
	
	
	/**
     * @var integer $qttefournadh
     *
     * @ORM\Column(name="qttefournadh", type="integer", nullable=true)
     */
    private $qttefournadh;
	
	/**
     * @var boolean $modifsouhait
     *
     * @ORM\Column(name="modifsouhait", type="boolean", nullable=true)
     */
    private $modifsouhait;
	
	/**
     * @var boolean $archive
     *
     * @ORM\Column(name="archive", type="boolean")
     */
    private $archive;
	
	/**
     * @var decimal $poids
     * @Assert\Type(type="float", message="Nombre décimal avec virgule: ,")
     * @ORM\Column(name="poids", type="decimal", length="10", scale="2", nullable=true)
     */
    private $poids;
	
	/**
     * @var decimal $volume
     * @Assert\Type(type="float", message="Nombre décimal avec virgule: ,")
     * @ORM\Column(name="volume", type="decimal", length="10", scale="2", nullable=true)
     */
    private $volume;
	
	/**
     * @var decimal $prepa
     * @Assert\Type(type="float", message="Nombre décimal avec virgule: ,")
     * @ORM\Column(name="prepa", type="decimal", length="10", scale="2", nullable=true)
     */
    private $prepa;
	
	/**
     * @var decimal $admin
     * @Assert\Type(type="float", message="Nombre décimal avec virgule: ,")
     * @ORM\Column(name="admin", type="decimal", length="10", scale="2", nullable=true)
     */
    private $admin;
	
	/**
     * @var decimal $pale
     * @Assert\Type(type="float", message="Nombre décimal avec virgule: ,")
     * @ORM\Column(name="pale", type="decimal", length="10", scale="2", nullable=true)
     */
    private $pale;
	
	/**
     * @var decimal $fourniture
     * @Assert\Type(type="float", message="Nombre décimal avec virgule: ,")
     * @ORM\Column(name="fourniture", type="decimal", length="10", scale="2", nullable=true)
     */
    private $fourniture;
	
	/**
     * @var decimal $transp
     * @Assert\Type(type="float", message="Nombre décimal avec virgule: ,")
     * @ORM\Column(name="transp", type="decimal", length="10", scale="2", nullable=true)
     */
    private $transp;
	
	/**
     * @var decimal $recupbon
     * @Assert\Type(type="float", message="Nombre décimal avec virgule: ,")
     * @ORM\Column(name="recupbon", type="decimal", length="10", scale="2", nullable=true)
     */
    private $recupbon;
	
	/**
     * @var boolean $prefact
     *
     * @ORM\Column(name="prefact", type="boolean", nullable=true)
     */
    private $prefact;

    /**
     * @var text $comm
     *
     * @ORM\Column(name="comm", type="text", nullable=true)
     */
    private $comm;
	
	/**
     * @var text $memoclient
     *
     * @ORM\Column(name="memoclient", type="text", nullable=true)
     */
    private $memoclient;

    /**
     * @var string $compdf
     * @Assert\File( maxSize = "4M", mimeTypes = {"application/pdf", "application/x-pdf", "application/vnd.ms-excel"}, mimeTypesMessage = "SVP Uploader un fichier PDF")
     * @ORM\Column(name="compdf", type="string", length=255, nullable=true)
     */
    private $compdf;
	
	/**
     * @var string $compdf2
     * @Assert\File( maxSize = "4M", mimeTypes = {"application/pdf", "application/x-pdf", "application/vnd.ms-excel"}, mimeTypesMessage = "SVP Uploader un fichier PDF")
     * @ORM\Column(name="compdf2", type="string", length=255, nullable=true)
     */
    private $compdf2;
	
	/**
     * @var string $compdf3
     * @Assert\File( maxSize = "4M", mimeTypes = {"application/pdf", "application/x-pdf", "application/vnd.ms-excel"}, mimeTypesMessage = "SVP Uploader un fichier PDF")
     * @ORM\Column(name="compdf3", type="string", length=255, nullable=true)
     */
    private $compdf3;
	
	/**
     * @var string $compdf4
     * @Assert\File( maxSize = "4M", mimeTypes = {"application/pdf", "application/x-pdf", "application/vnd.ms-excel"}, mimeTypesMessage = "SVP Uploader un fichier PDF")
     * @ORM\Column(name="compdf4", type="string", length=255, nullable=true)
     */
    private $compdf4;

    /**
     * @var string $client
     *
     * @ORM\Column(name="client", type="string", length=255, nullable=true)
     */
    private $client;

    /**
     * @var integer $idclient
     *
     * @ORM\Column(name="idclient", type="integer", nullable=true)
     */
    private $idclient;
	
	/**
     * @var string $etat
     *
     * @ORM\Column(name="etat", type="string", nullable=true)
     */
    private $etat;

	/**
     * @var datetime $datelivrsouh
     *
     * @ORM\Column(name="datelivrsouh", type="datetime", nullable=true)
     */
    private $datelivrsouh;
	
	/**
     * @var datetime $dateencoursprepa
     *
     * @ORM\Column(name="dateencoursprepa", type="datetime", nullable=true)
     */
    private $dateencoursprepa;
	
	/**
     * @var datetime $dateprepare
     *
     * @ORM\Column(name="dateprepare", type="datetime", nullable=true)
     */
    private $dateprepare;
	
	/**
     * @var datetime $datelivre
     *
     * @ORM\Column(name="datelivre", type="datetime", nullable=true)
     */
    private $datelivre;
	
	/**
     * @var boolean $livrimp
     *
     * @ORM\Column(name="livrimp", type="boolean", nullable=true)
     */
    private $livrimp;
	
	/**
     * @var boolean $datelivrend
     *
     * @ORM\Column(name="datelivrend", type="boolean", nullable=true)
     */
    private $datelivrend;
	
	/**
     * @var boolean $autoor
     *
     * @ORM\Column(name="autoor", type="boolean", nullable=true)
     */
    private $autoor;
	
	/**
     * @var datetime $dateenvoi
     *
     * @ORM\Column(name="dateenvoi", type="datetime", nullable=true)
     */
    private $dateenvoi;
	
	/**
     * @var datetime $dateintervprev
     *
     * @ORM\Column(name="dateintervprev", type="datetime", nullable=true)
     */
    private $dateintervprev;
	
	
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datecom
     *
     * @param datetime $datecom
     */
    public function setDatecom($datecom)
    {
        $this->datecom = $datecom;
    }

    /**
     * Get datecom
     *
     * @return datetime 
     */
    public function getDatecom()
    {
        return $this->datecom;
    }
	
	/**
     * Set numop
     *
     * @param string $numop
     */
    public function setNumop($numop)
    {
        $this->numop = $numop;
    }

    /**
     * Get numop
     *
     * @return string 
     */
    public function getNumop()
    {
        return $this->numop;
    }
	
	/**
     * Set nummag
     *
     * @param string $nummag
     */
    public function setNummag($nummag)
    {
        $this->nummag = $nummag;
    }

    /**
     * Get nummag
     *
     * @return string 
     */
    public function getNummag()
    {
        return $this->nummag;
    }
	
	/**
     * Set compte
     *
     * @param string $compte
     */
    public function setCompte($compte)
    {
        $this->compte = $compte;
    }

    /**
     * Get compte
     *
     * @return string 
     */
    public function getCompte()
    {
        return $this->compte;
    }
	
	/**
     * Set numcommaint
     *
     * @param string $numcommaint
     */
    public function setNumcommaint($numcommaint)
    {
        $this->numcommaint = $numcommaint;
    }

    /**
     * Get numcommaint
     *
     * @return string 
     */
    public function getNumcommaint()
    {
        return $this->numcommaint;
    }

    /**
     * Set adlivrid
     *
     * @param integer $adlivrid
     */
    public function setAdlivrid($adlivrid)
    {
        $this->adlivrid = $adlivrid;
    }

    /**
     * Get adlivrid
     *
     * @return integer 
     */
    public function getAdlivrid()
    {
        return $this->adlivrid;
    }
	
	/**
     * Set nomdusite
     *
     * @param string $nomdusite
     */
    public function setNomdusite($nomdusite)
    {
        $this->nomdusite = $nomdusite;
    }

    /**
     * Get nomdusite
     *
     * @return string 
     */
    public function getNomdusite()
    {
        return $this->nomdusite;
    }

    /**
     * Set install
     *
     * @param boolean $install
     */
    public function setInstall($install)
    {
        $this->install = $install;
    }

    /**
     * Get install
     *
     * @return boolean 
     */
    public function getInstall()
    {
        return $this->install;
    }
	
	/**
     * Set valid
     *
     * @param boolean $valid
     */
    public function setValid($valid)
    {
        $this->valid = $valid;
    }

    /**
     * Get valid
     *
     * @return boolean 
     */
    public function getValid()
    {
        return $this->valid;
    }
	
	/**
     * Set validor
     *
     * @param boolean $validor
     */
    public function setValidor($validor)
    {
        $this->validor = $validor;
    }

    /**
     * Get validor
     *
     * @return boolean 
     */
    public function getValidor()
    {
        return $this->validor;
    }
	
	/**
     * Set modifsouhait
     *
     * @param boolean $modifsouhait
     */
    public function setModifsouhait($modifsouhait)
    {
        $this->modifsouhait = $modifsouhait;
    }

    /**
     * Get modifsouhait
     *
     * @return boolean 
     */
    public function getModifsouhait()
    {
        return $this->modifsouhait;
    }
	
	/**
     * Set poids
     *
     * @param decimal $poids
     */
    public function setPoids($poids)
    {
        $this->poids = $poids;
    }

    /**
     * Get poids
     *
     * @return decimal 
     */
    public function getPoids()
    {
        return $this->poids;
    }
	
	/**
     * Set volume
     *
     * @param decimal $volume
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;
    }

    /**
     * Get volume
     *
     * @return decimal 
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * Set comm
     *
     * @param text $comm
     */
    public function setComm($comm)
    {
        $this->comm = $comm;
    }

    /**
     * Get comm
     *
     * @return text 
     */
    public function getComm()
    {
        return $this->comm;
    }
	
	/**
     * Set memoclient
     *
     * @param text $memoclient
     */
    public function setMemoclient($memoclient)
    {
        $this->memoclient = $memoclient;
    }

    /**
     * Get memoclient
     *
     * @return text 
     */
    public function getMemoclient()
    {
        return $this->memoclient;
    }

    /**
     * Set compdf
     *
     * @param string $compdf
     */
    public function setCompdf($compdf)
    {
        $this->compdf = $compdf;
    }

    /**
     * Get compdf
     *
     * @return string 
     */
    public function getCompdf()
    {
        return $this->compdf;
    }
	
	/**
     * Set compdf2
     *
     * @param string $compdf2
     */
    public function setCompdf2($compdf2)
    {
        $this->compdf2 = $compdf2;
    }

    /**
     * Get compdf2
     *
     * @return string 
     */
    public function getCompdf2()
    {
        return $this->compdf2;
    }
	
	/**
     * Set compdf3
     *
     * @param string $compdf3
     */
    public function setCompdf3($compdf3)
    {
        $this->compdf3 = $compdf3;
    }

    /**
     * Get compdf3
     *
     * @return string 
     */
    public function getCompdf3()
    {
        return $this->compdf3;
    }
	
	/**
     * Set compdf4
     *
     * @param string $compdf4
     */
    public function setCompdf4($compdf4)
    {
        $this->compdf4 = $compdf4;
    }

    /**
     * Get compdf4
     *
     * @return string 
     */
    public function getCompdf4()
    {
        return $this->compdf4;
    }

    /**
     * Set client
     *
     * @param string $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * Get client
     *
     * @return string 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set idclient
     *
     * @param integer $idclient
     */
    public function setIdclient($idclient)
    {
        $this->idclient = $idclient;
    }

    /**
     * Get idclient
     *
     * @return integer 
     */
    public function getIdclient()
    {
        return $this->idclient;
    }
	
	/**
     * Set etat
     *
     * @param string $etat
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;
    }

    /**
     * Get etat
     *
     * @return string 
     */
    public function getEtat()
    {
        return $this->etat;
    }
	
	/**
     * Set datelivrsouh
     *
     * @param datetime $datelivrsouh
     */
    public function setDatelivrsouh($datelivrsouh)
    {
        $this->datelivrsouh = $datelivrsouh;
    }

    /**
     * Get datelivrsouh
     *
     * @return datetime 
     */
    public function getDatelivrsouh()
    {
        return $this->datelivrsouh;
    }
	
	/**
     * Set dateencoursprepa
     *
     * @param datetime $dateencoursprepa
     */
    public function setDateencoursprepa($dateencoursprepa)
    {
        $this->dateencoursprepa = $dateencoursprepa;
    }

    /**
     * Get dateencoursprepa
     *
     * @return datetime 
     */
    public function getDateencoursprepa()
    {
        return $this->dateencoursprepa;
    }
	
	/**
     * Set dateprepare
     *
     * @param datetime $dateprepare
     */
    public function setDateprepare($dateprepare)
    {
        $this->dateprepare = $dateprepare;
    }

    /**
     * Get dateprepare
     *
     * @return datetime 
     */
    public function getDateprepare()
    {
        return $this->dateprepare;
    }
	
	/**
     * Set datelivre
     *
     * @param datetime $datelivre
     */
    public function setDatelivre($datelivre)
    {
        $this->datelivre = $datelivre;
    }

    /**
     * Get datelivre
     *
     * @return datetime 
     */
    public function getDatelivre()
    {
        return $this->datelivre;
    }
	
	/**
     * Set livrimp
     *
     * @param boolean $livrimp
     */
    public function setLivrimp($livrimp)
    {
        $this->livrimp = $livrimp;
    }

    /**
     * Get livrimp
     *
     * @return boolean 
     */
    public function getLivrimp()
    {
        return $this->livrimp;
    }
	
	/**
     * Set datelivrend
     *
     * @param boolean $datelivrend
     */
    public function setDatelivrend($datelivrend)
    {
        $this->datelivrend = $datelivrend;
    }

    /**
     * Get datelivrend
     *
     * @return boolean 
     */
    public function getDatelivrend()
    {
        return $this->datelivrend;
    }
	
	/**
     * Set autoor
     *
     * @param boolean $autoor
     */
    public function setAutoor($autoor)
    {
        $this->autoor = $autoor;
    }

    /**
     * Get autoor
     *
     * @return boolean 
     */
    public function getAutoor()
    {
        return $this->autoor;
    }
	
	/**
     * Set dateenvoi
     *
     * @param datetime $dateenvoi
     */
    public function setDateenvoi($dateenvoi)
    {
        $this->dateenvoi = $dateenvoi;
    }

    /**
     * Get dateenvoi
     *
     * @return datetime 
     */
    public function getDateenvoi()
    {
        return $this->dateenvoi;
    }
	
	/**
     * Set dateintervprev
     *
     * @param datetime $dateintervprev
     */
    public function setDateintervprev($dateintervprev)
    {
        $this->dateintervprev = $dateintervprev;
    }

    /**
     * Get dateintervprev
     *
     * @return datetime 
     */
    public function getDateintervprev()
    {
        return $this->dateintervprev;
    }
	
	public function __construct()
	{
		$this->datecom = new \DateTime('now');
	}
	
	public function getFullPdfPath() {
        return null === $this->compdf ? null : $this->getUploadRootDir(). $this->compdf;
    }
	
	public function getFullPdfPath2() {
        return null === $this->compdf2 ? null : $this->getUploadRootDir2(). $this->compdf2;
    }
	
	public function getFullPdfPath3() {
        return null === $this->compdf3 ? null : $this->getUploadRootDir3(). $this->compdf3;
    }
	
	public function getFullPdfPath4() {
        return null === $this->compdf4 ? null : $this->getUploadRootDir4(). $this->compdf4;
    }
 
    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return $this->getTmpUploadRootDir().$this->getId()."/";
    }
	
	protected function getUploadRootDir2() {
        // the absolute directory path where uploaded documents should be saved
        return $this->getTmpUploadRootDir2().$this->getId()."/";
    }
	
	protected function getUploadRootDir3() {
        // the absolute directory path where uploaded documents should be saved
        return $this->getTmpUploadRootDir3().$this->getId()."/";
    }
	
	protected function getUploadRootDir4() {
        // the absolute directory path where uploaded documents should be saved
        return $this->getTmpUploadRootDir4().$this->getId()."/";
    }
 
    protected function getTmpUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/upload/toolbox/op/client/';
    }
	
	protected function getTmpUploadRootDir2() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/upload/toolbox/op/clientfile2/';
    }
	
	protected function getTmpUploadRootDir3() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/upload/toolbox/op/clientfile3/';
    }
	
	protected function getTmpUploadRootDir4() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/upload/toolbox/op/clientfile4/';
    }
 
    /**
     * @ORM\PrePersist()
     * 
     */
    public function uploadPdf() {
        // the file property can be empty if the field is not required
        if (null === $this->compdf) {
            return;
        }
        if(!$this->id){
            $this->compdf->move($this->getTmpUploadRootDir(), $this->compdf->getClientOriginalName());
        }else{
            $this->compdf->move($this->getUploadRootDir(), $this->compdf->getClientOriginalName());
        }
        $this->setCompdf($this->compdf->getClientOriginalName());
    }
 
    /**
     * @ORM\PostPersist()
     */
    public function movePdf()
    {
        if (null === $this->compdf) {
            return;
        }
        if(!is_dir($this->getUploadRootDir())){
            mkdir($this->getUploadRootDir());
        }
        copy($this->getTmpUploadRootDir().$this->compdf, $this->getFullPdfPath());
        unlink($this->getTmpUploadRootDir().$this->compdf);
    }
	
	/**
     * @ORM\PrePersist()
     * 
     */
    public function uploadPdf2() {
        // the file property can be empty if the field is not required
        if (null === $this->compdf2) {
            return;
        }
        if(!$this->id){
            $this->compdf2->move($this->getTmpUploadRootDir2(), $this->compdf2->getClientOriginalName());
        }else{
            $this->compdf2->move($this->getUploadRootDir2(), $this->compdf2->getClientOriginalName());
        }
        $this->setCompdf2($this->compdf2->getClientOriginalName());
    }
 
    /**
     * @ORM\PostPersist()
     */
    public function movePdf2()
    {
        if (null === $this->compdf2) {
            return;
        }
        if(!is_dir($this->getUploadRootDir2())){
            mkdir($this->getUploadRootDir2());
        }
        copy($this->getTmpUploadRootDir2().$this->compdf2, $this->getFullPdfPath2());
        unlink($this->getTmpUploadRootDir2().$this->compdf2);
    }
	
	/**
     * @ORM\PrePersist()
     * 
     */
    public function uploadPdf3() {
        // the file property can be empty if the field is not required
        if (null === $this->compdf3) {
            return;
        }
        if(!$this->id){
            $this->compdf3->move($this->getTmpUploadRootDir3(), $this->compdf3->getClientOriginalName());
        }else{
            $this->compdf3->move($this->getUploadRootDir3(), $this->compdf3->getClientOriginalName());
        }
        $this->setCompdf3($this->compdf3->getClientOriginalName());
    }
 
    /**
     * @ORM\PostPersist()
     */
    public function movePdf3()
    {
        if (null === $this->compdf3) {
            return;
        }
        if(!is_dir($this->getUploadRootDir3())){
            mkdir($this->getUploadRootDir3());
        }
        copy($this->getTmpUploadRootDir3().$this->compdf3, $this->getFullPdfPath3());
        unlink($this->getTmpUploadRootDir3().$this->compdf3);
    }
	
	/**
     * @ORM\PrePersist()
     * 
     */
    public function uploadPdf4() {
        // the file property can be empty if the field is not required
        if (null === $this->compdf4) {
            return;
        }
        if(!$this->id){
            $this->compdf4->move($this->getTmpUploadRootDir4(), $this->compdf4->getClientOriginalName());
        }else{
            $this->compdf4->move($this->getUploadRootDir4(), $this->compdf4->getClientOriginalName());
        }
        $this->setCompdf4($this->compdf4->getClientOriginalName());
    }
 
    /**
     * @ORM\PostPersist()
     */
    public function movePdf4()
    {
        if (null === $this->compdf4) {
            return;
        }
        if(!is_dir($this->getUploadRootDir4())){
            mkdir($this->getUploadRootDir4());
        }
        copy($this->getTmpUploadRootDir4().$this->compdf4, $this->getFullPdfPath4());
        unlink($this->getTmpUploadRootDir4().$this->compdf4);
    }
 
    
	
	

    /**
     * Set archive
     *
     * @param boolean $archive
     */
    public function setArchive($archive)
    {
        $this->archive = $archive;
    }

    /**
     * Get archive
     *
     * @return boolean 
     */
    public function getArchive()
    {
        return $this->archive;
    }

    /**
     * Set prepa
     *
     * @param decimal $prepa
     */
    public function setPrepa($prepa)
    {
        $this->prepa = $prepa;
    }

    /**
     * Get prepa
     *
     * @return decimal 
     */
    public function getPrepa()
    {
        return $this->prepa;
    }

    /**
     * Set admin
     *
     * @param decimal $admin
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;
    }

    /**
     * Get admin
     *
     * @return decimal 
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * Set pale
     *
     * @param decimal $pale
     */
    public function setPale($pale)
    {
        $this->pale = $pale;
    }

    /**
     * Get pale
     *
     * @return decimal 
     */
    public function getPale()
    {
        return $this->pale;
    }

    /**
     * Set fourniture
     *
     * @param decimal $fourniture
     */
    public function setFourniture($fourniture)
    {
        $this->fourniture = $fourniture;
    }

    /**
     * Get fourniture
     *
     * @return decimal 
     */
    public function getFourniture()
    {
        return $this->fourniture;
    }

    /**
     * Set transp
     *
     * @param decimal $transp
     */
    public function setTransp($transp)
    {
        $this->transp = $transp;
    }

    /**
     * Get transp
     *
     * @return decimal 
     */
    public function getTransp()
    {
        return $this->transp;
    }

    /**
     * Set recupbon
     *
     * @param decimal $recupbon
     */
    public function setRecupbon($recupbon)
    {
        $this->recupbon = $recupbon;
    }

    /**
     * Get recupbon
     *
     * @return decimal 
     */
    public function getRecupbon()
    {
        return $this->recupbon;
    }

    /**
     * Set prefact
     *
     * @param boolean $prefact
     */
    public function setPrefact($prefact)
    {
        $this->prefact = $prefact;
    }

    /**
     * Get prefact
     *
     * @return boolean 
     */
    public function getPrefact()
    {
        return $this->prefact;
    }

    /**
     * Set palettes23
     *
     * @param boolean $palettes23
     */
    public function setPalettes23($palettes23)
    {
        $this->palettes23 = $palettes23;
    }

    /**
     * Get palettes23
     *
     * @return boolean 
     */
    public function getPalettes23()
    {
        return $this->palettes23;
    }
	
	/**
     * Set palettes13
     *
     * @param boolean $palettes13
     */
    public function setPalettes13($palettes13)
    {
        $this->palettes13 = $palettes13;
    }

    /**
     * Get palettes13
     *
     * @return boolean 
     */
    public function getPalettes13()
    {
        return $this->palettes13;
    }
	
	/**
     * Set cartonneuf
     *
     * @param boolean $cartonneuf
     */
    public function setCartonneuf($cartonneuf)
    {
        $this->cartonneuf = $cartonneuf;
    }

    /**
     * Get cartonneuf
     *
     * @return boolean 
     */
    public function getCartonneuf()
    {
        return $this->cartonneuf;
    }

    /**
     * Set fournitadh
     *
     * @param boolean $fournitadh
     */
    public function setFournitadh($fournitadh)
    {
        $this->fournitadh = $fournitadh;
    }

    /**
     * Get fournitadh
     *
     * @return boolean 
     */
    public function getFournitadh()
    {
        return $this->fournitadh;
    }
	
	/**
     * Set qttecarton
     *
     * @param integer $qttecarton
     */
    public function setQttecarton($qttecarton)
    {
        $this->qttecarton = $qttecarton;
    }

    /**
     * Get qttecarton
     *
     * @return integer 
     */
    public function getQttecarton()
    {
        return $this->qttecarton;
    }

    /**
     * Set qttefournadh
     *
     * @param integer $qttefournadh
     */
    public function setQttefournadh($qttefournadh)
    {
        $this->qttefournadh = $qttefournadh;
    }

    /**
     * Get qttefournadh
     *
     * @return integer 
     */
    public function getQttefournadh()
    {
        return $this->qttefournadh;
    }
	
	/**
     * Set qttepal23
     *
     * @param integer $qttepal23
     */
    public function setQttepal23($qttepal23)
    {
        $this->qttepal23 = $qttepal23;
    }

    /**
     * Get qttepal23
     *
     * @return integer 
     */
    public function getQttepal23()
    {
        return $this->qttepal23;
    }
	
	/**
     * Set qttepal13
     *
     * @param integer $qttepal13
     */
    public function setQttepal13($qttepal13)
    {
        $this->qttepal13 = $qttepal13;
    }

    /**
     * Get qttepal13
     *
     * @return integer 
     */
    public function getQttepal13()
    {
        return $this->qttepal13;
    }
}