<?php

namespace Phareos\LogisToolBoxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\LogisToolBoxBundle\Entity\com_articles
 *
 * @ORM\Table(name="too_com_articles")
 * @ORM\Entity(repositoryClass="Phareos\LogisToolBoxBundle\Entity\com_articlesRepository")
 */
class com_articles
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer $idcomclients
     *
     * @ORM\Column(name="idcomclients", type="integer")
     */
    private $idcomclients;

    /**
     * @var integer $idarticles
     *
     * @ORM\Column(name="idarticles", type="integer")
     */
    private $idarticles;

    /**
     * @var integer $qtte
     *
     * @ORM\Column(name="qtte", type="integer")
     */
    private $qtte;
	
	/**
     * @var integer $test
     *
     * @ORM\Column(name="test", type="integer", nullable=true)
     */
    private $test;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idcomclients
     *
     * @param integer $idcomclients
     */
    public function setIdcomclients($idcomclients)
    {
        $this->idcomclients = $idcomclients;
    }

    /**
     * Get idcomclients
     *
     * @return integer 
     */
    public function getIdcomclients()
    {
        return $this->idcomclients;
    }

    /**
     * Set idarticles
     *
     * @param integer $idarticles
     */
    public function setIdarticles($idarticles)
    {
        $this->idarticles = $idarticles;
    }

    /**
     * Get idarticles
     *
     * @return integer 
     */
    public function getIdarticles()
    {
        return $this->idarticles;
    }

    /**
     * Set qtte
     *
     * @param integer $qtte
     */
    public function setQtte($qtte)
    {
        $this->qtte = $qtte;
    }

    /**
     * Get qtte
     *
     * @return integer 
     */
    public function getQtte()
    {
        return $this->qtte;
    }
	
	/**
     * Set test
     *
     * @param integer $test
     */
    public function setTest($test)
    {
        $this->test = $test;
    }

    /**
     * Get test
     *
     * @return integer 
     */
    public function getTest()
    {
        return $this->test;
    }
}