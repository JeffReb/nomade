<?php

namespace Phareos\LogisToolBoxBundle\Entity;

use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityRepository;


/**
 * articlesRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class articlesRepository extends EntityRepository
{
	public function myFindAll()
	{
		$session = $this->get('session');
		$societeUSER = $session->get('societeUSER');
		
		$queryBuilder = $this->createQueryBuilder('a');
        $queryBuilder->where('a.client = :societeUSER')
		->setParameter('societeUSER', $societeUSER)
		->orderBy('a.designat', 'ASC');
		return $queryBuilder;
	//	
	//	$query = $this->_em->createQuery('SELECT id FROM PhareosLogisToolBoxBundle:articles id');
    //    $resultats = $query->getResult();
	//			 
	//	return $resultats;
	}
}