<?php

namespace Phareos\LogisToolBoxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\LogisToolBoxBundle\Entity\ordrecstock
 *
 * @ORM\Table(name="too_ordrecstock")
 * @ORM\Entity(repositoryClass="Phareos\LogisToolBoxBundle\Entity\ordrecstockRepository")
 */
class ordrecstock
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\ManyToOne(targetEntity="commanderecip", inversedBy="commordrecstock", cascade={"remove"})
	 * @ORM\JoinColumn(name="commanderecip_id", referencedColumnName="id")
	 */
	protected $commanderecip;
	
	/**
	 * @ORM\ManyToOne(targetEntity="articles", inversedBy="articordrecstock", cascade={"remove"})
	 * @ORM\JoinColumn(name="articles_id", referencedColumnName="id")
	 */
	protected $articles;
	
	/**
	 * @ORM\OneToMany(targetEntity="emplarticle", mappedBy="ordrecstock", cascade={"remove", "persist"})
	 */
	protected $ordrecemplarticle;
	

    /**
     * @var string $numors
     *
     * @ORM\Column(name="numors", type="string", length=255)
     */
    private $numors;

    /**
     * @var datetime $daterecep
     *
     * @ORM\Column(name="daterecep", type="datetime")
     */
    private $daterecep;

    /**
     * @var datetime $datestock
     *
     * @ORM\Column(name="datestock", type="datetime", nullable=true)
     */
    private $datestock;

    /**
     * @var boolean $stock
     *
     * @ORM\Column(name="stock", type="boolean", nullable=true)
     */
    private $stock;

    /**
     * @var integer $qteastocker
     *
     * @ORM\Column(name="qteastocker", type="integer", nullable=true)
     */
    private $qteastocker;
	
	
	/**
     * @var integer $qtestockee
     *
     * @ORM\Column(name="qtestockee", type="integer", nullable=true)
     */
    private $qtestockee;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numors
     *
     * @param string $numors
     */
    public function setNumors($numors)
    {
        $this->numors = $numors;
    }

    /**
     * Get numors
     *
     * @return string 
     */
    public function getNumors()
    {
        return $this->numors;
    }

    /**
     * Set daterecep
     *
     * @param datetime $daterecep
     */
    public function setDaterecep($daterecep)
    {
        $this->daterecep = $daterecep;
    }

    /**
     * Get daterecep
     *
     * @return datetime 
     */
    public function getDaterecep()
    {
        return $this->daterecep;
    }

    /**
     * Set datestock
     *
     * @param datetime $datestock
     */
    public function setDatestock($datestock)
    {
        $this->datestock = $datestock;
    }

    /**
     * Get datestock
     *
     * @return datetime 
     */
    public function getDatestock()
    {
        return $this->datestock;
    }

    /**
     * Set stock
     *
     * @param boolean $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * Get stock
     *
     * @return boolean 
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set qteastocker
     *
     * @param integer $qteastocker
     */
    public function setQteastocker($qteastocker)
    {
        $this->qteastocker = $qteastocker;
    }

    /**
     * Get qteastocker
     *
     * @return integer 
     */
    public function getQteastocker()
    {
        return $this->qteastocker;
    }
	
	/**
     * Set qtestockee
     *
     * @param integer $qtestockee
     */
    public function setQtestockee($qtestockee)
    {
        $this->qtestockee = $qtestockee;
    }

    /**
     * Get qtestockee
     *
     * @return integer 
     */
    public function getQtestockee()
    {
        return $this->qtestockee;
    }

    /**
     * Set commanderecip
     *
     * @param Phareos\LogisToolBoxBundle\Entity\commanderecip $commanderecip
     */
    public function setCommanderecip(\Phareos\LogisToolBoxBundle\Entity\commanderecip $commanderecip)
    {
        $this->commanderecip = $commanderecip;
    }

    /**
     * Get commanderecip
     *
     * @return Phareos\LogisToolBoxBundle\Entity\commanderecip 
     */
    public function getCommanderecip()
    {
        return $this->commanderecip;
    }

    

    /**
     * Set articles
     *
     * @param Phareos\LogisToolBoxBundle\Entity\articles $articles
     */
    public function setArticles(\Phareos\LogisToolBoxBundle\Entity\articles $articles)
    {
        $this->articles = $articles;
    }

    /**
     * Get articles
     *
     * @return Phareos\LogisToolBoxBundle\Entity\articles 
     */
    public function getArticles()
    {
        return $this->articles;
    }
    public function __construct()
    {
        $this->ordrecemplarticle = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add ordrecemplarticle
     *
     * @param Phareos\LogisToolBoxBundle\Entity\emplarticle $ordrecemplarticle
     */
    public function addemplarticle(\Phareos\LogisToolBoxBundle\Entity\emplarticle $ordrecemplarticle)
    {
        $this->ordrecemplarticle[] = $ordrecemplarticle;
    }

    /**
     * Get ordrecemplarticle
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getOrdrecemplarticle()
    {
        return $this->ordrecemplarticle;
    }
}