<?php

namespace Phareos\LogisToolBoxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\LogisToolBoxBundle\Entity\stockcom
 *
 * @ORM\Table(name="too_stockcom")
 * @ORM\Entity(repositoryClass="Phareos\LogisToolBoxBundle\Entity\stockcomRepository")
 */
class stockcom
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var text $com
     *
     * @ORM\Column(name="com", type="text")
     */
    private $com;

	/**
     * @var date $date
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;
	
	/**
     * @var string $auteur
     *
     * @ORM\Column(name="auteur", type="string", length=255)
     */
    private $auteur;
	
	public function __construct()
	{
		$this->date = new \DateTime('now');
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set com
     *
     * @param text $com
     */
    public function setCom($com)
    {
        $this->com = $com;
    }

    /**
     * Get com
     *
     * @return text 
     */
    public function getCom()
    {
        return $this->com;
    }
	
	/**
     * Set date
     *
     * @param date $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Get date
     *
     * @return date 
     */
    public function getDate()
    {
        return $this->date;
    }
	
	/**
     * Set auteur
     *
     * @param string $auteur
     */
    public function setAuteur($auteur)
    {
        $this->auteur = $auteur;
    }

    /**
     * Get auteur
     *
     * @return string 
     */
    public function getAuteur()
    {
        return $this->auteur;
    }
}