<?php

namespace Phareos\LogisToolBoxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\LogisToolBoxBundle\Entity\emplacement
 *
 * @ORM\Table(name="too_emplacement")
 * @ORM\Entity(repositoryClass="Phareos\LogisToolBoxBundle\Entity\emplacementRepository")
 */
class emplacement
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\OneToMany(targetEntity="emplarticle", mappedBy="emplacement", cascade={"remove", "persist"})
	 */
	protected $emplemplarticle;

    /**
     * @var string $emplacement
     *
     * @ORM\Column(name="emplacement", type="string", length=255)
     */
    private $emplacement;
	
	/**
     * @var string $alletrav
     *
     * @ORM\Column(name="alletrav", type="string", length=255, nullable=true)
     */
    private $alletrav;
	
	/**
     * @var boolean $actif
     *
     * @ORM\Column(name="actif", type="boolean", nullable=true)
     */
    private $actif;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set emplacement
     *
     * @param string $emplacement
     */
    public function setEmplacement($emplacement)
    {
        $this->emplacement = $emplacement;
    }

    /**
     * Get emplacement
     *
     * @return string 
     */
    public function getEmplacement()
    {
        return $this->emplacement;
    }
	
	/**
     * Set alletrav
     *
     * @param string $alletrav
     */
    public function setAlletrav($alletrav)
    {
        $this->alletrav = $alletrav;
    }

    /**
     * Get alletrav
     *
     * @return string 
     */
    public function getAlletrav()
    {
        return $this->alletrav;
    }
	
    public function __construct()
    {
        $this->emplemplarticle = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add emplemplarticle
     *
     * @param Phareos\LogisToolBoxBundle\Entity\emplarticle $emplemplarticle
     */
    public function addemplarticle(\Phareos\LogisToolBoxBundle\Entity\emplarticle $emplemplarticle)
    {
        $this->emplemplarticle[] = $emplemplarticle;
    }

    /**
     * Get emplemplarticle
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getEmplemplarticle()
    {
        return $this->emplemplarticle;
    }
	
	/**
     * Set actif
     *
     * @param boolean $actif
     */
    public function setActif($actif)
    {
        $this->actif = $actif;
    }

    /**
     * Get actif
     *
     * @return boolean 
     */
    public function getActif()
    {
        return $this->actif;
    }
}