<?php

namespace Phareos\PointageServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\PointageServiceBundle\Entity\lieuxmagasin
 *
 * @ORM\Table(name="poi_lieuxmagasin")
 * @ORM\Entity(repositoryClass="Phareos\PointageServiceBundle\Entity\lieuxmagasinRepository")
 */
class lieuxmagasin
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer $id_gc
     *
     * @ORM\Column(name="id_gc", type="integer")
     */
    private $id_gc;

    /**
     * @var string $matricule_gc
     *
     * @ORM\Column(name="matricule_gc", type="string", length=255)
     */
    private $matricule_gc;

    /**
     * @var string $codag
     *
     * @ORM\Column(name="codag", type="string", length=255, nullable=true)
     */
    private $codag;

    /**
     * @var string $adres1mag
     *
     * @ORM\Column(name="adres1mag", type="string", length=255, nullable=true)
     */
    private $adres1mag;

    /**
     * @var string $adres2mag
     *
     * @ORM\Column(name="adres2mag", type="string", length=255, nullable=true)
     */
    private $adres2mag;

    /**
     * @var string $cpmag
     *
     * @ORM\Column(name="cpmag", type="string", length=255, nullable=true)
     */
    private $cpmag;

    /**
     * @var string $villemag
     *
     * @ORM\Column(name="villemag", type="string", length=255, nullable=true)
     */
    private $villemag;

    /**
     * @var string $emailmag
     *
     * @ORM\Column(name="emailmag", type="string", length=255, nullable=true)
     */
    private $emailmag;

    /**
     * @var string $faxmag
     *
     * @ORM\Column(name="faxmag", type="string", length=255, nullable=true)
     */
    private $faxmag;

    /**
     * @var string $telmag
     *
     * @ORM\Column(name="telmag", type="string", length=255, nullable=true)
     */
    private $telmag;

    /**
     * @var string $tel2mag
     *
     * @ORM\Column(name="tel2mag", type="string", length=255, nullable=true)
     */
    private $tel2mag;

    /**
     * @var string $tel3mag
     *
     * @ORM\Column(name="tel3mag", type="string", length=255, nullable=true)
     */
    private $tel3mag;

    /**
     * @var string $tel4
     *
     * @ORM\Column(name="tel4", type="string", length=255, nullable=true)
     */
    private $tel4;

    /**
     * @var text $commentaires
     *
     * @ORM\Column(name="commentaires", type="text", nullable=true)
     */
    private $commentaires;

    /**
     * @var integer $agent_1_id
     *
     * @ORM\Column(name="agent_1_id", type="integer", nullable=true)
     */
    private $agent_1_id;

    /**
     * @var integer $agent_2_id
     *
     * @ORM\Column(name="agent_2_id", type="integer", nullable=true)
     */
    private $agent_2_id;

    /**
     * @var integer $agent_3_id
     *
     * @ORM\Column(name="agent_3_id", type="integer", nullable=true)
     */
    private $agent_3_id;

    /**
     * @var integer $agent_4_id
     *
     * @ORM\Column(name="agent_4_id", type="integer", nullable=true)
     */
    private $agent_4_id;

    /**
     * @var integer $agent_5_id
     *
     * @ORM\Column(name="agent_5_id", type="integer", nullable=true)
     */
    private $agent_5_id;

    /**
     * @var integer $agent_6_id
     *
     * @ORM\Column(name="agent_6_id", type="integer", nullable=true)
     */
    private $agent_6_id;

    /**
     * @var integer $agent_7_id
     *
     * @ORM\Column(name="agent_7_id", type="integer", nullable=true)
     */
    private $agent_7_id;

    /**
     * @var integer $agent_8_id
     *
     * @ORM\Column(name="agent_8_id", type="integer", nullable=true)
     */
    private $agent_8_id;

    /**
     * @var integer $agent_9_id
     *
     * @ORM\Column(name="agent_9_id", type="integer", nullable=true)
     */
    private $agent_9_id;

    /**
     * @var integer $agent_10_id
     *
     * @ORM\Column(name="agent_10_id", type="integer", nullable=true)
     */
    private $agent_10_id;

    /**
     * @var string $societeuser
     *
     * @ORM\Column(name="societeuser", type="string", length=255, nullable=true)
     */
    private $societeuser;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id_gc
     *
     * @param integer $idGc
     */
    public function setIdGc($idGc)
    {
        $this->id_gc = $idGc;
    }

    /**
     * Get id_gc
     *
     * @return integer 
     */
    public function getIdGc()
    {
        return $this->id_gc;
    }

    /**
     * Set matricule_gc
     *
     * @param string $matriculeGc
     */
    public function setMatriculeGc($matriculeGc)
    {
        $this->matricule_gc = $matriculeGc;
    }

    /**
     * Get matricule_gc
     *
     * @return string 
     */
    public function getMatriculeGc()
    {
        return $this->matricule_gc;
    }

    /**
     * Set codag
     *
     * @param string $codag
     */
    public function setCodag($codag)
    {
        $this->codag = $codag;
    }

    /**
     * Get codag
     *
     * @return string 
     */
    public function getCodag()
    {
        return $this->codag;
    }

    /**
     * Set adres1mag
     *
     * @param string $adres1mag
     */
    public function setAdres1mag($adres1mag)
    {
        $this->adres1mag = $adres1mag;
    }

    /**
     * Get adres1mag
     *
     * @return string 
     */
    public function getAdres1mag()
    {
        return $this->adres1mag;
    }

    /**
     * Set adres2mag
     *
     * @param string $adres2mag
     */
    public function setAdres2mag($adres2mag)
    {
        $this->adres2mag = $adres2mag;
    }

    /**
     * Get adres2mag
     *
     * @return string 
     */
    public function getAdres2mag()
    {
        return $this->adres2mag;
    }

    /**
     * Set cpmag
     *
     * @param string $cpmag
     */
    public function setCpmag($cpmag)
    {
        $this->cpmag = $cpmag;
    }

    /**
     * Get cpmag
     *
     * @return string 
     */
    public function getCpmag()
    {
        return $this->cpmag;
    }

    /**
     * Set villemag
     *
     * @param string $villemag
     */
    public function setVillemag($villemag)
    {
        $this->villemag = $villemag;
    }

    /**
     * Get villemag
     *
     * @return string 
     */
    public function getVillemag()
    {
        return $this->villemag;
    }

    /**
     * Set emailmag
     *
     * @param string $emailmag
     */
    public function setEmailmag($emailmag)
    {
        $this->emailmag = $emailmag;
    }

    /**
     * Get emailmag
     *
     * @return string 
     */
    public function getEmailmag()
    {
        return $this->emailmag;
    }

    /**
     * Set faxmag
     *
     * @param string $faxmag
     */
    public function setFaxmag($faxmag)
    {
        $this->faxmag = $faxmag;
    }

    /**
     * Get faxmag
     *
     * @return string 
     */
    public function getFaxmag()
    {
        return $this->faxmag;
    }

    /**
     * Set telmag
     *
     * @param string $telmag
     */
    public function setTelmag($telmag)
    {
        $this->telmag = $telmag;
    }

    /**
     * Get telmag
     *
     * @return string 
     */
    public function getTelmag()
    {
        return $this->telmag;
    }

    /**
     * Set tel2mag
     *
     * @param string $tel2mag
     */
    public function setTel2mag($tel2mag)
    {
        $this->tel2mag = $tel2mag;
    }

    /**
     * Get tel2mag
     *
     * @return string 
     */
    public function getTel2mag()
    {
        return $this->tel2mag;
    }

    /**
     * Set tel3mag
     *
     * @param string $tel3mag
     */
    public function setTel3mag($tel3mag)
    {
        $this->tel3mag = $tel3mag;
    }

    /**
     * Get tel3mag
     *
     * @return string 
     */
    public function getTel3mag()
    {
        return $this->tel3mag;
    }

    /**
     * Set tel4
     *
     * @param string $tel4
     */
    public function setTel4($tel4)
    {
        $this->tel4 = $tel4;
    }

    /**
     * Get tel4
     *
     * @return string 
     */
    public function getTel4()
    {
        return $this->tel4;
    }

    /**
     * Set commentaires
     *
     * @param text $commentaires
     */
    public function setCommentaires($commentaires)
    {
        $this->commentaires = $commentaires;
    }

    /**
     * Get commentaires
     *
     * @return text 
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }

    /**
     * Set agent_1_id
     *
     * @param integer $agent1Id
     */
    public function setAgent1Id($agent1Id)
    {
        $this->agent_1_id = $agent1Id;
    }

    /**
     * Get agent_1_id
     *
     * @return integer 
     */
    public function getAgent1Id()
    {
        return $this->agent_1_id;
    }

    /**
     * Set agent_2_id
     *
     * @param integer $agent2Id
     */
    public function setAgent2Id($agent2Id)
    {
        $this->agent_2_id = $agent2Id;
    }

    /**
     * Get agent_2_id
     *
     * @return integer 
     */
    public function getAgent2Id()
    {
        return $this->agent_2_id;
    }

    /**
     * Set agent_3_id
     *
     * @param integer $agent3Id
     */
    public function setAgent3Id($agent3Id)
    {
        $this->agent_3_id = $agent3Id;
    }

    /**
     * Get agent_3_id
     *
     * @return integer 
     */
    public function getAgent3Id()
    {
        return $this->agent_3_id;
    }

    /**
     * Set agent_4_id
     *
     * @param integer $agent4Id
     */
    public function setAgent4Id($agent4Id)
    {
        $this->agent_4_id = $agent4Id;
    }

    /**
     * Get agent_4_id
     *
     * @return integer 
     */
    public function getAgent4Id()
    {
        return $this->agent_4_id;
    }

    /**
     * Set agent_5_id
     *
     * @param integer $agent5Id
     */
    public function setAgent5Id($agent5Id)
    {
        $this->agent_5_id = $agent5Id;
    }

    /**
     * Get agent_5_id
     *
     * @return integer 
     */
    public function getAgent5Id()
    {
        return $this->agent_5_id;
    }

    /**
     * Set agent_6_id
     *
     * @param integer $agent6Id
     */
    public function setAgent6Id($agent6Id)
    {
        $this->agent_6_id = $agent6Id;
    }

    /**
     * Get agent_6_id
     *
     * @return integer 
     */
    public function getAgent6Id()
    {
        return $this->agent_6_id;
    }

    /**
     * Set agent_7_id
     *
     * @param integer $agent7Id
     */
    public function setAgent7Id($agent7Id)
    {
        $this->agent_7_id = $agent7Id;
    }

    /**
     * Get agent_7_id
     *
     * @return integer 
     */
    public function getAgent7Id()
    {
        return $this->agent_7_id;
    }

    /**
     * Set agent_8_id
     *
     * @param integer $agent8Id
     */
    public function setAgent8Id($agent8Id)
    {
        $this->agent_8_id = $agent8Id;
    }

    /**
     * Get agent_8_id
     *
     * @return integer 
     */
    public function getAgent8Id()
    {
        return $this->agent_8_id;
    }

    /**
     * Set agent_9_id
     *
     * @param integer $agent9Id
     */
    public function setAgent9Id($agent9Id)
    {
        $this->agent_9_id = $agent9Id;
    }

    /**
     * Get agent_9_id
     *
     * @return integer 
     */
    public function getAgent9Id()
    {
        return $this->agent_9_id;
    }

    /**
     * Set agent_10_id
     *
     * @param integer $agent10Id
     */
    public function setAgent10Id($agent10Id)
    {
        $this->agent_10_id = $agent10Id;
    }

    /**
     * Get agent_10_id
     *
     * @return integer 
     */
    public function getAgent10Id()
    {
        return $this->agent_10_id;
    }

    /**
     * Set societeuser
     *
     * @param string $societeuser
     */
    public function setSocieteuser($societeuser)
    {
        $this->societeuser = $societeuser;
    }

    /**
     * Get societeruser
     *
     * @return string 
     */
    public function getSocieteuser()
    {
        return $this->societeuser;
    }
}