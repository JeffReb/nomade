<?php

namespace Phareos\PointageServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\PointageServiceBundle\Entity\agents
 *
 * @ORM\Table(name="poi_agents")
 * @ORM\Entity(repositoryClass="Phareos\PointageServiceBundle\Entity\agentsRepository")
 */
class agents
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bigint $num_pers
     *
     * @ORM\Column(name="num_pers", type="bigint", nullable=true)
     */
    private $num_pers;

    /**
     * @var string $nom_pers
     *
     * @ORM\Column(name="nom_pers", type="string", length=255, nullable=true)
     */
    private $nom_pers;

    /**
     * @var string $prenom_pers
     *
     * @ORM\Column(name="prenom_pers", type="string", length=255, nullable=true)
     */
    private $prenom_pers;

    /**
     * @var string $sexe
     *
     * @ORM\Column(name="sexe", type="string", length=1, nullable=true)
     */
    private $sexe;

    /**
     * @var string $num_ss
     *
     * @ORM\Column(name="num_ss", type="string", length=255, nullable=true)
     */
    private $num_ss;

    /**
     * @var string $nationalite
     *
     * @ORM\Column(name="nationalite", type="string", length=255, nullable=true)
     */
    private $nationalite;

    /**
     * @var string $num_cs
     *
     * @ORM\Column(name="num_cs", type="string", length=255, nullable=true)
     */
    private $num_cs;

    /**
     * @var date $validite_cs
     *
     * @ORM\Column(name="validite_cs", type="date", nullable=true)
     */
    private $validite_cs;

    /**
     * @var string $vehicule
     *
     * @ORM\Column(name="vehicule", type="string", nullable=true)
     */
    private $vehicule;

    /**
     * @var string $permis
     *
     * @ORM\Column(name="permis", type="string", nullable=true)
     */
    private $permis;

    /**
     * @var string $telfix_pers
     *
     * @ORM\Column(name="telfix_pers", type="string", length=255, nullable=true)
     */
    private $telfix_pers;

    /**
     * @var string $telport_pers
     *
     * @ORM\Column(name="telport_pers", type="string", length=255, nullable=true)
     */
    private $telport_pers;

    /**
     * @var string $email_pers
     *
     * @ORM\Column(name="email_pers", type="string", length=255, nullable=true)
     */
    private $email_pers;

    /**
     * @var string $adres1
     *
     * @ORM\Column(name="adres1", type="string", length=255, nullable=true)
     */
    private $adres1;

    /**
     * @var string $adres2
     *
     * @ORM\Column(name="adres2", type="string", length=255, nullable=true)
     */
    private $adres2;

    /**
     * @var string $ville
     *
     * @ORM\Column(name="ville", type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @var string $cp
     *
     * @ORM\Column(name="cp", type="string", length=255, nullable=true)
     */
    private $cp;

    /**
     * @var text $commentaires
     *
     * @ORM\Column(name="commentaires", type="text", nullable=true)
     */
    private $commentaires;

    /**
     * @var string $societeuser
     *
     * @ORM\Column(name="societeuser", type="string", length=255, nullable=true)
     */
    private $societeuser;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set num_pers
     *
     * @param bigint $numPers
     */
    public function setNumPers($numPers)
    {
        $this->num_pers = $numPers;
    }

    /**
     * Get num_pers
     *
     * @return bigint 
     */
    public function getNumPers()
    {
        return $this->num_pers;
    }

    /**
     * Set nom_pers
     *
     * @param string $nomPers
     */
    public function setNomPers($nomPers)
    {
        $this->nom_pers = $nomPers;
    }

    /**
     * Get nom_pers
     *
     * @return string 
     */
    public function getNomPers()
    {
        return $this->nom_pers;
    }

    /**
     * Set prenom_pers
     *
     * @param string $prenomPers
     */
    public function setPrenomPers($prenomPers)
    {
        $this->prenom_pers = $prenomPers;
    }

    /**
     * Get prenom_pers
     *
     * @return string 
     */
    public function getPrenomPers()
    {
        return $this->prenom_pers;
    }

    /**
     * Set sexe
     *
     * @param string $sexe
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;
    }

    /**
     * Get sexe
     *
     * @return string 
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Set num_ss
     *
     * @param string $numSs
     */
    public function setNumSs($numSs)
    {
        $this->num_ss = $numSs;
    }

    /**
     * Get num_ss
     *
     * @return string 
     */
    public function getNumSs()
    {
        return $this->num_ss;
    }

    /**
     * Set nationalite
     *
     * @param string $nationalite
     */
    public function setNationalite($nationalite)
    {
        $this->nationalite = $nationalite;
    }

    /**
     * Get nationalite
     *
     * @return string 
     */
    public function getNationalite()
    {
        return $this->nationalite;
    }

    /**
     * Set num_cs
     *
     * @param string $numCs
     */
    public function setNumCs($numCs)
    {
        $this->num_cs = $numCs;
    }

    /**
     * Get num_cs
     *
     * @return string 
     */
    public function getNumCs()
    {
        return $this->num_cs;
    }

    /**
     * Set validite_cs
     *
     * @param date $validiteCs
     */
    public function setValiditeCs($validiteCs)
    {
        $this->validite_cs = $validiteCs;
    }

    /**
     * Get validite_cs
     *
     * @return date 
     */
    public function getValiditeCs()
    {
        return $this->validite_cs;
    }

    /**
     * Set vehicule
     *
     * @param string $vehicule
     */
    public function setVehicule($vehicule)
    {
        $this->vehicule = $vehicule;
    }

    /**
     * Get vehicule
     *
     * @return string 
     */
    public function getVehicule()
    {
        return $this->vehicule;
    }

    /**
     * Set permis
     *
     * @param string $permis
     */
    public function setPermis($permis)
    {
        $this->permis = $permis;
    }

    /**
     * Get permis
     *
     * @return string 
     */
    public function getPermis()
    {
        return $this->permis;
    }

    /**
     * Set telfix_pers
     *
     * @param string $telfixPers
     */
    public function setTelfixPers($telfixPers)
    {
        $this->telfix_pers = $telfixPers;
    }

    /**
     * Get telfix_pers
     *
     * @return string 
     */
    public function getTelfixPers()
    {
        return $this->telfix_pers;
    }

    /**
     * Set telport_pers
     *
     * @param string $telportPers
     */
    public function setTelportPers($telportPers)
    {
        $this->telport_pers = $telportPers;
    }

    /**
     * Get telport_pers
     *
     * @return string 
     */
    public function getTelportPers()
    {
        return $this->telport_pers;
    }

    /**
     * Set email_pers
     *
     * @param string $emailPers
     */
    public function setEmailPers($emailPers)
    {
        $this->email_pers = $emailPers;
    }

    /**
     * Get email_pers
     *
     * @return string 
     */
    public function getEmailPers()
    {
        return $this->email_pers;
    }

    /**
     * Set adres1
     *
     * @param string $adres1
     */
    public function setAdres1($adres1)
    {
        $this->adres1 = $adres1;
    }

    /**
     * Get adres1
     *
     * @return string 
     */
    public function getAdres1()
    {
        return $this->adres1;
    }

    /**
     * Set adres2
     *
     * @param string $adres2
     */
    public function setAdres2($adres2)
    {
        $this->adres2 = $adres2;
    }

    /**
     * Get adres2
     *
     * @return string 
     */
    public function getAdres2()
    {
        return $this->adres2;
    }

    /**
     * Set ville
     *
     * @param string $ville
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set cp
     *
     * @param string $cp
     */
    public function setCp($cp)
    {
        $this->cp = $cp;
    }

    /**
     * Get cp
     *
     * @return string 
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set commentaires
     *
     * @param text $commentaires
     */
    public function setCommentaires($commentaires)
    {
        $this->commentaires = $commentaires;
    }

    /**
     * Get commentaires
     *
     * @return text 
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }

    /**
     * Set societeuser
     *
     * @param string $societeuser
     */
    public function setSocieteuser($societeuser)
    {
        $this->societeuser = $societeuser;
    }

    /**
     * Get societeuser
     *
     * @return string 
     */
    public function getSocieteuser()
    {
        return $this->societeuser;
    }
}