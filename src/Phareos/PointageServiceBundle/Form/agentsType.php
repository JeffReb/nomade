<?php

namespace Phareos\PointageServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class agentsType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('nom_pers')
            ->add('prenom_pers')
            ->add('sexe', 'choice', array('choices' => array('M' => "M", 'F' => "F"), 
                                            'multiple' => false, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisir -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('num_ss')
            ->add('nationalite')
            ->add('num_cs')
            ->add('validite_cs')
            ->add('vehicule', 'choice', array('choices' => array('Oui' => "Oui", 'Non' => "Non"), 
                                            'multiple' => false, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisir -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('permis', 'choice', array('choices' => array('Oui' => "Oui", 'Non' => "Non"), 
                                            'multiple' => false, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Choisir -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('telfix_pers')
            ->add('telport_pers')
            ->add('email_pers')
            ->add('adres1')
            ->add('adres2')
            ->add('ville')
            ->add('cp')
            ->add('commentaires')
        ;
    }

    public function getName()
    {
        return 'phareos_pointageservicebundle_agentstype';
    }
}
