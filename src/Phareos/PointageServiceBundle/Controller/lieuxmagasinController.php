<?php

namespace Phareos\PointageServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Phareos\PointageServiceBundle\Entity\lieuxmagasin;
use Phareos\PointageServiceBundle\Form\lieuxmagasinType;

/**
 * lieuxmagasin controller.
 *
 */
class lieuxmagasinController extends Controller
{
    /**
     * Lists all lieuxmagasin entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosPointageServiceBundle:lieuxmagasin')->findBy(array('societeuser' => $societeUSER));

        return $this->render('PhareosPointageServiceBundle:lieuxmagasin:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a lieuxmagasin entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosPointageServiceBundle:lieuxmagasin')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find lieuxmagasin entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosPointageServiceBundle:lieuxmagasin:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new lieuxmagasin entity.
     *
     */
    public function newAction()
    {
        $entity = new lieuxmagasin();
        $form   = $this->createForm(new lieuxmagasinType(), $entity);

        return $this->render('PhareosPointageServiceBundle:lieuxmagasin:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new lieuxmagasin entity.
     *
     */
    public function createAction()
    {
        $entity  = new lieuxmagasin();
        $request = $this->getRequest();
        $form    = $this->createForm(new lieuxmagasinType(), $entity);
        $form->bindRequest($request);
		
		$session = $this->get('session');
		$societeUSER = $session->get('societeUSER');

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
			$entity->setSocieteuser($societeUSER);
			
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('lieuxmagasin_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosPointageServiceBundle:lieuxmagasin:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing lieuxmagasin entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosPointageServiceBundle:lieuxmagasin')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find lieuxmagasin entity.');
        }

        $editForm = $this->createForm(new lieuxmagasinType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosPointageServiceBundle:lieuxmagasin:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing lieuxmagasin entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosPointageServiceBundle:lieuxmagasin')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find lieuxmagasin entity.');
        }

        $editForm   = $this->createForm(new lieuxmagasinType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('lieuxmagasin_show', array('id' => $id)));
        }

        return $this->render('PhareosPointageServiceBundle:lieuxmagasin:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a lieuxmagasin entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosPointageServiceBundle:lieuxmagasin')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find lieuxmagasin entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('lieuxmagasin'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
