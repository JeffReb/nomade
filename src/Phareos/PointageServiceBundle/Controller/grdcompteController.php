<?php

namespace Phareos\PointageServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Phareos\PointageServiceBundle\Entity\grdcompte;
use Phareos\PointageServiceBundle\Form\grdcompteType;

/**
 * grdcompte controller.
 *
 */
class grdcompteController extends Controller
{
    /**
     * Lists all grdcompte entities.
     *
     */
    public function indexAction()
    {
        $session = $this->get('session');
		$societeUSER = $session->get('societeUSER');
		
		$em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosPointageServiceBundle:grdcompte')->findBy(array('societeuser' => $societeUSER));

        return $this->render('PhareosPointageServiceBundle:grdcompte:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a grdcompte entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosPointageServiceBundle:grdcompte')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find grdcompte entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosPointageServiceBundle:grdcompte:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new grdcompte entity.
     *
     */
    public function newAction()
    {
        $entity = new grdcompte();
        $form   = $this->createForm(new grdcompteType(), $entity);

        return $this->render('PhareosPointageServiceBundle:grdcompte:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new grdcompte entity.
     *
     */
    public function createAction()
    {
        $entity  = new grdcompte();
        $request = $this->getRequest();
        $form    = $this->createForm(new grdcompteType(), $entity);
        $form->bindRequest($request);
		
		$session = $this->get('session');
		$societeUSER = $session->get('societeUSER');

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
			$entity->setSocieteuser($societeUSER);
			
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('grdcompte_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosPointageServiceBundle:grdcompte:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing grdcompte entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosPointageServiceBundle:grdcompte')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find grdcompte entity.');
        }

        $editForm = $this->createForm(new grdcompteType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosPointageServiceBundle:grdcompte:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing grdcompte entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosPointageServiceBundle:grdcompte')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find grdcompte entity.');
        }

        $editForm   = $this->createForm(new grdcompteType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('grdcompte_show', array('id' => $id)));
        }

        return $this->render('PhareosPointageServiceBundle:grdcompte:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a grdcompte entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosPointageServiceBundle:grdcompte')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find grdcompte entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('grdcompte'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
