<?php

namespace Phareos\PointageServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\PointageServiceBundle\Entity\progaffect;
use Phareos\PointageServiceBundle\Form\progaffectType;

/**
 * progaffect controller.
 *
 */
class progaffectController extends Controller
{
    /**
     * Lists all progaffect entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('PhareosPointageServiceBundle:progaffect')->findAll();

        return $this->render('PhareosPointageServiceBundle:progaffect:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a progaffect entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosPointageServiceBundle:progaffect')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find progaffect entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosPointageServiceBundle:progaffect:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new progaffect entity.
     *
     */
    public function newAction()
    {
        $entity = new progaffect();
        $form   = $this->createForm(new progaffectType(), $entity);

        return $this->render('PhareosPointageServiceBundle:progaffect:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new progaffect entity.
     *
     */
    public function createAction()
    {
        $entity  = new progaffect();
        $request = $this->getRequest();
        $form    = $this->createForm(new progaffectType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('progaffect_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosPointageServiceBundle:progaffect:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing progaffect entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosPointageServiceBundle:progaffect')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find progaffect entity.');
        }

        $editForm = $this->createForm(new progaffectType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosPointageServiceBundle:progaffect:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing progaffect entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosPointageServiceBundle:progaffect')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find progaffect entity.');
        }

        $editForm   = $this->createForm(new progaffectType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('progaffect_edit', array('id' => $id)));
        }

        return $this->render('PhareosPointageServiceBundle:progaffect:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a progaffect entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosPointageServiceBundle:progaffect')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find progaffect entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('progaffect'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
