<?php

namespace Phareos\DeskNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class josemcontType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('jour', 'choice', array('choices' => array('Lundi' => "Lundi", 'Mardi' => "Mardi", 'Mercredi' => "Mercredi", 'Jeudi' => "Jeudi", 'Vendredi' => "Vendredi", 'Samedi' => "Samedi", 'Dimanche' => "Dimanche" ), 
                                            'multiple' => true, 
                                            'expanded' => true, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            //->add('hdebut')
            //->add('hfin')
            ->add('encours')
        ;
    }

    public function getName()
    {
        return 'phareos_desknetservicebundle_josemconttype';
    }
}
