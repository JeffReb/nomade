<?php

namespace Phareos\DeskNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class avenantType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('typeavenant', 'choice', array('choices' => array('CDD Accroissement Activité' => "CDD Accroissement Activité", 'CDD Arrêt de Travail' => "CDD Arrêt de Travail", 'CDD Congé' => "CDD Congé", 'CDD Congé Maternité' => "CDD Congé Maternité", 'CDD Congé Paternité' => "CDD Congé Paternité", 'CDD Congé Parental' => "CDD Congé Parental", 'CDD ABS Injustifiée' => "CDD ABS Injustifiée", 'CDD ABS Autorisée' => "CDD ABS Autorisée", 'CDI' => "CDI", 'CDI Intermittent' => "CDI Intermittent" ), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(2),
                                            'empty_value' => '- Choisissez une option -',
                                            'empty_data'  => null,
											'required' => true
                                            ))
            ->add('commentaire')
            ->add('confirmok')
        ;
    }

    public function getName()
    {
        return 'phareos_desknetservicebundle_avenanttype';
    }
}
