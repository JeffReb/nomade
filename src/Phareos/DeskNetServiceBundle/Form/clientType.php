<?php

namespace Phareos\DeskNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

use Phareos\DeskNetServiceBundle\Entity\grdcpte;
use Phareos\DeskNetServiceBundle\Entity\grdcpteRepository;

use Phareos\DeskNetServiceBundle\Entity\dept;
use Phareos\DeskNetServiceBundle\Entity\deptRepository;

class clientType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $societeUSER = $_SESSION['societe'];
		
		$builder
            ->add('nom')
            ->add('adres1')
            ->add('adres2')
            ->add('cp')
            ->add('ville')
            ->add('tel')
            ->add('tel2')
            ->add('tel3')
            ->add('fax')
            ->add('mail')
            ->add('nummag')
            ->add('civilrespsite', 'choice', array('choices' => array('Mr' => "Mr", 'Mme' => "Mme", 'Mlle' => "Mlle"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Civilité -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('nomresposite')
            ->add('prenomrespsite')
            ->add('telrespsite')
            ->add('mailrespsite')
            ->add('nbrcolab')
            ->add('nomcolab')
            ->add('typeinter', 'choice', array('choices' => array('Vitrage' => "Vitrage", 'Ménage' => "Ménage", 'Encombrants' => "Encombrants"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Selectionnez -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('intervautre', 'choice', array('choices' => array('Sous-Traitant' => "Sous-Traitant", 'Fournisseur' => "Fournisseur"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Selectionnez -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('nominterv')
            ->add('frequenceinterv')
            ->add('surfacevit')
            ->add('surfacebo')
            ->add('surfacetapis')
            ->add('surfacemag')
            ->add('nbrenseignes')
            ->add('longenseigne')
            ->add('datemisenplace')
            ->add('installdistri', 'choice', array('choices' => array('Oui' => "Oui", 'Non' => "Non"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Oui / Non -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('cdb', 'choice', array('choices' => array('Oui' => "Oui", 'Non' => "Non"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Oui / Non -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('planprev', 'choice', array('choices' => array('Oui' => "Oui", 'Non' => "Non"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Oui / Non -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('grdcpte', 'entity', array('class' => 'PhareosDeskNetServiceBundle:grdcpte',
												'property' => 'Nom',
												'query_builder' => function(grdcpteRepository $er) {
												return $er->createQueryBuilder ('u')
												->orderBY ('u.Nom', 'ASC');
												},
												))
			->add('departements', 'entity', array('class' => 'PhareosDeskNetServiceBundle:dept',
												'property' => 'nom',
												'query_builder' => function(deptRepository $er) {
												return $er->createQueryBuilder ('u')
												->where ('u.client = :societeUSER')
												->setParameter('societeUSER', $_SESSION['societe'])
												->orderBY ('u.nom', 'ASC');
												},
												'multiple' => true,
												'expanded' => false
												))
        ;
    }

    public function getName()
    {
        return 'phareos_desknetservicebundle_clienttype';
    }
}
