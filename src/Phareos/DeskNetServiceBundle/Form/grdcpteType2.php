<?php

namespace Phareos\DeskNetServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class grdcpteType2 extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('Nom')
            ->add('adres1')
            ->add('adres2')
			->add('cp')
			->add('ville')
            ->add('tel')
            ->add('fax')
            ->add('mail')
            ->add('acheteur')
			->add('achcivil', 'choice', array('choices' => array('Mr' => "Mr", 'Mme' => "Mme", 'Mlle' => "Mlle"), 
                                            'multiple' => false, 
                                            'expanded' => false, 
                                            'preferred_choices' => array(0),
                                            'empty_value' => '- Civilité -',
                                            'empty_data'  => null,
											'required' => false
                                            ))
            ->add('achtel')
            ->add('achmail')
            ->add('nbragences')
        ;
    }

    public function getName()
    {
        return 'phareos_desknetservicebundle_grdcptetype2';
    }
}
