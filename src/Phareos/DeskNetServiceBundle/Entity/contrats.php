<?php

namespace Phareos\DeskNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\DeskNetServiceBundle\Entity\contrats
 *
 * @ORM\Table(name="desk_contrats")
 * @ORM\Entity(repositoryClass="Phareos\DeskNetServiceBundle\Entity\contratsRepository")
 */
class contrats
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\ManyToOne(targetEntity="collaborateur", inversedBy="collabcontrat", cascade={"remove"})
	 * @ORM\JoinColumn(name="collaborateur_id", referencedColumnName="id")
	 */
	protected $collaborateur;
	
	/**
     * @var date $dateemb
     *
     * @ORM\Column(name="dateemb", type="date", nullable=true)
     */
    private $dateemb;
	
	/**
     * @var date $datecont
     *
     * @ORM\Column(name="datecont", type="date", nullable=true)
     */
    private $datecont;

    /**
     * @var string $type
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var boolean $avenant
     *
     * @ORM\Column(name="avenant", type="boolean", nullable=true)
     */
    private $avenant;

    /**
     * @var integer $numav
     *
     * @ORM\Column(name="numav", type="integer", nullable=true)
     */
    private $numav;

    /**
     * @var date $dateenvoi
     *
     * @ORM\Column(name="dateenvoi", type="date", nullable=true)
     */
    private $dateenvoi;

    /**
     * @var date $daterenv
     *
     * @ORM\Column(name="daterenv", type="date", nullable=true)
     */
    private $daterenv;

    /**
     * @var date $datefin
     *
     * @ORM\Column(name="datefin", type="date", nullable=true)
     */
    private $datefin;

    /**
     * @var text $commentaires
     *
     * @ORM\Column(name="commentaires", type="text", nullable=true)
     */
    private $commentaires;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateemb
     *
     * @param date $dateemb
     */
    public function setDateemb($dateemb)
    {
        $this->dateemb = $dateemb;
    }

    /**
     * Get dateemb
     *
     * @return date 
     */
    public function getDateemb()
    {
        return $this->dateemb;
    }
	
	/**
     * Set datecont
     *
     * @param date $datecont
     */
    public function setDatecont($datecont)
    {
        $this->datecont = $datecont;
    }

    /**
     * Get datecont
     *
     * @return date 
     */
    public function getDatecont()
    {
        return $this->datecont;
    }

    /**
     * Set type
     *
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set avenant
     *
     * @param boolean $avenant
     */
    public function setAvenant($avenant)
    {
        $this->avenant = $avenant;
    }

    /**
     * Get avenant
     *
     * @return boolean 
     */
    public function getAvenant()
    {
        return $this->avenant;
    }

    /**
     * Set numav
     *
     * @param integer $numav
     */
    public function setNumav($numav)
    {
        $this->numav = $numav;
    }

    /**
     * Get numav
     *
     * @return integer 
     */
    public function getNumav()
    {
        return $this->numav;
    }

    /**
     * Set dateenvoi
     *
     * @param date $dateenvoi
     */
    public function setDateenvoi($dateenvoi)
    {
        $this->dateenvoi = $dateenvoi;
    }

    /**
     * Get dateenvoi
     *
     * @return date 
     */
    public function getDateenvoi()
    {
        return $this->dateenvoi;
    }

    /**
     * Set daterenv
     *
     * @param date $daterenv
     */
    public function setDaterenv($daterenv)
    {
        $this->daterenv = $daterenv;
    }

    /**
     * Get daterenv
     *
     * @return date 
     */
    public function getDaterenv()
    {
        return $this->daterenv;
    }

    /**
     * Set datefin
     *
     * @param date $datefin
     */
    public function setDatefin($datefin)
    {
        $this->datefin = $datefin;
    }

    /**
     * Get datefin
     *
     * @return date 
     */
    public function getDatefin()
    {
        return $this->datefin;
    }

    /**
     * Set commentaires
     *
     * @param text $commentaires
     */
    public function setCommentaires($commentaires)
    {
        $this->commentaires = $commentaires;
    }

    /**
     * Get commentaires
     *
     * @return text 
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }

    /**
     * Set collaborateur
     *
     * @param Phareos\DeskNetServiceBundle\Entity\collaborateur $collaborateur
     */
    public function setCollaborateur(\Phareos\DeskNetServiceBundle\Entity\collaborateur $collaborateur)
    {
        $this->collaborateur = $collaborateur;
    }

    /**
     * Get collaborateur
     *
     * @return Phareos\DeskNetServiceBundle\Entity\collaborateur 
     */
    public function getCollaborateur()
    {
        return $this->collaborateur;
    }
}