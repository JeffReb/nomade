<?php

namespace Phareos\DeskNetServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phareos\DeskNetServiceBundle\Entity\josemcont
 *
 * @ORM\Table(name="desk_josemcont")
 * @ORM\Entity(repositoryClass="Phareos\DeskNetServiceBundle\Entity\josemcontRepository")
 */
class josemcont
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var array $jour
     *
     * @ORM\Column(name="jour", type="array", length=255, nullable=true)
     */
    private $jour;

    /**
     * @var time $hdebut
     *
     * @ORM\Column(name="hdebut", type="time", nullable=true)
     */
    private $hdebut;

    /**
     * @var time $hfin
     *
     * @ORM\Column(name="hfin", type="time", nullable=true)
     */
    private $hfin;

    /**
     * @var boolean $encours
     *
     * @ORM\Column(name="encours", type="boolean", nullable=true)
     */
    private $encours;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set jour
     *
     * @param array $jour
     */
    public function setJour($jour)
    {
        $this->jour = $jour;
    }

    /**
     * Get jour
     *
     * @return array 
     */
    public function getJour()
    {
        return $this->jour;
    }

    /**
     * Set hdebut
     *
     * @param time $hdebut
     */
    public function setHdebut($hdebut)
    {
        $this->hdebut = $hdebut;
    }

    /**
     * Get hdebut
     *
     * @return time 
     */
    public function getHdebut()
    {
        return $this->hdebut;
    }

    /**
     * Set hfin
     *
     * @param time $hfin
     */
    public function setHfin($hfin)
    {
        $this->hfin = $hfin;
    }

    /**
     * Get hfin
     *
     * @return time 
     */
    public function getHfin()
    {
        return $this->hfin;
    }

    /**
     * Set encours
     *
     * @param boolean $encours
     */
    public function setEncours($encours)
    {
        $this->encours = $encours;
    }

    /**
     * Get encours
     *
     * @return boolean 
     */
    public function getEncours()
    {
        return $this->encours;
    }
}