<?php

namespace Phareos\DeskNetServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Phareos\DeskNetServiceBundle\Entity\josemcont;
use Phareos\DeskNetServiceBundle\Form\josemcontType;

/**
 * josemcont controller.
 *
 */
class josemcontController extends Controller
{
    /**
     * Lists all josemcont entities.
     *
     */
    public function indexAction()
    {
        $session = $this->get('session');
		$Dept = $session->get('nomDEPT');
		$societeUSER = $session->get('societeUSER');
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$repository_dept = $em->getRepository('PhareosDeskNetServiceBundle:dept');

        $entities = $em->getRepository('PhareosDeskNetServiceBundle:josemcont')->findAll();
		
		$depts = $repository_dept->findBy(array('client' => $societeUSER));

        return $this->render('PhareosDeskNetServiceBundle:josemcont:index.html.twig', array(
            'depts' => $depts,
			'defaultDept' => $Dept,
			'entities' => $entities
        ));
    }

    /**
     * Finds and displays a josemcont entity.
     *
     */
    public function showAction($id)
    {
        $session = $this->get('session');
		$Dept = $session->get('nomDEPT');
		$societeUSER = $session->get('societeUSER');
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$repository_dept = $em->getRepository('PhareosDeskNetServiceBundle:dept');

        $entity = $em->getRepository('PhareosDeskNetServiceBundle:josemcont')->find($id);
		
		$depts = $repository_dept->findBy(array('client' => $societeUSER));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find josemcont entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosDeskNetServiceBundle:josemcont:show.html.twig', array(
            'depts' => $depts,
			'defaultDept' => $Dept,
			'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new josemcont entity.
     *
     */
    public function newAction()
    {
        $session = $this->get('session');
		$Dept = $session->get('nomDEPT');
		$societeUSER = $session->get('societeUSER');
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$repository_dept = $em->getRepository('PhareosDeskNetServiceBundle:dept');
		
		$depts = $repository_dept->findBy(array('client' => $societeUSER));
		
		
		$entity = new josemcont();
        $form   = $this->createForm(new josemcontType(), $entity);

        return $this->render('PhareosDeskNetServiceBundle:josemcont:new.html.twig', array(
            'depts' => $depts,
			'defaultDept' => $Dept,
			'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new josemcont entity.
     *
     */
    public function createAction()
    {
        $entity  = new josemcont();
        $request = $this->getRequest();
        $form    = $this->createForm(new josemcontType(), $entity);
        $form->bindRequest($request);
		
		$hdebut = $request->request->get('hdebut');
		$hfin = $request->request->get('hfin');

		
		$hdebut2 = $hdebut . ':00';
		$hfin2 = $hfin . ':00';
		
		$hdebut3 = new \DateTime($hdebut2);
		$hfin3 = new \DateTime($hfin2);
		

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
			
			$entity->setHdebut($hdebut3);
			$entity->setHfin($hfin3);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('josemcont_show', array('id' => $entity->getId())));
            
        }

        return $this->render('PhareosDeskNetServiceBundle:josemcont:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing josemcont entity.
     *
     */
    public function editAction($id)
    {
        $session = $this->get('session');
		$Dept = $session->get('nomDEPT');
		$societeUSER = $session->get('societeUSER');
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$repository_dept = $em->getRepository('PhareosDeskNetServiceBundle:dept');

        $entity = $em->getRepository('PhareosDeskNetServiceBundle:josemcont')->find($id);
		
		$depts = $repository_dept->findBy(array('client' => $societeUSER));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find josemcont entity.');
        }

        $editForm = $this->createForm(new josemcontType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PhareosDeskNetServiceBundle:josemcont:edit.html.twig', array(
            'depts' => $depts,
			'defaultDept' => $Dept,
			'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing josemcont entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('PhareosDeskNetServiceBundle:josemcont')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find josemcont entity.');
        }

        $editForm   = $this->createForm(new josemcontType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('josemcont_edit', array('id' => $id)));
        }

        return $this->render('PhareosDeskNetServiceBundle:josemcont:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a josemcont entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('PhareosDeskNetServiceBundle:josemcont')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find josemcont entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('josemcont'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
