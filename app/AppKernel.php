<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\DoctrineBundle\DoctrineBundle(),
			new Symfony\Bundle\DoctrineFixturesBundle\DoctrineFixturesBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new JMS\SecurityExtraBundle\JMSSecurityExtraBundle(),
			new Sonata\IntlBundle\SonataIntlBundle(),
			new Sonata\EasyExtendsBundle\SonataEasyExtendsBundle(),
			new WhiteOctober\PagerfantaBundle\WhiteOctoberPagerfantaBundle(),
			new Ornicar\GravatarBundle\OrnicarGravatarBundle(),
            new Phareos\NomadeNetServiceBundle\PhareosNomadeNetServiceBundle(),
			new FOS\UserBundle\FOSUserBundle(),
			new Phareos\UserBundle\PhareosUserBundle(),
            new Phareos\BaseBundle\PhareosBaseBundle(),
            new Phareos\UtilisateurBundle\PhareosUtilisateurBundle(),
            new Phareos\LogisToolBoxBundle\PhareosLogisToolBoxBundle(),
            new Phareos\DeskNetServiceBundle\PhareosDeskNetServiceBundle(),
            new Phareos\PointageServiceBundle\PhareosPointageServiceBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
			$bundles[] = new CoreSphere\ConsoleBundle\CoreSphereConsoleBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
